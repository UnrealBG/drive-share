# Carpooling System using ASP.NET Core WebAPI, Database, and Blazor Server

The Carpooling System is a comprehensive web-based application designed to streamline and simplify the process of sharing rides and coordinating journeys. Using cutting-edge technologies and intuitive interfaces, it offers a seamless experience for drivers, passengers, and administrators alike.

## Features

### Public Part (Accessible without Authentication)

- **Registration & Login:** Anonymous users can register with an email verification process and log in.
- **Platform Overview:** Provides detailed information about Carpooling, its features, and insights into platform usage, such as the number of users and travels conducted.
- **Top Users:** Displays the top 10 travel organizers and top 10 passengers.

### Private Part (Accessible only to Authenticated Users)

- **User Profile:** Authenticated users can update their profiles, set avatars, and manage their accounts.
- **Travel Management:** Users can create, view, and manage their travel plans.
- **Travel Exploration:** Browse available trips with options to filter, sort, and paginate. Users can apply for trips and manage their trip applications.
- **Feedback System:** Post-trip feedback mechanism for both drivers and passengers, including numeric ratings (0 to 5) and optional comments.

### Administrative Dashboard

- **User Management:** Admins can view, search, block, and unblock users.
- **Travel Oversight:** View, filter, and sort all travels.
  
## Public Part Details

Anonymous users can:

- Register, which involves an email verification flow.
- Learn extensively about the Carpooling platform and its benefits.
- View platform statistics and top user rankings.

## Private Part Details

Authenticated users can:

- Personalize their profiles and manage their travel plans.
- Explore and participate in available journeys.
- Engage in a robust feedback system after each trip to ensure the quality of experiences.

## Admin Dashboard Details

The administrative dashboard offers:

- A comprehensive user management system, allowing admins to oversee and intervene when necessary.
- A holistic view of all travels, making administrative oversight efficient and effective.

## Technologies Used

- **ASP.NET Core WebAPI:** A modern, cross-platform framework for building web APIs that can be accessed from any device.
- **Entity Framework Core:** Microsoft's modern object-database mapper for .NET, which allows for interacting with databases using .NET objects.
- **Blazor Server:** A web UI framework where components run on the server, with UI updates, event handling, and JavaScript calls being handled over SignalR connections.
- **MAUI Blazor Hybrid:** A combination of .NET MAUI and Blazor, enabling building native UIs for Android, iOS, macOS, and Windows using .NET and web technologies.
- **SignalR:** A library for ASP.NET developers that simplifies the process of adding real-time functionality to applications via websockets.
- **HTML, CSS, and JavaScript:** Core technologies for building the structure, design, and functionality of web pages and web applications.
- **Azure Active Directory B2C:** A customer identity access management solution from Microsoft's Azure platform, providing identity as a service for custom-tailored consumer-facing applications.
- **Cloudinary:** A cloud service that offers solutions for image and video management, including uploads, storage, transformations, optimizations, and delivery.


## Prerequisites

Before running the project, ensure you have the following prerequisites installed:

- [.NET Core SDK](https://dotnet.microsoft.com/download) (v7 or later)
- [Visual Studio](https://visualstudio.microsoft.com/downloads/) or [Visual Studio Code](https://code.visualstudio.com/download)
- A compatible database server (e.g., SQL Server, PostgreSQL)

## Getting Started

1. Clone the repository:
```command
git clone https://gitlab.com/NedkoNedkov/carpooling.git
````
2. Open the project in your preferred IDE (e.g., Visual Studio or Visual Studio Code).

3. Configure the Database:

- Open the appsettings.json file.
- Update the ConnectionStrings section with the connection string for your database server.

Here is an image of the database schema for reference:
![Database Schema](https://drive.google.com/uc?id=12z4iXl9P9Ud5XxjybpCln-INAl4jVtT_)

And here's the SQL script for database generation:

[View the Database Generation Script here](https://drive.google.com/uc?id=134r7LQHFS3A5rsIYHePAPPETD9pHfaw1)

4. Apply Database Migrations:

- Open the terminal or command prompt.
- Navigate to the project directory.
- Run the following commands:

```commands
dotnet restore
dotnet ef database update
```
5. Build and Run the Backend:

- Build and run the ASP.NET Core WebAPI project.
6. Build and Run the Frontend:

- Navigate to the Client directory.
- Run the following commands:
```commands
dotnet restore
dotnet build
dotnet run
```
## Live Demo and Admin Credentials

For a live demonstration of the project, you can visit the following URLs:

- **Frontend:** [www.driveshare.app/](https://www.driveshare.app/)
- **Backend API:** [https://api.driveshare.app](https://api.driveshare.app/swagger)

Administrator access is available with the following credentials:

- **Username:** admin
- **Password:** 4ht12lM1t!7$

Please note: For security reasons, it's advised to change these credentials or create new ones as soon as possible.

## Contributing
#### Contributions to the Carpooling project are welcome! Here are a few ways you can contribute:

- Report bugs or issues by submitting GitLab issues.
- Suggest new features or enhancements.
- Submit pull requests with bug fixes or code improvements.

### Contributors

Here are some of the amazing contributors to this project:

| [<img src="https://drive.google.com/uc?id=138T0xAoxVGj1BMsClltA5F31qCQAMOjW" width="100" height="100" alt="Contributor1 Image" style="display:block; margin-left:auto; margin-right:auto;"/>](https://gitlab.com/UnrealBG) | [<img src="https://gitlab.com/uploads/-/system/user/avatar/13950363/avatar.png" width="100" height="100" alt="Contributor2 Image" style="display:block; margin-left:auto; margin-right:auto;"/>](https://gitlab.com/NedkoNedkov) | [<img src="https://media.licdn.com/dms/image/C5603AQGZ0GQC2uRc6Q/profile-displayphoto-shrink_800_800/0/1517478354414?e=1697068800&v=beta&t=Z1JFwbSDxG2U3LQvPuhEVB_vxr7qlsWmPKwXMdEF1KQ" width="100" height="100" alt="Contributor3 Image" style="display:block; margin-left:auto; margin-right:auto;"/>](https://gitlab.com/dimitar_nedelchev) |
|:------------------------------------------------------------:|:------------------------------------------------------------:|:------------------------------------------------------------:|
| [Zhelyazko Zhelyazkov](https://gitlab.com/UnrealBG)           | [Nedko Nedkov](https://gitlab.com/NedkoNedkov)             | [Dimitar Nedelchev](https://gitlab.com/dimitar_nedelchev)             |


## License
#### This project is licensed under the [MIT License](https://gitlab.com/NedkoNedkov/carpooling/-/blob/main/LICENSE).
