﻿namespace Carpooling.Tests
{
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.Entities;

    using Moq;

    [TestFixture]
    public class RideServiceTest
    {
        private Mock<IRepository<Ride, GetRideDto>> rideDtoRepository;
        private Mock<IRepository<Ride, Ride>> rideRepository;
        private Mock<IFeedbackService> feedbackService;
        private Mock<IGeoLocationService> geoMapService;
        private Mock<IVehicleService> vehicleService;
        private Mock<IAddressService> addressService;
        private Mock<IUserService> userService;
        private Mock<IBookService> bookService;
        private Mock<IEmailService> emailService;
        private Mock<IMapper> mapper;

        private IRideService rideService;

        [SetUp]
        public void Setup()
        {
            this.rideDtoRepository = new Mock<IRepository<Ride, GetRideDto>>();
            this.rideRepository = new Mock<IRepository<Ride, Ride>>();
            this.feedbackService = new Mock<IFeedbackService>();
            this.geoMapService = new Mock<IGeoLocationService>();
            this.vehicleService = new Mock<IVehicleService>();
            this.addressService = new Mock<IAddressService>();
            this.userService = new Mock<IUserService>();
            this.bookService = new Mock<IBookService>();
            this.emailService = new Mock<IEmailService>();
            this.mapper = new Mock<IMapper>();

            this.rideService = new RideService(rideDtoRepository.Object, rideRepository.Object, feedbackService.Object, geoMapService.Object, vehicleService.Object, addressService.Object, userService.Object, bookService.Object, emailService.Object, mapper.Object);
        }

        #region GetRidesAsyc
        [Test]
        public async Task GetRidesAsync_NotFound_NoRides()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            this.rideDtoRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Ride, GetRideDto>>>(),
                                                         It.IsAny<Func<IQueryable<Ride>, IQueryable<Ride>>>(),
                                                         It.IsAny<Func<IQueryable<Ride>, IOrderedQueryable<Ride>>>(),
                                                         It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>(), null))
                .ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.GetRidesAsync(new FindRideQuery());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetRidesAsync_Success()
        {
            //Arrange
            this.rideDtoRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Ride, GetRideDto>>>(),
                                                         It.IsAny<Func<IQueryable<Ride>, IQueryable<Ride>>>(),
                                                         It.IsAny<Func<IQueryable<Ride>, IOrderedQueryable<Ride>>>(),
                                                         It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>()
                                                         , It.IsAny<PageParameters>()))
                .ReturnsAsync(() => new List<GetRideDto>() { new GetRideDto(), new GetRideDto() });
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.5, 23.4)));

            //Act
            var result = await this.rideService.GetRidesAsync(new FindRideQuery());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        [Test]
        public async Task GetItemsCountAfterFiltesAsync_Success()
        {
            //Arrange
            this.rideRepository.Setup(x => x.GetItemsCountAfterFiltersAsync(It.IsAny<Func<IQueryable<Ride>, IQueryable<Ride>>>())).ReturnsAsync(4);

            //Act
            var result = await this.rideService.GetItemsCountAfterFiltesAsync(new FindRideQuery());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        #region CancelRideAsync
        [Test]
        public async Task UpdateRideStatusAsync_IncorrectStatus()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var newStatusDto = new UpdateRideStatus()
            {
                Status = RideStatus.Active,
                RideId = Guid.NewGuid()
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), null)).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.UpdateRideStatusAsync(Guid.NewGuid(), new UpdateRideStatus());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideStatusAsync_NotFound_Invalid_RideId()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            var newStatusDto = new UpdateRideStatus()
            {
                Status = RideStatus.Cancelled,
                RideId = Guid.NewGuid()
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), null)).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.UpdateRideStatusAsync(Guid.NewGuid(), newStatusDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideStatusAsync_Unauthorazed_UserNotOrganizer()
        {
            //Arrange
            var expected = HttpStatusCode.Unauthorized;

            var newStatusDto = new UpdateRideStatus()
            {
                Status = RideStatus.Completed,
                RideId = Guid.NewGuid()
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => new Ride() { DriverId = Guid.NewGuid() });

            //Act
            var result = await this.rideService.UpdateRideStatusAsync(Guid.NewGuid(), newStatusDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideStatusAsync_Verify_Intime()
        {
            //Arrange
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newStatusDto = new UpdateRideStatus()
            {
                Status = RideStatus.Cancelled,
                RideId = Guid.NewGuid()
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+3599098303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => new Ride() { DriverId = userId, DepartureTime = DateTime.UtcNow.AddMinutes(30) });
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>)).Returns(passenger);

            //Act
            var result = await this.rideService.UpdateRideStatusAsync(userId, newStatusDto);

            //Assert
            this.feedbackService.Verify(x => x.DeleteDriverRatingAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Test]
        public async Task UpdateRideStatusAsync_NoBookings()
        {
            //Arrange
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newStatusDto = new UpdateRideStatus()
            {
                Status = RideStatus.Cancelled,
                RideId = Guid.NewGuid()
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+3599098303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => new Ride() { DriverId = userId, DepartureTime = DateTime.UtcNow.AddMinutes(130) });
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Failure("", HttpStatusCode.NotFound));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>)).Returns(passenger);

            //Act
            var result = await this.rideService.UpdateRideStatusAsync(userId, newStatusDto);

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        [Test]
        public async Task UpdateRideStatusAsync_Success()
        {
            //Arrange
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newStatusDto = new UpdateRideStatus()
            {
                Status = RideStatus.Cancelled,
                RideId = Guid.NewGuid()
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+3599098303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => new Ride() { DriverId = userId, DepartureTime = DateTime.UtcNow.AddMinutes(130) });
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>)).Returns(passenger);

            //Act
            var result = await this.rideService.UpdateRideStatusAsync(userId, newStatusDto);

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region CreateRideAsync
        [Test]
        public async Task CreateRideAsync_BadRequest_DepartureTine_CanNotBe_InPAst()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var newRide = new CreateRideDto()
            {
                VehicleId = Guid.NewGuid(),
                DepartureTime = DateTime.UtcNow.AddMinutes(-10)
            };

            //Act
            var result = await this.rideService.CreateRideAsync(Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateRideAsync_NotFound_NotVehicle()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            var newRide = new CreateRideDto()
            {
                VehicleId = Guid.NewGuid(),
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            this.vehicleService.Setup(x => x.GetVehicleAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<GetVehicleDto>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.CreateRideAsync(Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateRideAsync_NotFound_PicUpAddressNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            var newRide = new CreateRideDto()
            {
                VehicleId = Guid.NewGuid(),
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            this.vehicleService.Setup(x => x.GetVehicleAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<GetVehicleDto>.Success(new GetVehicleDto()));
            this.addressService.Setup(x => x.GetAddressByIdAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<Address>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.CreateRideAsync(Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateRideAsync_NotFound_DropOffAddressNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            var addressId = Guid.NewGuid();
            var dropOffAddressId = Guid.NewGuid();

            var newRide = new CreateRideDto()
            {
                VehicleId = Guid.NewGuid(),
                StartAddressId = addressId,
                EndAddressId = dropOffAddressId,
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            this.vehicleService.Setup(x => x.GetVehicleAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<GetVehicleDto>.Success(new GetVehicleDto()));
            this.addressService.Setup(x => x.GetAddressByIdAsync(addressId)).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.addressService.Setup(x => x.GetAddressByIdAsync(dropOffAddressId)).ReturnsAsync(() => Result<Address>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.CreateRideAsync(Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateRideAsync_Success()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            var addressId = Guid.NewGuid();
            var dropOffAddressId = Guid.NewGuid();

            var newRide = new CreateRideDto()
            {
                VehicleId = Guid.NewGuid(),
                StartAddressId = addressId,
                EndAddressId = dropOffAddressId,
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            var ride = new Ride()
            {
                VehicleId = newRide.VehicleId
            };

            this.vehicleService.Setup(x => x.GetVehicleAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<GetVehicleDto>.Success(new GetVehicleDto()));
            this.addressService.Setup(x => x.GetAddressByIdAsync(addressId)).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.addressService.Setup(x => x.GetAddressByIdAsync(dropOffAddressId)).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.mapper.Setup(x => x.Map<Ride>(It.IsAny<CreateRideDto>())).Returns(ride);
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<CreateRideDto>())).Returns(new GetRideDto());

            //Act
            var result = await this.rideService.CreateRideAsync(Guid.NewGuid(), newRide);

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region DeleteRideAsync
        [Test]
        public async Task DeleteRideAsync_NotFound_RideNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.DeleteRideAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteRideAsync_NotFound_RolesNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => new Ride() { DepartureTime = DateTime.UtcNow.AddHours(2) });
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.DeleteRideAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteRideAsync_Unathorized_NotOrganaizer()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => new Ride() { DepartureTime = DateTime.UtcNow.AddHours(2) });
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));

            //Act
            var result = await this.rideService.DeleteRideAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteRideAsync_Success()
        {
            //Arrange
            var driverId = Guid.NewGuid();

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => new Ride() { DriverId = driverId, DepartureTime = DateTime.UtcNow.AddHours(2) });
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));

            //Act
            var result = await this.rideService.DeleteRideAsync(driverId, Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region GetRideDetailsAsync
        [Test]
        public async Task GetRideDetailsAsync_NotFound_NotRide()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            this.rideDtoRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, GetRideDto>>>(), null)).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.GetRideDetailsAsync(Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetRideDetailsAsync_Success()
        {
            //Arrange
            this.rideDtoRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, GetRideDto>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => new GetRideDto());

            //Act
            var result = await this.rideService.GetRideDetailsAsync(Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region UpdateRideStartLocationAsync
        [Test]
        public async Task UodateRideStartLocationAsync_NotFount_NotRide()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(Guid.NewGuid(), Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideStartLocationAsync_Conflict_DateTimeNowAfter_DepartureTime()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(-10)
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(Guid.NewGuid(), Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideStartLocationAsync_NotFound_GetUserRolesFailed()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(Guid.NewGuid(), Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideStartLocationAsync_Unauthorized_UserNotOrganizer()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = driverId
            };

            var user = new User()
            {
                Id = userId,
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideStartLocationAsync_BadRequest_GetFreePlacesFailed()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = userId
            };

            var user = new User()
            {
                Id = userId,
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Failure("", HttpStatusCode.BadRequest));

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideStartLocationAsync_BadRequest_AlredyHasBooking()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = userId,
                FreeSpots = 2
            };

            var user = new User()
            {
                Id = userId,
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(1));

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideStartLocationAsync_VerifyDeleteRating_LessOneHourBeforeGo()
        {
            //Arrange
            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId,
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.4, 43.4)));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            this.feedbackService.Verify(x => x.DeleteDriverRatingAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Test]
        public async Task UodateRideStartLocationAsync_NotFound_AddressNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId,
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Failure("", HttpStatusCode.NotFound));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.4, 43.4)));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideStartLocationAsync_Conflict_GetDistanceFailed()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Failure("", HttpStatusCode.Conflict));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideStartLocationAsync_Success_NoAffectedPeople()
        {
            //Arrange
            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.4, 43.4)));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Failure("", HttpStatusCode.NotFound));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        [Test]
        public async Task UodateRideStartLocationAsync_Success()
        {
            //Arrange
            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(150),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.4, 43.4)));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideStartLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region UpdateRideEndLocationAsync
        [Test]
        public async Task UodateRideEndLocationAsync_NotFount_NotRide()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(Guid.NewGuid(), Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideEndLocationAsync_Conflict_DateTimeNowAfter_DepartureTime()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(-10)
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(Guid.NewGuid(), Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideEndLocationAsync_NotFound_GetUserRolesFailed()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(Guid.NewGuid(), Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideEndLocationAsync_Unauthorized_UserNotOrganizer()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = driverId
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideEndLocationAsync_BadRequest_GetFreePlacesFailed()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = userId
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Failure("", HttpStatusCode.BadRequest));

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideEndLocationAsync_BadRequest_AlredyHasBooking()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = userId,
                FreeSpots = 2
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(1));

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideEndLocationAsync_VerifyDeleteRating_LessOneHourBeforeGo()
        {
            //Arrange
            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.4, 43.4)));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            this.feedbackService.Verify(x => x.DeleteDriverRatingAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Test]
        public async Task UodateRideEndLocationAsync_NotFound_AddressNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Failure("", HttpStatusCode.NotFound));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.4, 43.4)));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideEndLocationAsync_Conflict_GetDistanceFailed()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Failure("", HttpStatusCode.Conflict));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideEndLocationAsync_Success_NoAffectedPeople()
        {
            //Arrange
            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.4, 43.4)));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Failure("", HttpStatusCode.NotFound));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        [Test]
        public async Task UodateRideEndLocationAsync_Success()
        {
            //Arrange
            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(150),
                DriverId = userId,
                FreeSpots = 2,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            var user = new User()
            {
                Id = userId
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var driver = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(2));
            this.addressService.Setup(x => x.UpdateAddressAsync(It.IsAny<Address>(), It.IsAny<CreateAddressDto>())).ReturnsAsync(() => Result<Address>.Success(new Address()));
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.4, 43.4)));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => new GetRideDto());
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { new GetRideBookingDto() { Passenger = passenger } }));
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(() => driver);

            //Act
            var result = await this.rideService.UpdateRideEndLocationAsync(userId, Guid.NewGuid(), new CreateAddressDto());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region UpdateRideVehicleAsync
        [Test]
        public async Task UodateRideVehicleAsync_NotFound_NotRide()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.UpdateRideVehicleAsync(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideVehicleAsync_Conflict_DateTimeNowAfter_DepartureTime()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(-10)
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);

            //Act
            var result = await this.rideService.UpdateRideVehicleAsync(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideVehicleAsync_NotFound_GetUserRolesFailed()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.UpdateRideVehicleAsync(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideVehicleAsync_Unauthorized_UserNotOrganizer()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = driverId
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));

            //Act
            var result = await this.rideService.UpdateRideVehicleAsync(userId, Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideVehicleAsync_NotFound_NotVehicle()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = userId
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.vehicleService.Setup(x => x.GetVehicleAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<GetVehicleDto>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.UpdateRideVehicleAsync(userId, Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideVehicleAsync_NotFound_UpdateRideFailed()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var vehicle = new GetVehicleDto()
            {
                LicensePlate = "BP 1914 SF"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = userId,
                Vehicle = new Vehicle() { PlateNumber = "BP 1914 SF" }
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.vehicleService.Setup(x => x.GetVehicleAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<GetVehicleDto>.Success(vehicle));
            this.rideRepository.Setup(x => x.UpdateAsync(It.IsAny<Ride>())).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.UpdateRideVehicleAsync(userId, Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideVehicleAsync_Conflict_GetDictanceFailed()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;

            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var rideId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var vehicle = new GetVehicleDto()
            {
                LicensePlate = "BP 1914 SF"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = userId,
                Vehicle = new Vehicle() { PlateNumber = "BP 1914 SF" }
            };

            var user = new User()
            {
                Id = userId
            };

            var getRide = new GetRideDto()
            {
                Id = rideId,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.vehicleService.Setup(x => x.GetVehicleAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<GetVehicleDto>.Success(vehicle));
            this.rideRepository.Setup(x => x.UpdateAsync(It.IsAny<Ride>())).ReturnsAsync(() => ride);
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(getRide);
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(Result<(double Distance, double Duration)>.Failure("", HttpStatusCode.Conflict));

            //Act
            var result = await this.rideService.UpdateRideVehicleAsync(userId, Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UodateRideVehicleAsync_Success_NoAffectedPeople()
        {
            //Arrange
            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var rideId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var vehicle = new GetVehicleDto()
            {
                LicensePlate = "BP 1914 SF"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = userId,
                Vehicle = new Vehicle() { PlateNumber = "BP 1914 SF" }
            };

            var user = new User()
            {
                Id = userId
            };

            var getRide = new GetRideDto()
            {
                Id = rideId,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.vehicleService.Setup(x => x.GetVehicleAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<GetVehicleDto>.Success(vehicle));
            this.rideRepository.Setup(x => x.UpdateAsync(It.IsAny<Ride>())).ReturnsAsync(() => ride);
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(getRide);
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(Result<(double Distance, double Duration)>.Success((34.5, 65.6)));
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(Result<IEnumerable<GetRideBookingDto>>.Failure("", HttpStatusCode.Conflict));

            //Act
            var result = await this.rideService.UpdateRideVehicleAsync(userId, Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        [Test]
        public async Task UodateRideVehicleAsync_Success()
        {
            //Arrange
            var driverId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var rideId = Guid.NewGuid();

            var pickUpAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Byala Slatina",
                Region = "Vratsa"
            };

            var dropOffAddress = new Address()
            {
                Country = "Bulgaria",
                City = "Burgas",
                Region = "Burgas"
            };

            var vehicle = new GetVehicleDto()
            {
                LicensePlate = "BP 1914 SF"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120),
                DriverId = userId,
                Vehicle = new Vehicle() { PlateNumber = "BP 1914 SF" }
            };

            var user = new User()
            {
                Id = userId,
                Email = "test@test.com"
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var getRide = new GetRideDto()
            {
                Id = rideId,
                StartAddress = pickUpAddress,
                EndAddress = dropOffAddress,
            };

            var getRideBook = new GetRideBookingDto()
            {
                Passenger = passenger
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.vehicleService.Setup(x => x.GetVehicleAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<GetVehicleDto>.Success(vehicle));
            this.rideRepository.Setup(x => x.UpdateAsync(It.IsAny<Ride>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(getRide);
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(Result<(double Distance, double Duration)>.Success((34.5, 65.6)));
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { getRideBook }));

            //Act
            var result = await this.rideService.UpdateRideVehicleAsync(userId, Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region UpdateRideAsync
        [Test]
        public async Task UpdateRideAsync_Conflict_InvalidTime()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(-10)
            };

            //Act
            var result = await this.rideService.UpdateRideAsync(Guid.NewGuid(), Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideAsync_NotFound_NotRide()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100)
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.UpdateRideAsync(Guid.NewGuid(), Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideAsync_Conflict_CanNotChange_AfterStart()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100)
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(-10)
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);

            //Act
            var result = await this.rideService.UpdateRideAsync(Guid.NewGuid(), Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideAsync_NotFound_CanNotGetUserRoles()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100)
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(50)
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.UpdateRideAsync(Guid.NewGuid(), Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideAsync_Unauthorized_UserNoOrganizer()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100)
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = driverId
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));

            //Act
            var result = await this.rideService.UpdateRideAsync(Guid.NewGuid(), Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideAsync_Verify_LessThanOneHour_BeforStart()
        {
            //Arrange
            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100)
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));

            //Act
            var result = await this.rideService.UpdateRideAsync(userId, Guid.NewGuid(), newRide);

            //Assert
            this.feedbackService.Verify(x => x.DeleteDriverRatingAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Test]
        public async Task UpdateRideAsync_BadRequest_CanNotGetFreeSpace()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100),
                FreeSpots = 1
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 2
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Failure("", HttpStatusCode.BadRequest));

            //Act
            var result = await this.rideService.UpdateRideAsync(userId, Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideAsync_BadRequest_PlaceCanNotBeLessThanAprovedPassengers()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100),
                FreeSpots = 1
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 3
            };

            var user = new User()
            {
                Id = userId
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(1));

            //Act
            var result = await this.rideService.UpdateRideAsync(userId, Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideAsync_Conflict_UpdateFailed()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;

            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100),
                FreeSpots = null
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 3
            };

            var user = new User()
            {
                Id = userId
            };

            var getRide = new GetRideDto()
            {
                Id = rideId,
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(1));
            this.rideRepository.Setup(x => x.UpdateAsync(It.IsAny<Ride>())).ReturnsAsync(() => null);

            //Act
            var result = await this.rideService.UpdateRideAsync(userId, Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideAsync_Conflict_GetDistanceFailed()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;

            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100),
                FreeSpots = null
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 3
            };

            var user = new User()
            {
                Id = userId
            };

            var getRide = new GetRideDto()
            {
                Id = rideId,
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(1));
            this.rideRepository.Setup(x => x.UpdateAsync(It.IsAny<Ride>())).ReturnsAsync(() => ride);
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => getRide);
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Failure("", HttpStatusCode.Conflict));

            //Act
            var result = await this.rideService.UpdateRideAsync(userId, Guid.NewGuid(), newRide);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task UpdateRideAsync_Success_NoAffectedPeople()
        {
            //Arrange
            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100),
                FreeSpots = null
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                FreeSpots = 3
            };

            var user = new User()
            {
                Id = userId
            };

            var getRide = new GetRideDto()
            {
                Id = rideId,
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(1));
            this.rideRepository.Setup(x => x.UpdateAsync(It.IsAny<Ride>())).ReturnsAsync(() => ride);
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => getRide);
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.5, 54.6)));
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.rideService.UpdateRideAsync(userId, Guid.NewGuid(), newRide);

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        [Test]
        public async Task UpdateRideAsync_Success()
        {
            //Arrange
            var rideId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var driverId = Guid.NewGuid();

            var newRide = new UpdateRideDto()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(100),
                FreeSpots = null
            };

            var passenger = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            var user = new User()
            {
                Id = userId,
                Email = "tets@test.com"
            };

            var ride = new Ride()
            {
                Id = rideId,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                DriverId = userId,
                Driver = user,
                FreeSpots = 3
            };

            var getRide = new GetRideDto()
            {
                Id = rideId,
            };

            var getRideBooks = new GetRideBookingDto()
            {
                Passenger = passenger
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(), It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.bookService.Setup(x => x.GetFreePlacesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<int>.Success(1));
            this.rideRepository.Setup(x => x.UpdateAsync(It.IsAny<Ride>())).ReturnsAsync(() => ride);
            this.mapper.Setup(x => x.Map<GetRideDto>(It.IsAny<Ride>())).Returns(() => getRide);
            this.geoMapService.Setup(x => x.GetDistanceAndDurationAsync(It.IsAny<Address>(), It.IsAny<Address>())).ReturnsAsync(() => Result<(double Distance, double Duration)>.Success((34.5, 54.6)));
            this.bookService.Setup(x => x.GetBookingsAsync(It.IsAny<FindBookingQuery>())).ReturnsAsync(() => Result<IEnumerable<GetRideBookingDto>>.Success(new List<GetRideBookingDto>() { getRideBooks }));
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(passenger);

            //Act
            var result = await this.rideService.UpdateRideAsync(userId, Guid.NewGuid(), newRide);

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion
    }
}
