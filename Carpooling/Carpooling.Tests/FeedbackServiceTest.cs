﻿namespace Carpooling.Tests
{
    [TestFixture]
    public class FeedbackServiceTest
    {
        private Mock<IRepository<Feedback, Feedback>> feedbackRepository;
        private Mock<IRepository<Feedback, GetGivenFeedbackDto>> givenDtoRepository;
        private Mock<IRepository<Feedback, GetRecivedFeedbackDto>> recivedDtoRepositoryFeedback;
        private Mock<IUserService> userService;
        private Mock<IMapper> mapper;

        private FeedbackService feedbackService;

        [SetUp]
        public void Setup()
        {
            this.feedbackRepository = new Mock<IRepository<Feedback, Feedback>>();
            this.givenDtoRepository = new Mock<IRepository<Feedback, GetGivenFeedbackDto>>();
            this.recivedDtoRepositoryFeedback = new Mock<IRepository<Feedback, GetRecivedFeedbackDto>>();
            this.userService = new Mock<IUserService>();
            this.mapper = new Mock<IMapper>();

            this.feedbackService = new FeedbackService(feedbackRepository.Object, recivedDtoRepositoryFeedback.Object, givenDtoRepository.Object, userService.Object, mapper.Object);
        }

        [Test]
        public async Task CreateFeedbackAsync_HttpStatusCode_Conflick_NoRecipientId()
        {
            //Arrange
            HttpStatusCode expected = HttpStatusCode.NotFound;

            CreatFeedbackDto feedBack = new CreatFeedbackDto()
            {
                ReciverId = Guid.NewGuid(),

                Comment = "Test comment",

                Rating = 3,

                Type = FeedbackType.Passenger
            };

            this.userService.Setup(x => x.GetUserDetailsAsync(feedBack.ReciverId)).ReturnsAsync(() => Result<User>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound));

            //Act
            var result = await this.feedbackService.CreateFeedbackAsync(Guid.NewGuid(), feedBack);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateFeedbackAsync_HttpStatusCode_Conflick_RatingGreaterThanFive()
        {
            //Arrange
            HttpStatusCode expected = HttpStatusCode.Conflict;

            CreatFeedbackDto feedBack = new CreatFeedbackDto()
            {
                ReciverId = Guid.NewGuid(),

                Comment = "Test comment",

                Rating = 6,

                Type = FeedbackType.Passenger
            };

            this.userService.Setup(x => x.GetUserDetailsAsync(feedBack.ReciverId)).ReturnsAsync(() => Result<User>.Success(new User()));

            //Act
            var result = await this.feedbackService.CreateFeedbackAsync(Guid.NewGuid(), feedBack);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateFeedbackAsync_HttpStatusCode_Conflick_RatingLessThanZero()
        {
            //Arrange
            HttpStatusCode expected = HttpStatusCode.Conflict;

            CreatFeedbackDto feedBack = new CreatFeedbackDto()
            {
                ReciverId = Guid.NewGuid(),

                Comment = "Test comment",

                Rating = -1,

                Type = FeedbackType.Passenger
            };

            this.userService.Setup(x => x.GetUserDetailsAsync(feedBack.ReciverId)).ReturnsAsync(() => Result<User>.Success(new User()));

            //Act
            var result = await this.feedbackService.CreateFeedbackAsync(Guid.NewGuid(), feedBack);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateFeedbackAsync_IsSuccess()
        {
            //Arrange
            CreatFeedbackDto feedBack = new CreatFeedbackDto()
            {
                ReciverId = Guid.NewGuid(),

                Comment = "Test comment",

                Rating = 3,

                Type = FeedbackType.Passenger
            };

            this.userService.Setup(x => x.GetUserDetailsAsync(feedBack.ReciverId)).ReturnsAsync(() => Result<User>.Success(new User()));
            this.feedbackRepository.Setup(x => x.CreateAsync(It.IsAny<Feedback>())).ReturnsAsync(new Feedback());

            //Act
            var result = await this.feedbackService.CreateFeedbackAsync(Guid.NewGuid(), feedBack);

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        [Test]
        public async Task DeleteDriverRatingAsync_NotFound_WhenNoFeedbak()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            this.feedbackRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>()))
                .ReturnsAsync(new List<Feedback>());
            this.feedbackRepository.Setup(x => x.UpdateAsync(It.IsAny<Feedback>())).ReturnsAsync(new Feedback());

            //Act
            var result = await this.feedbackService.DeleteDriverRatingAsync(Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteDriverRatingAsync_Success()
        {
            //Arrange
            this.feedbackRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>()))
                .ReturnsAsync(new List<Feedback>() { new Feedback() { Rating = 4 } });
            this.feedbackRepository.Setup(x => x.UpdateAsync(It.IsAny<Feedback>())).ReturnsAsync(new Feedback());

            //Act
            var result = await this.feedbackService.DeleteDriverRatingAsync(Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        [Test]
        public async Task DeletePassengerRatingAsync_NotFound_WhenNoFeedbak()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            this.feedbackRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>()))
                .ReturnsAsync(new List<Feedback>());
            this.feedbackRepository.Setup(x => x.UpdateAsync(It.IsAny<Feedback>())).ReturnsAsync(new Feedback());

            //Act
            var result = await this.feedbackService.DeletePassengerRatingAsync(Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeletePassengerRatingAsync_Success()
        {
            //Arrange
            this.feedbackRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>()))
                .ReturnsAsync(new List<Feedback>() { new Feedback() { Rating = 4 } });
            this.feedbackRepository.Setup(x => x.UpdateAsync(It.IsAny<Feedback>())).ReturnsAsync(new Feedback());

            //Act
            var result = await this.feedbackService.DeletePassengerRatingAsync(Guid.NewGuid());

            //Assert
            this.feedbackRepository.Verify(x => x.UpdateAsync(It.IsAny<Feedback>()), Times.Once);
        }

        [Test]
        public async Task GetRecivedFeedbackAsync_HttpStatusCode_Conflick_NoRecipientId()
        {
            //Arrange
            HttpStatusCode expected = HttpStatusCode.Conflict;

            //Act
            var result = await this.feedbackService.GetRecivedFeedbacksAsync(new FindFeedbackQuery());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetRecivedFeedbackAsync_Feedback_Not_Found()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            Guid reciverId = Guid.NewGuid();

            Feedback feedback = new Feedback()
            {
                Rating = 4,
                ReceiverId = reciverId
            };

            GetRecivedFeedbackDto feedbackDto = new GetRecivedFeedbackDto()
            {
                Rating = 4,
                AuthorName = "Test",
            };

            this.recivedDtoRepositoryFeedback.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, GetRecivedFeedbackDto>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>())).ReturnsAsync(() => null);

            //Act
            var result = await this.feedbackService.GetRecivedFeedbacksAsync(new FindFeedbackQuery() { RecipientId = reciverId });

            //Assert
            Assert.That(expected, Is.EqualTo(expected));
        }

        [Test]
        public async Task GetRecivedFeedbackAsync_Success()
        {
            //Arrange
            Guid reciverId = Guid.NewGuid();

            Feedback feedback = new Feedback()
            {
                Rating = 4,
                ReceiverId = reciverId
            };

            GetRecivedFeedbackDto feedbackDto = new GetRecivedFeedbackDto()
            {
                Rating = 4,
                AuthorName = "Test",
            };

            string expected = feedbackDto.AuthorName;

            this.recivedDtoRepositoryFeedback.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, GetRecivedFeedbackDto>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>())).ReturnsAsync(new List<GetRecivedFeedbackDto>() { feedbackDto });

            //Act
            var result = await this.feedbackService.GetRecivedFeedbacksAsync(new FindFeedbackQuery() { RecipientId = reciverId });

            //Assert
            Assert.That(expected, Is.EqualTo(result.Value.FirstOrDefault(x => x.AuthorName == feedbackDto.AuthorName).AuthorName));
        }

        [Test]
        public async Task GetGivenFeedbackAsync_HttpStatusCode_Conflick_NoSenderId()
        {
            //Arrange
            HttpStatusCode expected = HttpStatusCode.Conflict;

            //Act
            var result = await this.feedbackService.GetGivenFeedbacksAsync(new FindFeedbackQuery());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetGivenFeedbackAsync_FeedbackNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            Guid senderId = Guid.NewGuid();

            Feedback feedback = new Feedback()
            {
                Rating = 4,
                SenderId = senderId
            };

            GetGivenFeedbackDto feedbackDto = new GetGivenFeedbackDto()
            {
                Rating = 4,
                RecipientName = "Test",
            };

            this.givenDtoRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, GetGivenFeedbackDto>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>())).ReturnsAsync(new List<GetGivenFeedbackDto>());

            //Act
            var result = await this.feedbackService.GetGivenFeedbacksAsync(new FindFeedbackQuery() { SenderId = senderId });

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetGivenFeedbackAsync_Success()
        {
            //Arrange
            Guid senderId = Guid.NewGuid();

            Feedback feedback = new Feedback()
            {
                Rating = 4,
                SenderId = senderId
            };

            GetGivenFeedbackDto feedbackDto = new GetGivenFeedbackDto()
            {
                Rating = 4,
                RecipientName = "Test",
            };

            string expected = feedbackDto.RecipientName;

            this.givenDtoRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, GetGivenFeedbackDto>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>())).ReturnsAsync(new List<GetGivenFeedbackDto>() { feedbackDto });

            //Act
            var result = await this.feedbackService.GetGivenFeedbacksAsync(new FindFeedbackQuery() { SenderId = senderId });

            //Assert
            Assert.That(expected, Is.EqualTo(result.Value.FirstOrDefault(x => x.RecipientName == feedbackDto.RecipientName).RecipientName));
        }

        [Test]
        public async Task GetRatingAsync_FeedbackNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            Guid userId = Guid.NewGuid();

            Feedback feedback = new Feedback()
            {
                ReceiverId = userId,
                Rating = 4,
                Type = FeedbackType.Driver
            };

            Feedback feedback1 = new Feedback()
            {
                ReceiverId = userId,
                Rating = 4,
                Type = FeedbackType.Driver
            };

            this.feedbackRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>())).ReturnsAsync(() => null);

            //Act
            var result = await this.feedbackService.GetRatingAsync(userId, FeedbackType.Driver);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetRatingAsync_Success()
        {
            //Arrange
            double expected = 4.0;

            Guid userId = Guid.NewGuid();

            Feedback feedback = new Feedback()
            {
                ReceiverId = userId,
                Rating = 4,
                Type = FeedbackType.Driver
            };

            Feedback feedback1 = new Feedback()
            {
                ReceiverId = userId,
                Rating = 4,
                Type = FeedbackType.Driver
            };

            this.feedbackRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<Feedback, Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>>>(),
                It.IsAny<Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>>>(), It.IsAny<PageParameters>())).ReturnsAsync(new List<Feedback>() { feedback, feedback1 });

            //Act
            var result = await this.feedbackService.GetRatingAsync(userId, FeedbackType.Driver);

            //Assert
            Assert.That(expected, Is.EqualTo(result.Value));
        }

        [Test]
        public async Task DeleteFeetbackAsync_UserNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound));
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Failure(" ", HttpStatusCode.NotFound));

            //Act
            var result = await this.feedbackService.DeleteFeedbackAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteFeetbackAsync_HttpStatusCode_Unauthorized_UserNoAministrator()
        {
            //Arrange
            var expected = HttpStatusCode.Unauthorized;
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));

            //Act
            var result = await this.feedbackService.DeleteFeedbackAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteFeetbackAsync_FeedbackNotFound()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.Administrator }));
            this.feedbackRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Feedback, Feedback>>>(), null)).ReturnsAsync((Feedback)null);

            //Act
            var result = await this.feedbackService.DeleteFeedbackAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteFeetbackAsync_True_Success()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));
            this.feedbackRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Feedback, Feedback>>>(), null)).ReturnsAsync(new Feedback());
            this.feedbackRepository.Setup(x => x.SoftDeleteAsync(It.IsAny<Feedback>())).ReturnsAsync(true);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.Administrator }));

            //Act
            var result = await this.feedbackService.DeleteFeedbackAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(result.Value, Is.True);
        }

        [Test]
        public async Task GetItemsCountAfterFiltesAsync()
        {
            //Arrange
            var expected = 2;

            this.feedbackRepository.Setup(x => x.GetItemsCountAfterFiltersAsync(It.IsAny<Func<IQueryable<Feedback>, IQueryable<Feedback>>>())).ReturnsAsync(() => 2);

            //Act
            var result = await this.feedbackService.GetItemsCountAfterFiltesAsync(new FindFeedbackQuery());

            //Assert
            Assert.That(expected,Is.EqualTo(result.Value));
        }
    }
}

