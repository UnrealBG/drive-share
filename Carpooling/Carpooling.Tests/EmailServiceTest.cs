﻿namespace Carpooling.Tests
{
    using MimeKit;

    [TestFixture]
    public class EmailServiceTest
    {
        private Mock<IEmailClient> client;

        private IEmailService emailService;

        [SetUp]
        public void Setup()
        {
            this.client = new Mock<IEmailClient>();

            this.emailService = new EmailService(this.client.Object);
        }

        [Test]
        public void CraetBookingMessage_Success()
        {
            //Arrange
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = Path.Combine(baseDir, "EmailResponseHtml", "Images", "journeytogether.jpg");
            var operationName = "Ride Booking";
            var emailTo = "testmail@test.com";

            GetUserDto fromUser = new GetUserDto()
            {
                Name = "Nedko Nedkov",
                UserName = "nedko_nedkov",
                Email = "nedkos@test.com",
                PhoneNumber = "1234567890",
                ImgUrl = filePath,
                PassengerRating = 2.3
            };

            User user = new User()
            {
                FirstName = "Nedko",
                LastName = "Nedkov",
                Username = "nedko_nedkov",
                Email = "nedkos@test.com",
                PhoneNumber = "1234567890",
                ProfileImageUrl = filePath
            };

            Ride ride = new Ride()
            {
                StartAddress = new Address
                {
                    City = "Byala Slatina",
                    Country = "Bulgaria",
                    Region = "Vratsa"
                },
                EndAddress = new Address
                {
                    City = "Burgas",
                    Country = "Bulgaria",
                    Region = "Burgas"
                }
            };

            RideBooking booking = new RideBooking()
            {
                UserId = Guid.NewGuid(),
                CreatedOn = DateTime.UtcNow,
                User = user,
                Ride = ride
            };

            this.client.Setup(x => x.EmailFrom).Returns("test@test.com");
            this.client.Setup(x => x.AppLink).Returns("AppLink.com");

            //Act
            var result = this.emailService.CreateBookingMessage(operationName, emailTo, fromUser, booking);

            //Assert
            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void CancelingRideMessage_Success()
        {
            //Arrange
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = Path.Combine(baseDir, "EmailResponseHtml", "Images", "journeytogether.jpg");
            var operationName = "Ride Booking";
            var emailTo = new List<string>() { "testmail1@test.com", "testmail2@test.com" };

            GetUserDto fromUser = new GetUserDto()
            {
                Name = "Nedko Nedkov",
                UserName = "nedko_nedkov",
                Email = "nedkos@test.com",
                PhoneNumber = "1234567890",
                ImgUrl = filePath,
                PassengerRating = 2.3
            };

            User user = new User()
            {
                FirstName = "Nedko",
                LastName = "Nedkov",
                Username = "nedko_nedkov",
                Email = "nedkos@test.com",
                PhoneNumber = "1234567890",
                ProfileImageUrl = filePath
            };

            Ride ride = new Ride()
            {
                Id = Guid.NewGuid(),
                StartAddress = new Address
                {
                    City = "Byala Slatina",
                    Country = "Bulgaria",
                    Region = "Vratsa"
                },
                EndAddress = new Address
                {
                    City = "Burgas",
                    Country = "Bulgaria",
                    Region = "Burgas"
                }
            };

            GetUserDto userDto = new GetUserDto()
            {
                Name = user.FirstName + " " + user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                ImgUrl = user.ProfileImageUrl,
                DriverRating = 3.3
            };

            this.client.Setup(x => x.EmailFrom).Returns("test@test.com");
            this.client.Setup(x => x.AppLink).Returns("AppLink.com");

            //Act
            var result = this.emailService.CreateRideMessage("operation", emailTo, "oldValue", "newValue", ride.Id, userDto);

            //Assert
            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public async Task SendEmailAsync_Verify_Send()
        {
            //Act
            this.emailService.SendEmailAsync(new MimeMessage());

            //Assert
            this.client.Verify(x => x.SendAsync(It.IsAny<MimeMessage>()), Times.Once);
        }
    }
}

