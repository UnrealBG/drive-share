
global using System.Net;
global using System.Linq.Expressions;

global using AutoMapper;

global using Carpooling.Models.DtoModels.GetDtos;
global using Carpooling.Models.DtoModels.UpdateDtos;
global using Carpooling.Models.Enums;
global using Carpooling.Models.QueryModels;
global using Carpooling.Services.Contracts;
global using Carpooling.DataAccess.Repository.Contracts;
global using Carpooling.Models.DtoModels.CreateDtos;
global using Carpooling.Models.Entities;
global using Carpooling.Services;
global using Carpooling.Services.Helpers;

global using Microsoft.Extensions.Configuration;

global using Moq;

global using Microsoft.EntityFrameworkCore.Query;

global using NUnit.Framework;

global using static Carpooling.Models.GlobalConstants.ResultMessages;