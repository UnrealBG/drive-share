﻿namespace Carpooling.Tests
{
    using Carpooling.Services.Helpers;

    using MimeKit;

    using Moq;

    [TestFixture]
    public class BookServiceTest
    {
        private Mock<IRepository<RideBooking, RideBooking>> rideBookingRepository;
        private Mock<IRepository<RideBooking, GetRideBookingDto>> getRideBookingDtoRepository;
        private Mock<IRepository<Ride, Ride>> rideRepository;
        private Mock<IEmailService> emailService;
        private Mock<IMapper> mapper;
        private Mock<IUserService> userService;
        private Mock<IFeedbackService> feedbackService;

        private IBookService bookService;

        [SetUp]
        public void Setup()
        {
            this.userService = new Mock<IUserService>();
            this.rideBookingRepository = new Mock<IRepository<RideBooking, RideBooking>>();
            this.getRideBookingDtoRepository = new Mock<IRepository<RideBooking, GetRideBookingDto>>();
            this.rideRepository = new Mock<IRepository<Ride, Ride>>();
            this.emailService = new Mock<IEmailService>();
            this.mapper = new Mock<IMapper>();
            this.feedbackService = new Mock<IFeedbackService>();

            this.bookService = new BookService(rideBookingRepository.Object, getRideBookingDtoRepository.Object, rideRepository.Object, emailService.Object, userService.Object, feedbackService.Object, mapper.Object);
        }

        #region CreateBookingAsync

        [Test]
        public async Task CreateBookingAsync_InvalidInput_InvalidRideId()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;
            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>()))
                .ReturnsAsync(() => null);

            CreateBookingDto bookingDto = new CreateBookingDto()
            {
                RideId = Guid.NewGuid(),
                BookPlaces = 1
            };

            //Act
            var result = await this.bookService.CreateBookingAsync(Guid.NewGuid(), bookingDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateBookingAsync_BadRequest_CanNotGetFreePlaces()
        {
            //Arrange
            var expected = HttpStatusCode.BadRequest;

            var book = new RideBooking()
            {
                BookedPlaces = 3
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>()))
                .ReturnsAsync(() => new Ride() { FreeSpots = 2 });
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null)).ReturnsAsync(() => new List<RideBooking>() { book });

            CreateBookingDto bookingDto = new CreateBookingDto()
            {
                RideId = Guid.NewGuid(),
                BookPlaces = 1
            };

            //Act
            var result = await this.bookService.CreateBookingAsync(Guid.NewGuid(), bookingDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateBookingAsync_Conflict_CanNotBookMoreOfFreePlaces()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;

            var book = new RideBooking()
            {
                BookedPlaces = 1
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>()))
                .ReturnsAsync(() => new Ride() { FreeSpots = 2 });
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null)).ReturnsAsync(() => new List<RideBooking>() { book });

            CreateBookingDto bookingDto = new CreateBookingDto()
            {
                RideId = Guid.NewGuid(),
                BookPlaces = 2
            };

            //Act
            var result = await this.bookService.CreateBookingAsync(Guid.NewGuid(), bookingDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task CreateBookingAsync_Success()
        {
            //Arrange
            var book = new RideBooking()
            {
                BookedPlaces = 1,
            };

            CreateBookingDto bookingDto = new CreateBookingDto()
            {
                RideId = Guid.NewGuid(),
                BookPlaces = 1,
            };

            var getBookDto = new GetRideBookingDto()
            {
                Passenger = new GetUserDto()
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>()))
                .ReturnsAsync(() => new Ride() { FreeSpots = 2, Driver = new User() { Email = "dfsdf" } });
            this.rideBookingRepository.Setup(x => x.CreateAsync(It.IsAny<RideBooking>())).ReturnsAsync(new RideBooking() { Ride = new Ride() { Driver = new User() { Email = "dfsd@5345.com" } } });
            this.getRideBookingDtoRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, GetRideBookingDto>>>(), null)).ReturnsAsync(new GetRideBookingDto());
            this.emailService.Setup(x => x.CreateBookingMessage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GetUserDto>(), It.IsAny<RideBooking>())).Returns(new MimeKit.MimeMessage());
            this.emailService.Setup(x => x.SendEmailAsync(It.IsAny<MimeMessage>()));
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null)).ReturnsAsync(() => new List<RideBooking>() { book });
            book.Ride = new Ride() { Driver = new User() { Email = "dsadas@asdasd.com" } };
            this.mapper.Setup(x => x.Map<GetRideBookingDto>(It.IsAny<RideBooking>())).Returns(getBookDto);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(new User()));

            //Act
            var result = await this.bookService.CreateBookingAsync(Guid.NewGuid(), bookingDto);

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region DeleteBookingAsync
        [Test]
        public async Task DeleteBookingAsync_InvalidInput_InvalidBookId()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;
            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync((RideBooking)null);

            //Act
            var result = await this.bookService.DeleteBookingAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteBookingAsync_UnathorizedOperation_InvalidUserRole()
        {
            //Arrange
            var expected = HttpStatusCode.Unauthorized;
            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(new RideBooking());
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.User }));

            //Act
            var result = await this.bookService.DeleteBookingAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteBookingAsync_UnathorizedOperation_InvalidUserId()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;
            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(new RideBooking());
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Failure("", HttpStatusCode.NotFound));

            //Act
            var result = await this.bookService.DeleteBookingAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task DeleteBookingAsync_ResultSuccess()
        {
            //Arrange            
            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(new RideBooking());
            this.rideBookingRepository.Setup(x => x.SoftDeleteAsync(It.IsAny<RideBooking>())).ReturnsAsync(true);
            this.userService.Setup(x => x.GetUserRolesAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<List<UserRoles>>.Success(new List<UserRoles>() { UserRoles.Administrator }));

            //Act
            var result = await this.bookService.DeleteBookingAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region GetBookingDetails
        [Test]
        public async Task GetBookingDetails_InvalidInput_InvalidBookId()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;
            this.mapper.Setup(x => x.Map<GetRideBookingDto>(It.IsAny<Guid>())).Returns(new GetRideBookingDto());
            this.getRideBookingDtoRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, GetRideBookingDto>>>(), null)).ReturnsAsync((GetRideBookingDto)null);

            //Act
            var result = await this.bookService.GetBookingDetailsAsync(Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetBookingDetails_ResultSuccess()
        {
            //Arrange
            this.mapper.Setup(x => x.Map<GetRideBookingDto>(It.IsAny<Guid>())).Returns(new GetRideBookingDto());
            this.getRideBookingDtoRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, GetRideBookingDto>>>(), null)).ReturnsAsync(new GetRideBookingDto());

            //Act
            var result = await this.bookService.GetBookingDetailsAsync(Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region GetBookingAsync
        [Test]
        public async Task GetBookingAsync_Booking_Not_Found()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;
            this.mapper.Setup(x => x.Map<GetRideBookingDto>(It.IsAny<Guid>())).Returns(new GetRideBookingDto());
            this.getRideBookingDtoRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<RideBooking, GetRideBookingDto>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(), It.IsAny<PageParameters>()))
                .ReturnsAsync(() => null);

            //Act
            var result = await this.bookService.GetBookingsAsync(new FindBookingQuery() { Status = BookingStatus.Аpproved });

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetBookingAsync_ResultSuccess()
        {
            //Arrange
            this.mapper.Setup(x => x.Map<GetRideBookingDto>(It.IsAny<Guid>())).Returns(new GetRideBookingDto());
            this.getRideBookingDtoRepository.Setup(x => x.GetAsync(It.IsAny<Expression<Func<RideBooking, GetRideBookingDto>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(), It.IsAny<PageParameters>())).ReturnsAsync(() => new List<GetRideBookingDto>() { new GetRideBookingDto() { Status = BookingStatus.Аpproved } });

            //Act
            var result = await this.bookService.GetBookingsAsync(new FindBookingQuery() { Status = BookingStatus.Аpproved });

            //Assert
            Assert.That(result, Is.Not.Null);
        }
        #endregion

        [Test]
        public async Task GetItemsCountAfterFiltesAsync_Success()
        {
            //Arrange
            this.rideBookingRepository.Setup(x => x.GetItemsCountAfterFiltersAsync(It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>())).ReturnsAsync(3);

            //Act
            var result = await this.bookService.GetItemsCountAfterFiltesAsync(new FindBookingQuery());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        #region GetFreePlacesAsync
        [Test]
        public async Task GetFreePlacesAsync_NotFound_Ride()
        {
            //Arrange
            var expected = HttpStatusCode.NotFound;

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => null);

            //Act
            var result = await this.bookService.GetFreePlacesAsync(Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetFreePlacesAsync_Success_NoApprovedBooks()
        {
            //Arrange
            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => new Ride());
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null
                )).ReturnsAsync(() => null);

            //Act
            var result = await this.bookService.GetFreePlacesAsync(Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        [Test]
        public async Task GetFreePlacesAsync_Conflict_BadResult()
        {
            //Arrange
            var expected = HttpStatusCode.Conflict;

            var ride = new Ride()
            {
                FreeSpots = 2
            };

            var book = new RideBooking()
            {
                BookedPlaces = 3
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null
                )).ReturnsAsync(() => new List<RideBooking>() { book });

            //Act
            var result = await this.bookService.GetFreePlacesAsync(Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task GetFreePlacesAsync_Success()
        {
            //Arrange
            var ride = new Ride()
            {
                FreeSpots = 2
            };

            var book = new RideBooking()
            {
                BookedPlaces = 1
            };

            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => ride);
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null
                )).ReturnsAsync(() => new List<RideBooking>() { book });

            //Act
            var result = await this.bookService.GetFreePlacesAsync(Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region BookingCancelationAsync
        [Test]
        public async Task BookingCancelationAsync_Conflict_BookNoExists()
        {
            //Arrenge
            var expected = HttpStatusCode.Conflict;

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => null);

            //Act
            var result = await this.bookService.BookingCancelationAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task BookingCancelationAsync_BadRequest_UserNoAuthorOfBooking()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var book = new RideBooking()
            {
                UserId = user.Id
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.BookingCancelationAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task BookingCancelationAsync_BadRequest_BookStatus_Is_Cancled()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Canceled
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.BookingCancelationAsync(user.Id, Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task BookingCancelationAsync_BadRequest_BookStatus_Is_Rejected()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Rejected
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.BookingCancelationAsync(user.Id, Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task BookingCancelationAsync_BadRequest_Canceletion_After_DepartureTime()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(-5)
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Аpproved,
                Ride = ride
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.BookingCancelationAsync(user.Id, Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task BookingCancelationAsync_Verify_DeletePassengerRating()
        {
            //Arrenge
            var user = new User()
            {
                Id = Guid.NewGuid(),
                Email = "test@test.com"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                Driver = user
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Аpproved,
                Ride = ride
            };

            var getUSer = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(getUSer);

            //Act
            var result = await this.bookService.BookingCancelationAsync(user.Id, Guid.NewGuid());

            //Assert
            this.feedbackService.Verify(x => x.DeletePassengerRatingAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Test]
        public async Task BookingCancelationAsync_Success()
        {
            //Arrenge
            var user = new User()
            {
                Id = Guid.NewGuid(),
                Email = "test@test.com"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                Driver = user
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Аpproved,
                Ride = ride
            };

            var getUSer = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(getUSer);

            //Act
            var result = await this.bookService.BookingCancelationAsync(user.Id, Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region RejectBookingAsync
        [Test]
        public async Task RejectBookingAsync_Conflict_BookNoExists()
        {
            //Arrenge
            var expected = HttpStatusCode.Conflict;

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => null);

            //Act
            var result = await this.bookService.RejectBookingAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task RejectBookingAsync_BadRequest_UserNoDriverOfRide()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DriverId = user.Id
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Ride = ride
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.RejectBookingAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task RejectBookingAsync_BadRequest_BookStatus_Is_Cancled()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DriverId = user.Id
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Canceled,
                Ride = ride
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.RejectBookingAsync(user.Id, Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task RejectBookingAsync_BadRequest_BookStatus_Is_Rejected()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DriverId = user.Id
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Rejected,
                Ride = ride
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.RejectBookingAsync(user.Id, Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task RejectBookingAsync_BadRequest_Canceletion_After_DepartureTime()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DriverId = user.Id,
                DepartureTime = DateTime.UtcNow.AddMinutes(-5)
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Аpproved,
                Ride = ride
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.RejectBookingAsync(user.Id, Guid.NewGuid());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task RejectBookingAsync_Verify_DeletePassengerRating()
        {
            //Arrenge
            var user = new User()
            {
                Id = Guid.NewGuid(),
                Email = "test@test.com"
            };

            var ride = new Ride()
            {
                DriverId = user.Id,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                Driver = user
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                User = user,
                Status = BookingStatus.Аpproved,
                Ride = ride
            };

            var getUSer = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(getUSer);

            //Act
            var result = await this.bookService.RejectBookingAsync(user.Id, Guid.NewGuid());

            //Assert
            this.feedbackService.Verify(x => x.DeleteDriverRatingAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Test]
        public async Task RejectBookingAsync_Success()
        {
            //Arrenge
            var user = new User()
            {
                Id = Guid.NewGuid(),
                Email = "test@test.com"
            };

            var ride = new Ride()
            {
                DriverId = user.Id,
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                Driver = user
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                User = user,
                Status = BookingStatus.Аpproved,
                Ride = ride
            };

            var getUSer = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(getUSer);

            //Act
            var result = await this.bookService.RejectBookingAsync(user.Id, Guid.NewGuid());

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion

        #region ChangeBookedPassengersAsync
        [Test]
        public async Task ChangeBookedPassengersAsync_Conflict_BookNoExists()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => null);

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(Guid.NewGuid(), new UpdateBookingDto());

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task ChangeBookedPassengersAsync_BadRequest_UserNoAuthorOfBooking()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Ride = ride
            };

            var updateDto = new UpdateBookingDto()
            {
                BookingId = book.Id,
                BookedPlaces = 1
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(Guid.NewGuid(), updateDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task ChangeBookedPassengersAsync_BadRequest_BookStatus_Is_Cancled()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Canceled,
                Ride = ride
            };

            var updateDto = new UpdateBookingDto()
            {
                BookingId = book.Id,
                BookedPlaces = 1
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(user.Id, updateDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task ChangeBookedPassengersAsync_BadRequest_BookStatus_Is_Rejected()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(120)
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Rejected,
                Ride = ride
            };

            var updateDto = new UpdateBookingDto()
            {
                BookingId = book.Id,
                BookedPlaces = 1
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(user.Id, updateDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task ChangeBookedPassengersAsync_BadRequest_Canceletion_After_DepartureTime()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(-10)
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Rejected,
                Ride = ride
            };

            var updateDto = new UpdateBookingDto()
            {
                BookingId = book.Id,
                BookedPlaces = 1
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(user.Id, updateDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task ChangeBookedPassengersAsync_Verify_DeletePassengerRating()
        {
            //Arrenge
            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                FreeSpots = 2
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Pending,
                Ride = ride,
                BookedPlaces = 1
            };

            var updateDto = new UpdateBookingDto()
            {
                BookingId = book.Id,
                BookedPlaces = 1
            };

            var getRide = new Ride()
            {
                FreeSpots = 2
            };

            var getUSer = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(getUSer);
            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => getRide);
            this.rideBookingRepository.Setup(x => x.UpdateAsync(It.IsAny<RideBooking>())).ReturnsAsync(() => new RideBooking());
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null
                )).ReturnsAsync(() => new List<RideBooking>() { book });

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(user.Id, updateDto);

            //Assert
            this.feedbackService.Verify(x => x.DeletePassengerRatingAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Test]
        public async Task ChangeBookedPassengersAsync_BadRequest_BookStatusNotApproved_NewPlaces_GreaterThan_FreePlaces()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                FreeSpots = 2
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Pending,
                Ride = ride,
                BookedPlaces = 1
            };

            var updateDto = new UpdateBookingDto()
            {
                BookingId = book.Id,
                BookedPlaces = 3
            };

            var getRide = new Ride()
            {
                FreeSpots = 2
            };

            var getUSer = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(getUSer);
            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => getRide);
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null
                )).ReturnsAsync(() => new List<RideBooking>() { book });

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(user.Id, updateDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task ChangeBookedPassengersAsync_Success_BookStatusNotApproved()
        {
            // Arrenge
            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                FreeSpots = 2
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Pending,
                Ride = ride,
                BookedPlaces = 1
            };

            var updateDto = new UpdateBookingDto()
            {
                BookingId = book.Id,
                BookedPlaces = 1
            };

            var getRide = new Ride()
            {
                FreeSpots = 2
            };

            var getUSer = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(getUSer);
            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => getRide);
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null
                )).ReturnsAsync(() => new List<RideBooking>() { book });
            this.rideBookingRepository.Setup(x => x.UpdateAsync(It.IsAny<RideBooking>())).ReturnsAsync(() => new RideBooking());

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(user.Id, updateDto);

            //Assert
            Assert.That(result.IsSuccess, Is.True);
        }

        [Test]
        public async Task ChangeBookedPassengersAsync_BadRequest_NotEnoughPlaces()
        {
            //Arrenge
            var expected = HttpStatusCode.BadRequest;

            var user = new User()
            {
                Id = Guid.NewGuid()
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                FreeSpots = 2
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Аpproved,
                Ride = ride,
                BookedPlaces = 1
            };

            var updateDto = new UpdateBookingDto()
            {
                BookingId = book.Id,
                BookedPlaces = 3
            };

            var getRide = new Ride()
            {
                FreeSpots = 2
            };

            var getUSer = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(getUSer);
            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => getRide);
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null
                )).ReturnsAsync(() => new List<RideBooking>() { book });

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(user.Id, updateDto);

            //Assert
            Assert.That(expected, Is.EqualTo(result.StatusCode));
        }

        [Test]
        public async Task ChangeBookedPassengersAsync_Success()
        {
            //Arrenge
            var user = new User()
            {
                Id = Guid.NewGuid(),
                Email = "test@test.com"
            };

            var ride = new Ride()
            {
                DepartureTime = DateTime.UtcNow.AddMinutes(50),
                FreeSpots = 2,
                Driver = user
            };

            var book = new RideBooking()
            {
                UserId = user.Id,
                Status = BookingStatus.Аpproved,
                Ride = ride,
                BookedPlaces = 1
            };

            var updateDto = new UpdateBookingDto()
            {
                BookingId = book.Id,
                BookedPlaces = 2
            };

            var getRide = new Ride()
            {
                FreeSpots = 2
            };

            var getUSer = new GetUserDto()
            {
                Name = "Test Test",
                UserName = "test_test",
                Email = "test@test.com",
                PhoneNumber = "+359882026303",
                ImgUrl = null
            };

            this.rideBookingRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<RideBooking, RideBooking>>>(), null)).ReturnsAsync(() => book);
            this.userService.Setup(x => x.GetUserDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => Result<User>.Success(user));
            this.mapper.Setup(x => x.Map<GetUserDto>(It.IsAny<User>())).Returns(getUSer);
            this.rideRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<Expression<Func<Ride, Ride>>>(),
                It.IsAny<Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>>>())).ReturnsAsync(() => getRide);
            this.rideBookingRepository.Setup(x => x.GetAsync(
                It.IsAny<Expression<Func<RideBooking, RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>>>(),
                It.IsAny<Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>>>(),
                null
                )).ReturnsAsync(() => new List<RideBooking>() { book });
            this.rideBookingRepository.Setup(x => x.UpdateAsync(It.IsAny<RideBooking>())).ReturnsAsync(() => new RideBooking());

            //Act
            var result = await this.bookService.ChangeBookedPassengersAsync(user.Id, updateDto);

            //Assert

            Assert.That(result.IsSuccess, Is.True);
        }
        #endregion
    }
}
