﻿namespace Carpooling.Tests
{
    public class VehicleServiceTests
    {
        private Mock<IRepository<Vehicle, Vehicle>> mockRepository;
        private Mock<IMapper> mockMapper;
        private Mock<IUserService> mockUserService;
        private IVehicleService vehicleService;
        private Mock<IRepository<Vehicle, GetVehicleDto>> mockGetVehicleRepo;

        [SetUp]
        public void Setup()
        {
            this.mockRepository = new Mock<IRepository<Vehicle, Vehicle>>();
            this.mockMapper = new Mock<IMapper>();
            this.mockUserService = new Mock<IUserService>();
            this.mockGetVehicleRepo = new Mock<IRepository<Vehicle, GetVehicleDto>>();
            this.vehicleService = new VehicleService(this.mockRepository.Object, this.mockGetVehicleRepo.Object, this.mockMapper.Object, this.mockUserService.Object);
        }

        [Test]
        public async Task CreateVehicleAsync_NullVehicle_ReturnsFailureResult()
        {
            var result = await this.vehicleService.CreateVehicleAsync(Guid.NewGuid(), null);

            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(result.Error, Is.EqualTo(VEHICLE_NOT_FOUND));
            Assert.That(result.IsSuccess, Is.False);
        }

        [Test]
        public async Task CreateVehicleAsync_ValidVehicle_ReturnsSuccessfulResult()
        {
            var dto = new CreateVehicleDto()
            {
                PlateNumber = "CA1234CA"
            };
            var vehicle = new Vehicle()
            {
                PlateNumber = "CA1234CA"
            };
            var getDto = new GetVehicleDto()
            {
                LicensePlate = "CA1234CA"
            };
            this.mockMapper.Setup(x => x.Map<Vehicle>(dto)).Returns(vehicle);
            this.mockRepository.Setup(x => x.CreateAsync(vehicle)).ReturnsAsync(vehicle);
            this.mockMapper.Setup(x => x.Map<GetVehicleDto>(vehicle)).Returns(getDto);

            var result = await this.vehicleService.CreateVehicleAsync(Guid.NewGuid(), dto);

            Assert.That(result.IsSuccess, Is.True);
            Assert.That(dto.PlateNumber, Is.SameAs(result.Value.LicensePlate));
        }

        [Test]
        public async Task UpdateVehicleAsync_NonexistentVehicle_ReturnsFailureResult()
        {
            Guid vehicleId = Guid.NewGuid();
            this.mockRepository.Setup(x => x.GetByIdAsync(vehicleId, v => v, null)).ReturnsAsync((Vehicle)null);

            var result = await this.vehicleService.UpdateVehicleAsync(vehicleId, new CreateVehicleDto());

            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(result.Error, Is.EqualTo(string.Format(VEHICLE_UPDATE_ERROR, vehicleId)));
            Assert.That(result.IsSuccess, Is.False);
        }

        [Test]
        public async Task UpdateVehicleAsync_ValidVehicle_ReturnsSuccessfulResult()
        {
            Guid vehicleId = Guid.NewGuid();
            var dto = new CreateVehicleDto()
            {
                PlateNumber = "CA1234CA"
            };
            var vehicle = new Vehicle()
            {
                PlateNumber = "CA1234CA"
            };
            var getDto = new GetVehicleDto()
            {
                LicensePlate = "CA1234CA"
            };
            this.mockRepository.Setup(x => x.GetByIdAsync(vehicleId, v => v, null)).ReturnsAsync(vehicle);
            this.mockMapper.Setup(x => x.Map(dto, vehicle)).Returns(vehicle);
            this.mockMapper.Setup(x => x.Map<GetVehicleDto>(vehicle)).Returns(getDto);
            this.mockRepository.Setup(x => x.UpdateAsync(vehicle)).ReturnsAsync(vehicle);

            var result = await this.vehicleService.UpdateVehicleAsync(vehicleId, dto);

            Assert.That(result.IsSuccess, Is.True);
            Assert.That(getDto.LicensePlate, Is.SameAs(result.Value.LicensePlate));
        }

        [Test]
        public async Task DeleteVehicleAsync_NonexistentVehicle_ReturnsFailureResult()
        {
            Guid vehicleId = Guid.NewGuid();
            this.mockRepository.Setup(x => x.GetByIdAsync(vehicleId, v => v, null)).ReturnsAsync((Vehicle)null);

            var result = await this.vehicleService.DeleteVehicleAsync(vehicleId);

            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(result.Error, Is.EqualTo(VEHICLE_NOT_FOUND));
            Assert.That(result.IsSuccess, Is.False);
        }

        [Test]
        public async Task DeleteVehicleAsync_ValidVehicle_ReturnsSuccessfulResult()
        {
            Guid vehicleId = Guid.NewGuid();
            var vehicle = new Vehicle();
            this.mockRepository.Setup(x => x.GetByIdAsync(vehicleId, v => v, null)).ReturnsAsync(vehicle);
            this.mockRepository.Setup(x => x.SoftDeleteAsync(vehicle)).ReturnsAsync(true);

            var result = await this.vehicleService.DeleteVehicleAsync(vehicleId);

            Assert.That(result.IsSuccess, Is.True);
            Assert.That(result.Value, Is.True);
        }

        [Test]
        public async Task GetVehicleAsync_NonexistentVehicle_ReturnsFailureResult()
        {
            Guid vehicleId = Guid.NewGuid();
            this.mockRepository.Setup(x => x.GetByIdAsync(vehicleId, v => v, null))
                .ReturnsAsync(((Vehicle)null));

            var result = await this.vehicleService.GetVehicleAsync(vehicleId);

            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(result.Error, Is.EqualTo(VEHICLE_NOT_FOUND));
            Assert.That(result.IsSuccess, Is.False);
        }

        [Test]
        public async Task GetVehicleAsync_ValidVehicle_ReturnsSuccessfulResult()
        {
            Guid vehicleId = Guid.NewGuid();
            var vehicle = new Vehicle()
            {
                PlateNumber = "1914"
            };

            var getDto = new GetVehicleDto()
            {
                LicensePlate = "1914"
            };

            this.mockRepository.Setup(x => x.GetByIdAsync(vehicleId, v => v, null)).ReturnsAsync(vehicle);
            this.mockMapper.Setup(x => x.Map<GetVehicleDto>(vehicle)).Returns(getDto);

            var result = await this.vehicleService.GetVehicleAsync(vehicleId);

            Assert.That(result.IsSuccess, Is.True);
            Assert.That(getDto.LicensePlate, Is.SameAs(result.Value.LicensePlate));
        }

        [Test]
        public async Task GetVehiclesAsync_NonexistentUser_ReturnsFailureResult()
        {
            Guid userId = Guid.NewGuid();
            this.mockRepository.Setup(r => r.GetAsync(
                    It.IsAny<Expression<Func<Vehicle, Vehicle>>>(),
                    It.IsAny<Func<IQueryable<Vehicle>, IQueryable<Vehicle>>>(),
                    It.IsAny<Func<IQueryable<Vehicle>, IOrderedQueryable<Vehicle>>>(),
                    It.IsAny<Func<IQueryable<Vehicle>, IIncludableQueryable<Vehicle, object>>>(),
                    It.IsAny<PageParameters>()
                )).ReturnsAsync(new List<Vehicle>().Where(x => x.UserId == userId && !x.IsDeleted) as ICollection<Vehicle>);

            var result = await this.vehicleService.GetVehiclesAsync(userId);

            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(result.Error, Is.EqualTo(VEHICLE_NOT_FOUND));
            Assert.That(result.IsSuccess, Is.False);
        }

        [Test]
        public async Task GetVehiclesAsync_ValidUser_ReturnsSuccessfulResult()
        {
            Guid userId = Guid.NewGuid();
            var vehicles = new List<Vehicle> { new Vehicle { UserId = userId, PlateNumber = "1914" } };
            var getVehicle = new GetVehicleDto()
            {
                LicensePlate = "1914"
            };
            this.mockGetVehicleRepo.Setup(r => r.GetAsync(
                It.IsAny<Expression<Func<Vehicle, GetVehicleDto>>>(),
                It.IsAny<Func<IQueryable<Vehicle>, IQueryable<Vehicle>>>(),
                It.IsAny<Func<IQueryable<Vehicle>, IOrderedQueryable<Vehicle>>>(),
                It.IsAny<Func<IQueryable<Vehicle>, IIncludableQueryable<Vehicle, object>>>(),
                null)).ReturnsAsync(() => new List<GetVehicleDto>() { getVehicle });

            Result<List<GetVehicleDto>> result = await this.vehicleService.GetVehiclesAsync(userId);

            var expected = vehicles.Count;

            Assert.That(result.IsSuccess, Is.True);
            Assert.That(result.Value.Count, Is.EqualTo(expected));
        }

        [Test]
        public async Task IsPlateNumberExistAsync_ShouldReturnTrue_WhenPlateNumberExists()
        {
            // Arrange
            var plateNumber = "A2288AH";
            var vehicles = new List<Vehicle>
                               {
                                   new Vehicle { PlateNumber = "A2288AH" }
                               };

            this.mockRepository.Setup(r => r.GetAsync(
                    It.IsAny<Expression<Func<Vehicle, Vehicle>>>(), 
                    It.IsAny<Func<IQueryable<Vehicle>, IQueryable<Vehicle>>>(), 
                    It.IsAny<Func<IQueryable<Vehicle>, IOrderedQueryable<Vehicle>>>(),
                    It.IsAny<Func<IQueryable<Vehicle>, IIncludableQueryable<Vehicle, object>>>(),
                    null))
                .ReturnsAsync(vehicles);

            // Act
            var result = await this.vehicleService.IsPlateNumberExistAsync(plateNumber);

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public async Task IsPlateNumberExistAsync_ShouldReturnFalse_WhenPlateNumberDoesNotExist()
        {
            // Arrange
            var plateNumber = "A5520AH";
            var vehicles = new List<Vehicle>();

            this.mockRepository.Setup(r => r.GetAsync(
                    It.IsAny<Expression<Func<Vehicle, Vehicle>>>(), 
                    It.IsAny<Func<IQueryable<Vehicle>, IQueryable<Vehicle>>>(), 
                    It.IsAny<Func<IQueryable<Vehicle>, IOrderedQueryable<Vehicle>>>(),
                    It.IsAny<Func<IQueryable<Vehicle>, IIncludableQueryable<Vehicle, object>>>(),
                    null))
                .ReturnsAsync(vehicles);

            // Act
            var result = await this.vehicleService.IsPlateNumberExistAsync(plateNumber);

            // Assert
            Assert.That(result, Is.False);
        }
    }
}
