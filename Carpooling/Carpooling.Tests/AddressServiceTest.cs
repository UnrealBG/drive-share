﻿namespace Carpooling.Tests
{
    using BingMapsRESTToolkit;

    using Address = Carpooling.Models.Entities.Address;

    [TestFixture]
    public class AddressServiceTests
    {
        private Mock<IRepository<Address, Address>> mockRepo;
        private Mock<IMapper> mockMapper;
        private AddressService service;
        private Mock<IGeoLocationService> mockGeoLocationService;

        [SetUp]
        public void SetUp()
        {
            this.mockRepo = new Mock<IRepository<Address, Address>>();
            this.mockMapper = new Mock<IMapper>();
            this.mockGeoLocationService = new Mock<IGeoLocationService>();
            this.service = new AddressService(this.mockRepo.Object, this.mockMapper.Object, this.mockGeoLocationService.Object);
        }

        [Test]
        public async Task CreateAddressAsync_ShouldReturnCreatedAddress_WhenNewAddressIsValid()
        {
            // Arrange
            var newAddressDto = new CreateAddressDto { City = "Sofia", Country = "Bulgaria", Region = "Sofia", AddressLine = "Vasil Levski 5" };
            var address = new Address { City = "Sofia", Country = "Bulgaria", Region = "Sofia", AddressLine = "Vasil Levski 5", Latitude = 42.6977, Longitude = 23.3219 };
            var location = new Location { Point = new Point() { Coordinates = new[] { 42.6977, 23.3219 } } };

            this.mockMapper.Setup(m => m.Map<Address>(It.IsAny<CreateAddressDto>())).Returns(address);
            this.mockGeoLocationService.Setup(g => g.GeocodeAsync(It.IsAny<CreateAddressDto>())).ReturnsAsync(Result<Location>.Success(location));
            this.mockRepo.Setup(r => r.GetAsync(
                    It.IsAny<Expression<Func<Address, Address>>>(),
                    It.IsAny<Func<IQueryable<Address>, IQueryable<Address>>>(),
                    It.IsAny<Func<IQueryable<Address>, IOrderedQueryable<Address>>>(),
                    It.IsAny<Func<IQueryable<Address>, IIncludableQueryable<Address, object>>>(),
                    It.IsAny<PageParameters>()
                ))
                .ReturnsAsync(new List<Address>());
            this.mockRepo.Setup(r => r.CreateAsync(It.IsAny<Address>())).ReturnsAsync(address);

            // Act
            var response = await this.service.CreateAddressAsync(newAddressDto);

            // Assert
            Assert.That(response.IsSuccess, Is.True);
            Assert.That(response.Value, Is.EqualTo(address));
            mockRepo.Verify(r => r.CreateAsync(It.IsAny<Address>()), Times.Once);
        }

        [Test]
        public async Task CreateAddressAsync_ShouldReturnFailure_WhenNewAddressIsInvalid()
        {
            // Arrange
            var newAddressDto = new CreateAddressDto { City = "City", Country = "Country", Region = "Region", AddressLine = "AddressLine1" };
            var address = new Address { City = "City", Country = "Country", Region = "Region", AddressLine = "AddressLine1" };

            this.mockMapper.Setup(m => m.Map<Address>(It.IsAny<CreateAddressDto>())).Returns(address);
            this.mockGeoLocationService.Setup(g => g.GeocodeAsync(It.IsAny<CreateAddressDto>())).ReturnsAsync(Result<Location>.Failure("Error", HttpStatusCode.BadRequest));
            this.mockRepo.Setup(r => r.CreateAsync(It.IsAny<Address>())).Returns(Task.FromResult(address));

            // Act
            var response = await this.service.CreateAddressAsync(newAddressDto);

            // Assert
            Assert.That(response.IsSuccess, Is.False);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task CreateAddressAsync_ShouldReturnFailure_WhenGeocodeAsyncReturnsFailure()
        {
            // Arrange
            var newAddressDto = new CreateAddressDto { City = "City", Country = "Country", Region = "Region", AddressLine = "AddressLine1" };
            var address = new Address { City = "City", Country = "Country", Region = "Region", AddressLine = "AddressLine1" };

            this.mockMapper.Setup(m => m.Map<Address>(It.IsAny<CreateAddressDto>())).Returns(address);
            this.mockGeoLocationService.Setup(g => g.GeocodeAsync(It.IsAny<CreateAddressDto>())).ReturnsAsync(Result<Location>.Failure("Error", HttpStatusCode.BadRequest));
            this.mockRepo.Setup(r => r.CreateAsync(It.IsAny<Address>())).ReturnsAsync(address);

            // Act
            var response = await this.service.CreateAddressAsync(newAddressDto);

            // Assert
            Assert.That(response.IsSuccess, Is.False);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task DeleteAddressAsync_ShouldReturnTrue_WhenAddressExists()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            var address = new Address { Id = id };

            this.mockRepo.Setup(r => r.GetByIdAsync(id, a => a, null)).ReturnsAsync(address);
            this.mockRepo.Setup(r => r.SoftDeleteAsync(address)).ReturnsAsync(true);

            // Act
            var response = await this.service.DeleteAddressAsync(id);

            // Assert
            Assert.That(response.IsSuccess, Is.True);
        }

        [Test]
        public async Task DeleteAddressAsync_ShouldReturnFailure_WhenAddressDoesNotExist()
        {
            // Arrange
            Guid id = Guid.NewGuid();

            this.mockRepo.Setup(r => r.GetByIdAsync(id, a => a, null)).ReturnsAsync((Address)null);

            // Act
            var response = await this.service.DeleteAddressAsync(id);

            // Assert
            Assert.That(response.IsSuccess, Is.False);
        }

        [Test]
        public async Task DeleteAddressAsync_ShouldReturnFailure_WhenAddressIsDeleted()
        {
            // Arrange
            Guid id = Guid.NewGuid();

            // Act
            var response = await this.service.DeleteAddressAsync(id);

            // Assert
            Assert.That(response.IsSuccess, Is.False);
        }

        [Test]
        public async Task UpdateAddressAsync_ShouldReturnUpdatedAddress_WhenUpdateIsValid()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            var oldAddress = new Address { Id = id, City = "Old City" };
            var updateAddressDto = new CreateAddressDto { City = "New City" };
            var updatedAddress = new Address { Id = id, City = "New City" };
            var location = new Location { Point = new Point() };

            location.Point.Coordinates = new double[] { 0, 0 };

            updatedAddress.Longitude = location.Point.Coordinates[0];
            updatedAddress.Latitude = location.Point.Coordinates[1];

            this.mockRepo.Setup(r => r.GetByIdAsync(id, It.IsAny<Expression<Func<Address, Address>>>(), null)).ReturnsAsync(oldAddress);
            this.mockGeoLocationService.Setup(g => g.GeocodeAsync(It.IsAny<CreateAddressDto>())).ReturnsAsync(Result<Location>.Success(location));
            this.mockMapper.Setup(m => m.Map(updateAddressDto, oldAddress)).Returns(updatedAddress);
            this.mockRepo.Setup(r => r.UpdateAsync(updatedAddress)).ReturnsAsync(updatedAddress);

            // Act
            var response = await this.service.UpdateAddressAsync(oldAddress, updateAddressDto);

            // Assert
            Assert.That(response.IsSuccess, Is.True);
            Assert.That(response.Value, Is.EqualTo(updatedAddress));
        }

        [Test]
        public async Task UpdateAddressAsync_ShouldReturnFailure_WhenAddressDoesNotExist()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            var oldAddress = new Address { Id = id, City = "Old City" };
            var updateAddressDto = new CreateAddressDto { City = "New City" };

            this.mockRepo.Setup(r => r.GetByIdAsync(id, It.IsAny<Expression<Func<Address, Address>>>(), null)).ReturnsAsync((Address)null);

            // Act
            var response = await this.service.UpdateAddressAsync(oldAddress, updateAddressDto);

            // Assert
            Assert.That(response.IsSuccess, Is.False);
        }

        [Test]
        public async Task UpdateAddressAsync_ShouldReturnFailure_WhenUpdateIsInvalid()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            var oldAddress = new Address { Id = id, City = "Old City" };
            var updateAddressDto = new CreateAddressDto();

            // Act
            var response = await this.service.UpdateAddressAsync(oldAddress, updateAddressDto);

            // Assert
            Assert.That(response.IsSuccess, Is.False);
        }

        [Test]
        public async Task UpdateAddressAsync_ShouldReturnFailure_WhenGeocodeAsyncReturnsFailure()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            var oldAddress = new Address { Id = id, City = "Old City" };
            var updateAddressDto = new CreateAddressDto { City = "New City" };
            var location = new Location { Point = new Point() };

            this.mockRepo.Setup(r => r.GetByIdAsync(id, It.IsAny<Expression<Func<Address, Address>>>(), null)).ReturnsAsync(oldAddress);
            this.mockGeoLocationService.Setup(g => g.GeocodeAsync(It.IsAny<CreateAddressDto>())).ReturnsAsync(Result<Location>.Failure("Error", HttpStatusCode.BadRequest));

            // Act
            var response = await this.service.UpdateAddressAsync(oldAddress, updateAddressDto);

            // Assert
            Assert.That(response.IsSuccess, Is.False);
        }

        [Test]
        public async Task GetAddressByIdAsync_ShouldReturnFailure_WhenAddressDoesNotExist()
        {
            // Arrange
            Guid id = Guid.NewGuid();

            this.mockRepo.Setup(r => r.GetByIdAsync(id, It.IsAny<Expression<Func<Address, Address>>>(), null)).ReturnsAsync((Address)null);

            // Act
            var response = await this.service.GetAddressByIdAsync(id);

            // Assert
            Assert.That(response.IsSuccess, Is.False);
        }

        [Test]
        public async Task GetAddressByIdAsync_ShouldReturnAddress_WhenAddressExists()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            var address = new Address { Id = id };

            this.mockRepo.Setup(r => r.GetByIdAsync(id, It.IsAny<Expression<Func<Address, Address>>>(), null)).ReturnsAsync(address);

            // Act
            var response = await this.service.GetAddressByIdAsync(id);

            // Assert
            Assert.That(response.IsSuccess, Is.True);
            Assert.That(response.Value, Is.EqualTo(address));
        }
    }
}
