namespace Carpooling.Api
{
    using Carpooling.Api.Extensions;
    using Carpooling.Api.Middleware;
    using Carpooling.DataAccess.Data;

    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddApplicationServices(builder.Configuration); 

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            app.UseMiddleware<ExceptionMiddleware>();

            app.UseStatusCodePagesWithReExecute("/errors/{0}");

            //if (app.Environment.IsDevelopment())
            //{
                app.UseSwagger();
                app.UseSwaggerUI(config =>
                {
                    config.SwaggerEndpoint("/swagger/v1/swagger.json", "Carpooling.Api v1");
                    config.OAuthClientId(builder.Configuration["AzureADB2C:ClientId"]);
                    config.OAuthScopeSeparator(" ");
                    config.EnableDeepLinking();
                });
            //}

            app.UseHttpsRedirection();

            app.UseRouting();

            SeedDataBase();

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllers();

            app.Run();

            void SeedDataBase()
            {
                using var scope = app.Services.CreateScope();
                var dbInitializer = scope.ServiceProvider.GetRequiredService<IDbInitializer>();
                dbInitializer.Initialize();
            }
        }
    }
}