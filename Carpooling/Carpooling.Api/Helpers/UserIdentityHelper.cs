﻿namespace Carpooling.Api.Helpers
{
    using System.Security.Claims;

    using Carpooling.Services.Contracts;

    public static class UserIdentityHelper
    {
        public static async Task<Guid> GetUserId(ClaimsPrincipal user, IUserService userService)
        {
            var id = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            var azureId = Guid.Parse(id);

            var userId = (await userService.GetUserByAzureUserIdAsync(azureId)).Value.Id;

            return userId;
        }
    }
}
