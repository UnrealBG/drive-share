﻿namespace Carpooling.Api.Errors
{
    public class ApiResponse
    {
        public ApiResponse(int statusCode, string message = null)
        {
            this.StatusCode = statusCode;
            this.Message = message ?? this.GetDefaultMessageForStatusCode(statusCode);
        }
        public int StatusCode { get; set; }

        public string Message { get; set; }

        private string GetDefaultMessageForStatusCode(int statusCode)
        {
            return statusCode switch
                {
                    400 => "You have made a bad request.",
                    401 => "You are not authorized.",
                    403 => "You are forbidden.",
                    404 => "Resource not found.",
                    409 => "Resource already exists.",
                    503 => "Service unavailable.",
                    500 => "Server error.",
                    _ => null
                };
        }
    }
}
