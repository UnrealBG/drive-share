﻿namespace Carpooling.Api.Controllers
{
    using Carpooling.Api.Errors;

    using Microsoft.AspNetCore.Mvc;

    [Route("errors/{code}")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : BaseApiController
    {
        public IActionResult Error(int code)
        {
            return new ObjectResult(new ApiResponse(code));
        }
    }
}
