﻿namespace Carpooling.Api.Controllers
{
    using Carpooling.Api.Errors;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Mvc;

    public class ImagesController : BaseApiController
    {
        private readonly IImageService imageService;

        public ImagesController(IImageService imageService)
        {
            this.imageService = imageService;
        }

        [HttpPost]
        public async Task<IActionResult> UploadImageAsync(IFormFile file)
        {
            if (file == null)
            {
                return this.BadRequest(new ApiResponse(400));
            }

            var result = await this.imageService.UploadAsync(file);

            if (result == null)
            {
                return this.BadRequest(new ApiResponse(503));
            }

            return this.Ok(new { link = result });
        }
    }
}
