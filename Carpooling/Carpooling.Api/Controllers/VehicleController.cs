﻿namespace Carpooling.Api.Controllers
{
    using System.Security.Claims;

    using Carpooling.Api.Errors;
    using Carpooling.Api.Helpers;
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    public class VehicleController : BaseApiController
    {
        private readonly IVehicleService vehicleService;

        private readonly IUserService userService;

        public VehicleController(IVehicleService vehicleService, IUserService userService)
        {
            this.vehicleService = vehicleService;
            this.userService = userService;
        }

        [Authorize(Roles = "User")]
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetVehicleDto>> GetVehicle(Guid id)
        {
            var result = await this.vehicleService.GetVehicleAsync(id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GetVehicleDto>> CreateVehicle([FromBody] CreateVehicleDto createVehicleDto)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);
            var result = await this.vehicleService.CreateVehicleAsync(userId, createVehicleDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.Created(string.Empty, result.Value);
        }

        [Authorize(Roles = "User")]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateVehicle(Guid id, [FromBody] CreateVehicleDto createVehicleDto)
        {
            var result = await this.vehicleService.UpdateVehicleAsync(id, createVehicleDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.NoContent();
        }

        [Authorize(Roles = "User")]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteVehicle(Guid id)
        {
            var result = await this.vehicleService.DeleteVehicleAsync(id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.NoContent();
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<GetVehicleDto>>> GetVehicles()
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);
            var result = await this.vehicleService.GetVehiclesAsync(userId);

            return this.Ok(result);
        }
    }
}
