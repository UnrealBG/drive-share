﻿
namespace Carpooling.Api.Controllers
{
    using Carpooling.Api.Errors;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using System.Security.Claims;

    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : BaseApiController
    {
        private readonly IUserService userService;

        public AuthController(IUserService userService)
        {
            this.userService = userService;
        }

        // Login api/<UsersController>/login
        [HttpPost("Login")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Login()
        {
            var identity = this.User.Identity as ClaimsIdentity;

            if (identity is not null && identity.IsAuthenticated)
            {
                Guid azureUserId = Guid.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
                var userProfile = await userService.GetUserByAzureUserIdAsync(azureUserId);

                if (!userProfile.IsSuccess)
                {
                    return StatusCode(StatusCodes.Status303SeeOther, new ApiResponse(303, "The user doesn't have a profile in the system. Please create one at /api/Users"));
                }

                string userRoles = userProfile.Value.Roles.ToString();
                identity.AddClaim(new Claim(ClaimTypes.Role, userRoles));
                var cookieIdentity = new ClaimsIdentity(identity.Claims, CookieAuthenticationDefaults.AuthenticationScheme);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(cookieIdentity));
                return Ok();
            }

            return StatusCode(StatusCodes.Status401Unauthorized, new ApiResponse(401, "The user is not authenticated."));
        }

        // Logout api/<UsersController>/logout
        [HttpPost("Logout")]
        public async Task<ActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Ok();
        }
    }
}
