﻿namespace Carpooling.Api.Controllers
{
    using System.Security.Claims;

    using Carpooling.Api.Errors;
    using Carpooling.Api.Helpers;
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.DtoModels.UpdateDtos;
    using Carpooling.Models.DtoModels.Virtualize;
    using Carpooling.Models.Enums;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    public class RideController : BaseApiController
    {
        private readonly IRideService rideService;
        private readonly IUserService userService;

        public RideController(IRideService rideService, IUserService userService)
        {
            this.rideService = rideService;
            this.userService = userService;
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<GetRideDto>> CreateRideAsync(CreateRideDto createRideDto)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User,this.userService);
          
            var result = await this.rideService.CreateRideAsync(userId, createRideDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.StatusCode(201, result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<VirtualizeResponse<GetRideDto>>> GetRidesAsync(FindRideQuery query)
        {
            var items = await this.rideService.GetRidesAsync(query);

            if (!items.IsSuccess)
            {
                return this.StatusCode((int)items.StatusCode, new ApiResponse((int)items.StatusCode, items.Error));
            }

            var count = await this.rideService.GetItemsCountAfterFiltesAsync(query);

            if (!items.IsSuccess)
            {
                return this.StatusCode((int)items.StatusCode, new ApiResponse((int)items.StatusCode, items.Error));
            }

            VirtualizeResponse<GetRideDto> result = new VirtualizeResponse<GetRideDto>()
            {
                Items = items.Value.ToList(),
                ItemsCount = count.Value
            };

            return Ok(result);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<GetRideDto>> GetRideDetailsAsync(Guid id)
        {
            var result = await this.rideService.GetRideDetailsAsync(id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPut("{id}/cancele")]
        public async Task<ActionResult<bool>> CancelRideAsync(Guid id, [FromBody] UpdateRideStatus updateRideStatus)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.rideService.UpdateRideStatusAsync(userId, updateRideStatus);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPut("{id}/new-pickup")]
        public async Task<ActionResult<GetRideDto>> UpdateRideStartLocationAsync([FromQuery] Guid id, [FromBody] CreateAddressDto addressDto)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.rideService.UpdateRideStartLocationAsync(userId, id, addressDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPut("{id}/new-dropoff")]
        public async Task<ActionResult<GetRideDto>> UpdateRideEndLocationAsync([FromQuery] Guid id, [FromBody] CreateAddressDto addressDto)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.rideService.UpdateRideEndLocationAsync(userId, id, addressDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPut("{id}/new-vehicle")]
        public async Task<ActionResult<GetRideDto>> UpdateRideVehicleAsync([FromQuery] Guid id, [FromBody] Guid vehicleId)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.rideService.UpdateRideVehicleAsync(userId, id, vehicleId);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<ActionResult<GetRideDto>> UpdateRideAsync([FromQuery] Guid id, [FromBody] UpdateRideDto updateRideDto)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.rideService.UpdateRideAsync(userId, id, updateRideDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<GetRideDto>> DeleteRideAsync(Guid id)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.rideService.DeleteRideAsync(userId, id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }
    }
}
