﻿
namespace Carpooling.Api.Controllers
{
    using System.Security.Claims;

    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.UpdateDtos;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class UsersController : BaseApiController
    {        
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        // GET: api/<UsersController>
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetAllUsersAsync([FromQuery] FindUserQuery query)
        {
            var result = await this.userService.GetUserAsync(query);
            if (result.IsSuccess)
            {
                return Ok(result.Value);
            }
            return StatusCode((int)result.StatusCode, result.Error);
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetByIdAsync(Guid id)
        {
            var result = await this.userService.GetUserByIdAsync(id);
            if (result.IsSuccess)
            {
                return StatusCode(StatusCodes.Status200OK, result.Value);
            }
            return StatusCode((int)result.StatusCode, result.Error);
        }

        // POST api/<UsersController>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Create([FromBody] CreateUserDto createUserDto)
        {
            var loggedUserIdentity = this.User.Identity as ClaimsIdentity;

            var result = await this.userService.CreateUserAsync(createUserDto, loggedUserIdentity);
            if (result.IsSuccess)
            {
                return Ok(result.Value);
            }
            return StatusCode((int)result.StatusCode, result.Error);
        }

        // PUT api/<UsersController>/
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] UpdateUserDto updateUserDto)
        {
            var loggedUserIdentity = this.User.Identity as ClaimsIdentity;

            var result = await this.userService.UpdateUserAsync(updateUserDto, loggedUserIdentity);
            if (result.IsSuccess)
            {
                return Ok(result.Value);
            }
            return StatusCode((int)result.StatusCode, result.Error);
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
