﻿namespace Carpooling.Api.Controllers
{
    using Carpooling.Api.Errors;
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class AddressController : BaseApiController
    {
        private readonly IAddressService addressService;

        public AddressController(IAddressService addressService)
        {
            this.addressService = addressService;
        }

        [Authorize(Roles = "User")]
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetAddressDto>> GetAddress(Guid id)
        {
            var result = await this.addressService.GetAddressByIdAsync(id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CreateAddressDto>> CreateAddress(CreateAddressDto createAddressDto)
        {
            var result = await this.addressService.CreateAddressAsync(createAddressDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.CreatedAtAction(nameof(this.GetAddress), new { id = result.Value.Id }, result.Value);
        }

        [Authorize(Roles = "User")]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateAddress(Guid id, CreateAddressDto updateAddressDto)
        {
            var address = await this.addressService.GetAddressByIdAsync(id);

            if (!address.IsSuccess)
            {
                return this.StatusCode((int)address.StatusCode, new ApiResponse((int)address.StatusCode, address.Error));
            }

            var result = await this.addressService.UpdateAddressAsync(address.Value, updateAddressDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.NoContent();
        }

        [Authorize(Roles = "User")]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteAddress(Guid id)
        {
            var result = await this.addressService.DeleteAddressAsync(id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.NoContent();
        }
    }
}
