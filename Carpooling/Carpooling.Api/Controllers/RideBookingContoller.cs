﻿namespace Carpooling.Api.Controllers
{
    using System.Security.Claims;

    using Carpooling.Api.Errors;
    using Carpooling.Api.Helpers;
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.DtoModels.UpdateDtos;
    using Carpooling.Models.DtoModels.Virtualize;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    public class RideBookingController : BaseApiController
    {
        private readonly IBookService bookService;
        private readonly IUserService userService;

        public RideBookingController(IBookService bookService, IUserService userService)
        {
            this.bookService = bookService;
            this.userService = userService;
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<GetRideBookingDto>> CreateBookingAsync(CreateBookingDto newBook)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.bookService.CreateBookingAsync(userId, newBook);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.StatusCode(201, result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<VirtualizeResponse<GetRideBookingDto>>> GetBookingsAsync([FromQuery] FindBookingQuery query)
        {
            var result = await this.bookService.GetBookingsAsync(query);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            var itemsAfterFilter = await this.bookService.GetItemsCountAfterFiltesAsync(query);

            if (!itemsAfterFilter.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            VirtualizeResponse<GetRideBookingDto> response = new VirtualizeResponse<GetRideBookingDto>()
            {
                Items = result.Value.ToList(),
                ItemsCount = itemsAfterFilter.Value
            };

            return Ok(response);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<GetRideBookingDto>> GetBookingDetailsAsync(Guid id)
        {
            var result = await this.bookService.GetBookingDetailsAsync(id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpGet("freeplaces")]
        public async Task<ActionResult<int>> GetFreePlacesAsync([FromQuery] Guid rideId)
        {
            var result = await this.bookService.GetFreePlacesAsync(rideId);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPut("{id}/cancel")]
        public async Task<ActionResult<bool>> BookingCancelationAsync(Guid id)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.bookService.BookingCancelationAsync(userId, id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPut("{id}/reject")]
        public async Task<ActionResult<bool>> RejectBookingAsync(Guid id)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.bookService.RejectBookingAsync(userId, id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpPut("{id}/change")]
        public async Task<ActionResult<bool>> ChangeBookedPassengersAsync(UpdateBookingDto updateBookingDto)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.bookService.ChangeBookedPassengersAsync(userId, updateBookingDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }

        [Authorize(Roles = "Administrator")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> DeleteBookingAsync(Guid id)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);

            var result = await this.bookService.DeleteBookingAsync(userId, id);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return Ok(result.Value);
        }
    }
}
