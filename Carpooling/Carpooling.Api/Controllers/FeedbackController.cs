﻿namespace Carpooling.Api.Controllers
{
    using System.Security.Claims;

    using Carpooling.Api.Errors;
    using Carpooling.Api.Helpers;
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.DtoModels.GetDtos.AbstractClasses;
    using Carpooling.Models.DtoModels.Virtualize;
    using Carpooling.Models.Enums;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    public class FeedbackController : BaseApiController
    {
        private readonly IFeedbackService feedbackService;
        private readonly IUserService userService;

        public FeedbackController(IFeedbackService feedbackService, IUserService userService)
        {
            this.feedbackService = feedbackService;
            this.userService = userService;
        }

        [Authorize(Roles = "User")]
        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetFeedbackDto>> CreateFeedbackAsync(CreatFeedbackDto creatFeedbackDto)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);
            var result = await this.feedbackService.CreateFeedbackAsync(userId, creatFeedbackDto);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [HttpGet("rating")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetFeedbackDto>> GetRatingAsync(FeedbackType rideRole)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);
            var result = await this.feedbackService.GetRatingAsync(userId, rideRole);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [HttpGet("given-feedbacks")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<VirtualizeResponse<GetGivenFeedbackDto>>> GetGivenFeedbacksAsync(FindFeedbackQuery query)
        {
            var result = await this.feedbackService.GetGivenFeedbacksAsync(query);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            var itemsAfterFilter = await this.feedbackService.GetItemsCountAfterFiltesAsync(query);

            if (!itemsAfterFilter.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            VirtualizeResponse<GetGivenFeedbackDto> feedbacks = new VirtualizeResponse<GetGivenFeedbackDto>()
            {
                Items = result.Value.ToList(),
                ItemsCount = itemsAfterFilter.Value
            };

            return this.Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [HttpGet("recieved-feedbacks")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<VirtualizeResponse<GetFeedbackDto>>> GetRecivedFeedbacksAsync(FindFeedbackQuery query)
        {
            var result = await this.feedbackService.GetRecivedFeedbacksAsync(query);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            var itemsAfterFilter = await this.feedbackService.GetItemsCountAfterFiltesAsync(query);

            if (!itemsAfterFilter.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            VirtualizeResponse<GetRecivedFeedbackDto> feedbacks = new VirtualizeResponse<GetRecivedFeedbackDto>()
            {
                Items = result.Value.ToList(),
                ItemsCount = itemsAfterFilter.Value
            };

            return this.Ok(result.Value);
        }

        [Authorize(Roles = "User")]
        [HttpDelete("{feedbackId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteFeedbackAsync(Guid feebackId)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);
            var result = await this.feedbackService.DeleteFeedbackAsync(userId, feebackId);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.Ok();
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("passenger-rating/{passengerId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeletePassengerRatingAsync(Guid passengerId)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);
            var result = await this.feedbackService.DeletePassengerRatingAsync(passengerId);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.Ok();
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("driver-rating/{driverId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteDriverRatingAsync(Guid driverId)
        {
            var userId = await UserIdentityHelper.GetUserId(this.User, this.userService);
            var result = await this.feedbackService.DeleteDriverRatingAsync(driverId);

            if (!result.IsSuccess)
            {
                return this.StatusCode((int)result.StatusCode, new ApiResponse((int)result.StatusCode, result.Error));
            }

            return this.Ok();
        }
    }
}

