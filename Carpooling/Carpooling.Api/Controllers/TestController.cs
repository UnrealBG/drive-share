﻿namespace Carpooling.Api.Controllers
{
    using AutoMapper;

    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.Entities;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IGeoLocationService locationService;
        private readonly IEmailService emailService;
        private readonly IMapper mapper;

        public TestController(IGeoLocationService locationService, IEmailService emailService, IMapper mapper)
        {
            this.locationService = locationService;
            this.emailService = emailService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<string>> GetDistance()
        {
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = Path.Combine(baseDir, "EmailResponseHtml", "Images", "avatar.jpg");

            Ride ride = new Ride()
            {
                Id = Guid.NewGuid(),
                StartAddress = new Address()
                {
                    City = "Byala Slatina",
                    Region = "Vratsa",
                    Country = "Bulgaria"
                },
                EndAddress = new Address()
                {
                    City = "Burgas",
                    Region = "Burgas",
                    Country = "Bulgaria"
                },
                Driver = new User()
                {
                    Id = Guid.NewGuid(),
                    Username = "nedko_nedkov",
                    FirstName = "Nedko",
                    LastName = "Nedkov",
                    Email = "ngnedkov90@gmail.com",
                    PhoneNumber = "+359886063525",
                    ProfileImageUrl = filePath
                }
            };

            RideBooking book = new RideBooking()
            {
                UserId = Guid.NewGuid(),
                CreatedOn = DateTime.UtcNow,
                User = new User()
                {
                    Id = Guid.NewGuid(),
                    Username = "nedko_nedkov",
                    FirstName = "Nedko",
                    LastName = "Nedkov",
                    Email = "ngnedkov90@gmail.com",
                    PhoneNumber = "+359886063525",
                    ProfileImageUrl = filePath
                },
                RideId = ride.Id,
                Ride = ride
            };

            var message = this.emailService.CreateBookingMessage("Ride Booking", "ngnedkov80@gmail.com", this.mapper.Map<GetUserDto>(ride.Driver), book);

            this.emailService.SendEmailAsync(message);
            return Ok("Success");
        }

        // calculate distance and duration between two addresses , just for testing purposes, will be removed!!!
        [HttpGet("calculateDistTest")]
        public async Task<ActionResult<(double Distance, double Duration)>> GetDistanceAndDuration()
        {            
            return this.Ok();
        }
    }
}
