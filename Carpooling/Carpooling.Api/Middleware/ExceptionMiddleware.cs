﻿namespace Carpooling.Api.Middleware
{
    using System.Net;

    using Carpooling.Api.Errors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class ExceptionMiddleware
    {
        private readonly RequestDelegate next;

        private readonly ILogger<ExceptionMiddleware> logger;

        private readonly IHostEnvironment emEnvironment;

        public ExceptionMiddleware(
            RequestDelegate next,
            ILogger<ExceptionMiddleware> logger,
            IHostEnvironment emEnvironment)
        {
            this.next = next;
            this.logger = logger;
            this.emEnvironment = emEnvironment;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await this.next(context);
            }
            catch (Exception ex)
            {
                this.LogException(this.logger, ex, context, HttpStatusCode.InternalServerError);
            }
        }

        private async void LogException(ILogger loger, Exception ex, HttpContext context, HttpStatusCode statusCode)
        {
            this.logger.LogError(ex, ex.Message);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;

            var response = this.emEnvironment.IsDevelopment()
                               ? new ApiException((int)statusCode, ex.Message, ex.StackTrace.ToString())
                               : new ApiException((int)statusCode);

            var settings = new JsonSerializerSettings
                               {
                                   ContractResolver = new CamelCasePropertyNamesContractResolver()
                               };

            var json = JsonConvert.SerializeObject(response, settings);

            await context.Response.WriteAsync(json);
        }
    }
}
