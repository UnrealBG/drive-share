﻿namespace Carpooling.Api.Extensions
{
    using System.Text.Json.Serialization;

    using Carpooling.Api.Errors;
    using Carpooling.DataAccess.Data;
    using Carpooling.DataAccess.Repository;
    using Carpooling.DataAccess.Repository.Contracts;
    using Carpooling.Services;
    using Carpooling.Services.Contracts;
    using Carpooling.Services.ExternalServices;

    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Identity.Web;
    using Microsoft.OpenApi.Models;

    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddCookie(options =>
                {
                    options.Cookie.Name = "CarpoolingCookie";
                })
                .AddMicrosoftIdentityWebApi(config.GetSection("AzureADB2C"));

            services.AddAuthorization(options =>
            {
                // By default, all incoming requests will be authorized according to the default policy
                options.FallbackPolicy = options.DefaultPolicy;
            });

            services.AddControllers()
                .AddJsonOptions(options =>
                    {
                        options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
                    });
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Carpooling API", Version = "v1" });

                var scope = new List<string>() { config["AzureADB2C:Scope"] };
                var securitySchemeType = SecuritySchemeType.OAuth2;
                var securityScheme = new OpenApiSecurityScheme()
                {
                    Type = securitySchemeType,
                    Name = "Authorization",
                    In = ParameterLocation.Query,
                    Scheme = securitySchemeType.ToString(),
                    Flows = new OpenApiOAuthFlows
                    {
                        Implicit = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = new Uri(config["AzureADB2C:AuthorizationEndpoint"]),
                            Scopes = new Dictionary<string, string>
                            {
                                { $"https://{config["AzureADB2C:Domain"]}/{config["AzureADB2C:ClientId"]}/access_as_user","Acces as User"}
                            },
                        }
                    },
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = securitySchemeType.ToString()
                    },
                };

                var securityRequirements = new OpenApiSecurityRequirement
                {
                    { securityScheme, scope }
                };

                options.AddSecurityDefinition(securitySchemeType.ToString(), securityScheme);
                options.AddSecurityRequirement(securityRequirements);
            });
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            services.AddEndpointsApiExplorer();

            var connectionString = config.GetConnectionString("DefaultConnection");  
            services.AddDbContextFactory<CarpoolingDbContext>(
                options =>
                    options.UseSqlServer(connectionString));

            services.AddScoped<IDbInitializer, DbInitializer>();
            var emailConfig = config
                .GetSection("EmailConfiguration")
                .Get<EmailConfiguration>();
            services.AddSingleton(emailConfig);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddScoped<IGeoLocationService, GeoLocationService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IGeoLocationService, GeoLocationService>();
            services.AddScoped<IEmailClient, EmailClient>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IVehicleService, VehicleService>();
            services.AddScoped<IImageService, ImageService>();
            services.AddScoped<IUserService, UserService>();

            services.Configure<ApiBehaviorOptions>(
                options =>
                {
                    options.InvalidModelStateResponseFactory = actionContext =>
                    {
                        var errors = actionContext.ModelState.Where(e => e.Value.Errors.Count > 0)
                            .SelectMany(x => x.Value.Errors).Select(x => x.ErrorMessage).ToArray();

                        var errorResponse = new ApiValidationErrorResponse { Errors = errors };

                        return new BadRequestObjectResult(errorResponse);
                    };
                });

            return services;
        }
    }
}