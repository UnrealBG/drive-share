﻿namespace Carpooling.Services.Helpers
{
    using System.Net;

    public class Result<T>
    {
        private Result(bool isSuccess, T value, string error, HttpStatusCode statusCode)
        {
            this.IsSuccess = isSuccess;
            this.Value = value;
            this.Error = error;
            this.StatusCode = statusCode;
        }

        public T Value { get; }

        public bool IsSuccess { get; }

        public string Error { get; }

        public HttpStatusCode StatusCode { get; }

        public static Result<T> Success(T value) => new Result<T>(true, value, null, statusCode: HttpStatusCode.OK);

        public static Result<T> Failure(string error, HttpStatusCode errorCode) => new Result<T>(false, default(T), error, errorCode);
    }
}
