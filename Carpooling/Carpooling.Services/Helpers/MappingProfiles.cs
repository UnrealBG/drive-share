﻿namespace Carpooling.Services.Helpers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            this.CreateMap<User, GetUserDto>()
                .ForMember(ud => ud.Name, m => m.MapFrom(u => u.FirstName + " " + u.LastName))
                .ForMember(ud => ud.ImgUrl, m => m.MapFrom(u => u.ProfileImageUrl))
                .ForMember(
                    ud => ud.PassengerRating,
                    m => m.MapFrom(
                        u => (u.ReceivedFeedbacks.Count != 0 && u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Passenger).Count() != 0)
                                 ? u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Passenger)
                                     .Average(f => f.Rating)
                                 : 0)).ForMember(
                    ud => ud.DriverRating,
                    m => m.MapFrom(
                        u => (u.ReceivedFeedbacks.Count != 0 && u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Driver).Count() != 0)
                                 ? u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Driver).Average(f => f.Rating)
                                 : 0)).ForMember(
                    r => r.PassengerCommentsCount,
                    m => m.MapFrom(
                        u => u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Passenger)
                            .Count(f => f.Comment != null))).ForMember(
                    r => r.DriverCommentCount,
                    m => m.MapFrom(
                        u => u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Driver)
                            .Count(f => f.Comment != null)));

            this.CreateMap<User, UpdateUserDto>();
            
            this.CreateMap<Ride, GetRideDto>()
                .ForMember(gr => gr.FreeSpots, m => m.MapFrom(r => r.FreeSpots - (r.RideBookings.Where(b => b.Status == BookingStatus.Аpproved).Sum(b => b.BookedPlaces))));
            
            this.CreateMap<Feedback, GetRecivedFeedbackDto>().ForMember(
                fd => fd.AuthorName,
                m => m.MapFrom(f => f.Sender.FirstName + " " + f.Sender.LastName));

            this.CreateMap<Feedback, GetGivenFeedbackDto>().ForMember(
                fd => fd.RecipientName,
                m => m.MapFrom(f => f.Receiver.FirstName + " " + f.Receiver.LastName));

            this.CreateMap<RideBooking, GetRideBookingDto>();

            this.CreateMap<Vehicle, CreateVehicleDto>();

            this.CreateMap<CreateVehicleDto, Vehicle>();

            this.CreateMap<GetVehicleDto, Vehicle>();

            this.CreateMap<Vehicle, GetVehicleDto>();

            this.CreateMap<Address, CreateAddressDto>();

            this.CreateMap<CreateAddressDto, Address>();

            this.CreateMap<CreateUserDto, User>();

            this.CreateMap<User, CreateUserDto>();

            this.CreateMap<UpdateUserDto, User>().ForMember(u => u.Username, m => m.MapFrom(u => u.Username))
                .ForMember(u => u.FirstName, m => m.MapFrom(u => u.FirstName))
                .ForMember(u => u.LastName, m => m.MapFrom(u => u.LastName))
                .ForMember(u => u.Email, m => m.MapFrom(u => u.Email))
                .ForMember(u => u.PhoneNumber, m => m.MapFrom(u => u.PhoneNumber))
                .ForMember(u => u.ProfileImageUrl, m => m.MapFrom(u => u.ProfileImageUrl)).ForAllMembers(
                    options => options.Condition((src, dest, srcMember) => srcMember is not null));

            this.CreateMap<Feedback, CreatFeedbackDto>();

            this.CreateMap<CreateRideDto, Ride>();

            this.CreateMap<Ride, CreateRideDto>();
        }
    }
}