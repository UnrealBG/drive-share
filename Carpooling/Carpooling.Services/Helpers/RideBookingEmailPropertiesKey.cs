﻿namespace Carpooling.Services.Helpers
{
    static public class RideBookingEmailPropertiesKey
    {
        public const string BOOKING_OPERATION = "[BookingOperation]";
        public const string BOOKING_ID = "[RideBookingId]";
        public const string NAME = "[Name]";
        public const string USERNAME = "[Username]";
        public const string EMAIL = "[Email]";
        public const string PHONE_NUMBER = "[PhoneNumber]";
        public const string RATING = "[Rating]";
        public const string STATUS = "[Status]";
        public const string RIDE_ID = "[RideId]";
        public const string START_ADDRESS = "[StartAddress]";
        public const string END_ADDRESS = "[EndAddress]";
        public const string INFO_LINK = "[InfoLink]";
        public const string TEMPLATE_IMG = "[TamplateImg]";
        public const string AVATAR = "[Avatar]";
    }
}
