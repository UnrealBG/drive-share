﻿namespace Carpooling.Services.Helpers
{
    static public class RideEmailPropertiesKey
    {
        public const string OPERATION = "[Operation]";
        public const string TEMPLATE_IMG = "[TamplateImg]";
        public const string AVATAR = "[Avatar]";
        public const string RIDE_ID = "[RideId]";
        public const string OLD_VALUE = "[OldValue]";
        public const string NEW_VALUE = "[NewValue]";
        public const string DRIVER_NAME = "[Name]";
        public const string USER_NAME = "[Username]";
        public const string EMAIL = "[Email]";
        public const string PHONENUMBER = "[PhoneNumber]";
        public const string DRIVER_RATING = "[Rating]";
        public const string INFO_LINK = "[InfoLink]";
    }
}
