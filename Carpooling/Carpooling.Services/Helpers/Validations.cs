﻿namespace Carpooling.Services.Helpers
{
    public static class Validations
    {
        static public bool IsDepartureTimeApproaching(DateTime departureTime)
        {
            DateTime dateTime = DateTime.UtcNow;
            return departureTime < dateTime.AddHours(TIME_APPROACHING_IN_HOURS);
        }
    }
}
