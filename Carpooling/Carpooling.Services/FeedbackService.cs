﻿namespace Carpooling.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IRepository<Feedback, Feedback> feedbackRepository;
        private readonly IRepository<Feedback, GetGivenFeedbackDto> givenDtoRepository;
        private readonly IRepository<Feedback, GetRecivedFeedbackDto> recivedDtoRepositoryFeedback;
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public FeedbackService(IRepository<Feedback, Feedback> feedbackRepository,
                               IRepository<Feedback, GetRecivedFeedbackDto> recivedDtoRepositoryFeedback,
                               IRepository<Feedback, GetGivenFeedbackDto> givenDtoRepository,
                               IUserService userService, IMapper mapper)
        {
            this.feedbackRepository = feedbackRepository;
            this.givenDtoRepository = givenDtoRepository;
            this.recivedDtoRepositoryFeedback = recivedDtoRepositoryFeedback;
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<Result<bool>> CreateFeedbackAsync(Guid userId, CreatFeedbackDto newFeedback)
        {
            var userResult = await this.userService.GetUserDetailsAsync(newFeedback.ReciverId);

            if (!userResult.IsSuccess)
            {
                return Result<bool>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }

            User recipient = userResult.Value;

            if (newFeedback.Rating > 5 || newFeedback.Rating < 0)
            {
                return Result<bool>.Failure(ResultMessages.INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            Feedback feedback = new Feedback()
            {
                ReceiverId = newFeedback.ReciverId,
                Comment = newFeedback.Comment,
                SenderId = userId,
                Type = newFeedback.Type,
                CreatedDate = DateTime.Now
            };

            _ = await this.feedbackRepository.CreateAsync(feedback);
            return Result<bool>.Success(true);
        }

        public async Task<Result<bool>> DeleteDriverRatingAsync(Guid driverId)
        {
            var deleteResult = await this.DeleteRatingAsync(driverId, FeedbackType.Driver);

            if (!deleteResult.IsSuccess) return deleteResult;

            return Result<bool>.Success(deleteResult.Value);
        }

        public async Task<Result<int>> GetItemsCountAfterFiltesAsync(FindFeedbackQuery queryParams)
        {
            var count = await this.feedbackRepository.GetItemsCountAfterFiltersAsync(Filter(queryParams));
            return Result<int>.Success(count);
        }

        public async Task<Result<bool>> DeletePassengerRatingAsync(Guid passengerId)
        {
            var result = await this.DeleteRatingAsync(passengerId, FeedbackType.Passenger);

            if (result.IsSuccess) return Result<bool>.Success(result.Value);

            return Result<bool>.Failure(INVALID_PARAMS, HttpStatusCode.NotFound);
        }

        public async Task<Result<IEnumerable<GetRecivedFeedbackDto>>> GetRecivedFeedbacksAsync(FindFeedbackQuery query)
        {
            if (query.RecipientId == null)
            {
                return Result<IEnumerable<GetRecivedFeedbackDto>>.Failure(ResultMessages.INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            var result = await this.recivedDtoRepositoryFeedback.GetAsync(f => this.mapper.Map<GetRecivedFeedbackDto>(f), this.Filter(query), this.Order(), this.Include());

            if (result == null)
            {
                return Result<IEnumerable<GetRecivedFeedbackDto>>.Failure(FEEDBACK_NOT_FOUND, HttpStatusCode.NotFound);
            }

            return Result<IEnumerable<GetRecivedFeedbackDto>>.Success(result);
        }

        public async Task<Result<double>> GetRatingAsync(Guid userId, FeedbackType rideRole)
        {
            var feedbacks = await this.feedbackRepository.GetAsync(p => p, p => p.Where(p => p.ReceiverId == userId && p.Type == rideRole));

            if (feedbacks == null || feedbacks.Count == 0)
            {
                return Result<double>.Failure(FEEDBACK_NOT_FOUND, HttpStatusCode.NotFound);
            }

            return Result<double>.Success(feedbacks.Average(p => p.Rating));
        }

        public async Task<Result<IEnumerable<GetGivenFeedbackDto>>> GetGivenFeedbacksAsync(FindFeedbackQuery query)
        {
            if (query.SenderId == null)
            {
                return Result<IEnumerable<GetGivenFeedbackDto>>.Failure(ResultMessages.INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            var result = await this.givenDtoRepository.GetAsync(f => this.mapper.Map<GetGivenFeedbackDto>(f), this.Filter(query), this.Order(), this.Include(), new PageParameters() { StartIndex = query.StartIndex, Take = query.Take });

            if (result == null || result.Count == 0)
            {
                return Result<IEnumerable<GetGivenFeedbackDto>>.Failure(FEEDBACK_NOT_FOUND, HttpStatusCode.NotFound);
            }

            return Result<IEnumerable<GetGivenFeedbackDto>>.Success(result);
        }

        public async Task<Result<bool>> DeleteFeedbackAsync(Guid userId, Guid feedbackId)
        {
            var userRoles = await this.userService.GetUserRolesAsync(userId);

            if (!userRoles.IsSuccess)
            {
                return Result<bool>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }

            if (!userRoles.Value.Contains(UserRoles.Administrator))
            {
                return Result<bool>.Failure(UNATHORIZED_OPERATION, HttpStatusCode.Unauthorized);
            }

            var feedback = await this.feedbackRepository.GetByIdAsync(feedbackId, f => f);

            if (feedback == null)
            {
                return Result<bool>.Failure(ResultMessages.INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            return Result<bool>.Success(await this.feedbackRepository.SoftDeleteAsync(feedback));
        }

        private async Task<Result<bool>> DeleteRatingAsync(Guid userId, FeedbackType rideRole)
        {
            var feedbacks = await this.feedbackRepository.GetAsync(p => p, p => p.Where(p => p.ReceiverId == userId && p.Type == rideRole));

            if (feedbacks == null || feedbacks.Count == 0)
            {
                return Result<bool>.Failure(FEEDBACK_NOT_FOUND, HttpStatusCode.NotFound);
            }

            foreach (var feedback in feedbacks)
            {
                feedback.Rating = 0;
                _ = await this.feedbackRepository.UpdateAsync(feedback);
            }
            return Result<bool>.Success(true);
        }

        private Func<IQueryable<Feedback>, IQueryable<Feedback>> Filter(FindFeedbackQuery query)
        {
            return f => f.Where(f => !f.IsDeleted)
                         .Where(f => (query.RecipientId != null) ? f.ReceiverId == query.RecipientId : true)
                         .Where(f => (query.SenderId != null) ? f.SenderId == query.SenderId : true)
                         .Where(f => (query.Type != null) ? f.Type == query.Type : true);
        }

        private Func<IQueryable<Feedback>, IOrderedQueryable<Feedback>> Order()
        {
            return o => o.OrderByDescending(o => o.CreatedDate);
        }

        private Func<IQueryable<Feedback>, IIncludableQueryable<Feedback, object>> Include()
        {
            return f => f.Include(f => f.Sender).Where(f => !f.IsDeleted)
                         .Include(f => f.Receiver);
        }
    }
}
