﻿namespace Carpooling.Services
{
    public class AddressService : IAddressService
    {
        private readonly IRepository<Address, Address> repository;
        private readonly IMapper mapper;
        private readonly IGeoLocationService locationService;

        public AddressService(IRepository<Address, Address> repository, IMapper mapper, IGeoLocationService locationService)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.locationService = locationService;
        }

        public async Task<Result<Address>> CreateAddressAsync(CreateAddressDto newAddress)
        {
            var geocodeResult = await this.locationService.GeocodeAsync(newAddress);

            if (!geocodeResult.IsSuccess)
            {
                return Result<Address>.Failure(geocodeResult.Error, HttpStatusCode.BadRequest);
            }

            var address = this.mapper.Map<Address>(newAddress);

            address.Latitude = geocodeResult.Value.Point.Coordinates[0];
            address.Longitude = geocodeResult.Value.Point.Coordinates[1];

            Func<IQueryable<Address>, IQueryable<Address>> filter = p => p.Where(p => p.Latitude == address.Latitude && p.Longitude == address.Longitude);

            var addresses = await this.repository.GetAsync(p => p, filter);

            if (addresses.Any())
            {
                foreach (var addr in addresses)
                {
                    if (addr.Latitude == address.Latitude && addr.Longitude == address.Longitude)
                    {
                        return Result<Address>.Success(addr);
                    }
                }
            }

            address = await this.repository.CreateAsync(address);

            return Result<Address>.Success(address);
        }

        public async Task<Result<bool>> DeleteAddressAsync(Guid id)
        {
            var address = await this.repository.GetByIdAsync(id, a => a);

            if (address == null)
            {
                return Result<bool>.Failure(ADDRESS_NOT_FOUND, HttpStatusCode.BadRequest);
            }

            await this.repository.SoftDeleteAsync(address);
            return Result<bool>.Success(true);
        }

        public async Task<Result<Address>> UpdateAddressAsync(Address address, CreateAddressDto newAddress)
        {
            var isAddressExist = await this.repository.GetByIdAsync(address.Id, a => a);

            if (isAddressExist != null)
            {
                var geocodeResult = await this.locationService.GeocodeAsync(newAddress);
                if (!geocodeResult.IsSuccess)
                {
                    return Result<Address>.Failure(geocodeResult.Error, HttpStatusCode.BadRequest);
                }

                address = this.mapper.Map(newAddress, address);

                address.Latitude = geocodeResult.Value.Point.Coordinates[0];
                address.Longitude = geocodeResult.Value.Point.Coordinates[1];

                await this.repository.UpdateAsync(address);

                return Result<Address>.Success(address);
            }

            return Result<Address>.Failure(ADDRESS_NOT_FOUND, HttpStatusCode.BadRequest);
        }

        public async Task<Result<Address>> GetAddressByIdAsync(Guid addressId)
        {
            var address = await this.repository.GetByIdAsync(addressId, a => a);

            if (address == null)
            {
                return Result<Address>.Failure(ADDRESS_NOT_FOUND, HttpStatusCode.BadRequest);
            }

            return Result<Address>.Success(address);
        }
    }
}
