﻿namespace Carpooling.Services
{
    using Carpooling.Models.Entities;
    using Carpooling.Models.QueryModels;

    using MimeKit;

    public class RideService : IRideService
    {
        private const string CHANGED_PICKUP_ADDRESS = "Changed ride's pickup address";
        private const string CHANGED_DROPOFF_ADDRESS = "Changed ride's drop-off address";
        private const string CHANGE_VEHICLE = "The vehicle has changed";
        private const string RIDE_CANCELATION = "Ride canceled";
        private const string RIDE_COMPLATED = "Ride complated";
        private const string CHANGE_RIDE_PROPERTIES = "Modification of booking elements";
        private const string OLD_ADDRESS_FORMAT = "Old address: {0}, {1}, {2}";
        private const string NEW_ADDRESS_FORMAT = "New address: {0}, {1}, {2}";
        private const string NEW_VEHICLE_FORMAT = "New license plate number : {0}";
        private const string OLD_VEHICLE_FORMAT = "OLD license plate number : {0}";
        private const string NEW_STATUS_FORMAT = "New status : {0}";
        private const string OLD_STATUS_FORMAT = "OLD status : {0}";
        private const string OLD = "<div>Old values</div>";
        private const string NEW = "<div>New values</div>";
        private const string PROPERTIES_FORMAT = "<div>FreeSpots : {0}<div><div>Departure time : {1}</div><div>Comment : {2}</div><div>Pets : {3}</div><div>Smoking : {4}</div><div>Price : {5} {6}</div>";

        private readonly IRepository<Ride, GetRideDto> rideDtoRepository;
        private readonly IRepository<Ride, Ride> rideRepository;
        private readonly IFeedbackService feedbackService;
        private readonly IGeoLocationService geoMapService;
        private readonly IVehicleService vehicleService;
        private readonly IAddressService addressService;
        private readonly IUserService userService;
        private readonly IBookService bookService;
        private readonly IEmailService emailService;
        private readonly IMapper mapper;

        public RideService(IRepository<Ride, GetRideDto> rideDtoRepository,
                           IRepository<Ride, Ride> rideRepository,
                           IFeedbackService feedbackService,
                           IGeoLocationService geoMapService,
                           IVehicleService vehicleService,
                           IAddressService addressService,
                           IUserService userService,
                           IBookService bookService,
                           IEmailService emailService,
                           IMapper mapper)
        {
            this.rideDtoRepository = rideDtoRepository;
            this.rideRepository = rideRepository;
            this.feedbackService = feedbackService;
            this.geoMapService = geoMapService;
            this.vehicleService = vehicleService;
            this.addressService = addressService;
            this.userService = userService;
            this.bookService = bookService;
            this.emailService = emailService;
            this.mapper = mapper;
        }

        public async Task<Result<IEnumerable<GetRideDto>>> GetRidesAsync(FindRideQuery queryParams)
        {
            var resultRides = await this.rideDtoRepository.GetAsync(tr => this.mapper.Map<GetRideDto>(tr), this.Filter(queryParams), Order(queryParams), Include(), new PageParameters() { StartIndex = queryParams.StartIndex, Take = queryParams.Take });

            if (resultRides == null || resultRides.Count == 0)
            {
                return Result<IEnumerable<GetRideDto>>.Failure(RIDE_NOT_FOUND, HttpStatusCode.NotFound);
            }

            return Result<IEnumerable<GetRideDto>>.Success(resultRides);
        }

        public async Task<Result<int>> GetItemsCountAfterFiltesAsync(FindRideQuery queryParams)
        {
            var count = await this.rideRepository.GetItemsCountAfterFiltersAsync(Filter(queryParams));
            return Result<int>.Success(count);
        }

        public async Task<Result<bool>> UpdateRideStatusAsync(Guid userId, UpdateRideStatus newRideStatus)
        {
            if (newRideStatus.Status != RideStatus.Completed && newRideStatus.Status != RideStatus.Cancelled)
            {
                return Result<bool>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            var ride = await this.rideRepository.GetByIdAsync(newRideStatus.RideId, r => r, Include());

            if (ride == null)
            {
                return Result<bool>.Failure(RIDE_NOT_FOUND, HttpStatusCode.NotFound);
            }

            if (userId != ride.DriverId)
            {
                return Result<bool>.Failure(UNATHORIZED_OPERATION, HttpStatusCode.Unauthorized);
            }

            if (Validations.IsDepartureTimeApproaching(ride.DepartureTime) && newRideStatus.Status != RideStatus.Completed)
            {
                _ = await this.feedbackService.DeleteDriverRatingAsync(userId);
            }

            var oldValue = string.Format(OLD_STATUS_FORMAT, ride.Status.ToString());

            ride.Status = newRideStatus.Status;
            await this.rideRepository.UpdateAsync(ride);

            var newValue = string.Format(NEW_STATUS_FORMAT, ride.Status);

            var rideBooks = await this.bookService.GetBookingsAsync(FindRideBookParam(ride.Id));

            if (!rideBooks.IsSuccess)
            {
                return Result<bool>.Success(true);
            }

            var affectedPeople = rideBooks.Value.Select(x => x.Passenger.Email).ToList();

            string operation = (ride.Status == RideStatus.Completed) ? RIDE_COMPLATED : RIDE_CANCELATION;

            await this.NotifyAffectedPeople(operation, affectedPeople, oldValue, newValue, newRideStatus.RideId, userId);

            return Result<bool>.Success(true);
        }

        public async Task<Result<GetRideDto>> CreateRideAsync(Guid userId, CreateRideDto newRideDto)
        {
            if (DateTime.UtcNow > newRideDto.DepartureTime)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            var vehicleResult = await this.vehicleService.GetVehicleAsync(newRideDto.VehicleId);

            if (!vehicleResult.IsSuccess)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.NotFound);
            }

            var startAddressResult = await this.addressService.GetAddressByIdAsync(newRideDto.StartAddressId);

            if (!startAddressResult.IsSuccess)
            {
                return Result<GetRideDto>.Failure(ADDRESS_NOT_FOUND, HttpStatusCode.NotFound);
            }

            var endAddressResult = await this.addressService.GetAddressByIdAsync(newRideDto.EndAddressId);

            if (!endAddressResult.IsSuccess)
            {
                return Result<GetRideDto>.Failure(ADDRESS_NOT_FOUND, HttpStatusCode.NotFound);
            }

            Ride newRide = this.mapper.Map<Ride>(newRideDto);

            var ride = await this.rideRepository.CreateAsync(newRide);

            var rideDto = this.mapper.Map<GetRideDto>(ride);

            return Result<GetRideDto>.Success(rideDto);
        }

        public async Task<Result<bool>> DeleteRideAsync(Guid userId, Guid rideId)
        {
            var ride = await RideRolesValidation(userId, rideId);

            if (ride == null)
            {
                return Result<bool>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            var deleteResult = await this.rideRepository.SoftDeleteAsync(ride);

            return Result<bool>.Success(deleteResult);
        }

        public async Task<Result<GetRideDto>> GetRideDetailsAsync(Guid rideId)
        {
            var result = await this.rideDtoRepository.GetByIdAsync(rideId, r => this.mapper.Map<GetRideDto>(r), this.Include());

            if (result == null)
            {
                return Result<GetRideDto>.Failure(RIDE_NOT_FOUND, HttpStatusCode.NotFound);
            }

            return Result<GetRideDto>.Success(result);
        }

        public async Task<Result<GetRideDto>> UpdateRideStartLocationAsync(Guid userId, Guid rideId, CreateAddressDto newLocation)
        {
            var ride = await this.RideRolesValidation(userId, rideId);

            if (ride == null)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            var freePlaces = await this.bookService.GetFreePlacesAsync(rideId);

            if (!freePlaces.IsSuccess)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            if (freePlaces.Value != ride.FreeSpots)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            if (Validations.IsDepartureTimeApproaching(ride.DepartureTime))
            {
                _ = await this.feedbackService.DeleteDriverRatingAsync(userId);
            }
            var upDatedAddresResult = await this.addressService.UpdateAddressAsync(ride.StartAddress, newLocation);

            if (!upDatedAddresResult.IsSuccess)
            {
                return Result<GetRideDto>.Failure(ADDRESS_NOT_FOUND, HttpStatusCode.NotFound);
            }

            var oldValue = string.Format(OLD_ADDRESS_FORMAT, ride.StartAddress.City, ride.StartAddress.Region, ride.StartAddress.Country);

            ride.StartAddress = upDatedAddresResult.Value;

            var result = await this.geoMapService.GetDistanceAndDurationAsync(ride.StartAddress, ride.EndAddress);

            if (!result.IsSuccess)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            var upDateRide = this.mapper.Map<GetRideDto>(ride);

            upDateRide.Distance = result.Value.Distance;
            upDateRide.Duration = result.Value.Duration;

            var rideBooks = await this.bookService.GetBookingsAsync(FindRideBookParam(rideId));

            if (!rideBooks.IsSuccess)
            {
                return Result<GetRideDto>.Success(upDateRide);
            }

            var affectedPeople = rideBooks.Value.Select(x => x.Passenger.Email).ToList();

            var newValue = string.Format(NEW_ADDRESS_FORMAT, ride.StartAddress.City, ride.StartAddress.Region, ride.StartAddress.Country);

            await this.NotifyAffectedPeople(CHANGED_PICKUP_ADDRESS, affectedPeople, oldValue, newValue, rideId, userId);

            return Result<GetRideDto>.Success(upDateRide);
        }

        public async Task<Result<GetRideDto>> UpdateRideEndLocationAsync(Guid userId, Guid rideId, CreateAddressDto newLocation)
        {
            var ride = await this.RideRolesValidation(userId, rideId);

            if (ride == null)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            var freePlaces = await this.bookService.GetFreePlacesAsync(rideId);

            if (!freePlaces.IsSuccess)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            if (freePlaces.Value != ride.FreeSpots)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            if (Validations.IsDepartureTimeApproaching(ride.DepartureTime))
            {
                _ = await this.feedbackService.DeleteDriverRatingAsync(userId);
            }
            var upDatedAddresResult = await this.addressService.UpdateAddressAsync(ride.EndAddress, newLocation);

            if (!upDatedAddresResult.IsSuccess)
            {
                return Result<GetRideDto>.Failure(ADDRESS_NOT_FOUND, HttpStatusCode.NotFound);
            }

            var oldValue = string.Format(OLD_ADDRESS_FORMAT, ride.EndAddress.City, ride.EndAddress.Region, ride.EndAddress.Country);

            ride.EndAddress = upDatedAddresResult.Value;

            var result = await this.geoMapService.GetDistanceAndDurationAsync(ride.StartAddress, ride.EndAddress);

            if (!result.IsSuccess)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            var upDateRide = this.mapper.Map<GetRideDto>(ride);

            upDateRide.Distance = result.Value.Distance;
            upDateRide.Duration = result.Value.Duration;

            var rideBooks = await this.bookService.GetBookingsAsync(FindRideBookParam(rideId));

            if (!rideBooks.IsSuccess)
            {
                return Result<GetRideDto>.Success(upDateRide);
            }

            var affectedPeople = rideBooks.Value.Select(x => x.Passenger.Email).ToList();

            var newValue = string.Format(NEW_ADDRESS_FORMAT, ride.EndAddress.City, ride.EndAddress.Region, ride.EndAddress.Country);

            await this.NotifyAffectedPeople(CHANGED_DROPOFF_ADDRESS, affectedPeople, oldValue, newValue, rideId, userId);

            return Result<GetRideDto>.Success(upDateRide);
        }

        public async Task<Result<GetRideDto>> UpdateRideVehicleAsync(Guid userId, Guid rideId, Guid VehicleId)
        {
            var ride = await this.RideRolesValidation(userId, rideId);

            if (ride == null)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            var upDatedVehicle = await this.vehicleService.GetVehicleAsync(VehicleId);

            if (!upDatedVehicle.IsSuccess)
            {
                return Result<GetRideDto>.Failure(VEHICLE_NOT_FOUND, HttpStatusCode.NotFound);
            }

            var oldValue = string.Format(OLD_VEHICLE_FORMAT, ride.Vehicle.PlateNumber);

            ride.VehicleId = upDatedVehicle.Value.Id;

            var newRide = await this.rideRepository.UpdateAsync(ride);

            if (newRide == null)
            {
                return Result<GetRideDto>.Failure(RIDE_NOT_FOUND, HttpStatusCode.NotFound);
            }

            var upDatedRide = this.mapper.Map<GetRideDto>(newRide);

            var result = await this.geoMapService.GetDistanceAndDurationAsync(ride.StartAddress, ride.EndAddress);

            if (!result.IsSuccess)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            upDatedRide.Distance = result.Value.Distance;
            upDatedRide.Duration = result.Value.Duration;

            var rideBooks = await this.bookService.GetBookingsAsync(FindRideBookParam(ride.Id));

            if (!rideBooks.IsSuccess)
            {
                return Result<GetRideDto>.Success(upDatedRide);
            }

            var newValue = string.Format(NEW_VEHICLE_FORMAT, upDatedVehicle.Value.Id);

            var affectedPeople = rideBooks.Value.Select(x => x.Passenger.Email).ToList();

            await this.NotifyAffectedPeople(CHANGE_VEHICLE, affectedPeople, oldValue, newValue, rideId, userId);

            return Result<GetRideDto>.Success(upDatedRide);
        }

        public async Task<Result<GetRideDto>> UpdateRideAsync(Guid userId, Guid rideId, UpdateRideDto newRide)
        {
            if (newRide.DepartureTime < DateTime.UtcNow)
            {
                return Result<GetRideDto>.Failure(UNSUPPORTED_OPERATION, HttpStatusCode.Conflict);
            }

            var ride = await this.RideRolesValidation(userId, rideId);

            if (ride == null)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            if (Validations.IsDepartureTimeApproaching(ride.DepartureTime))
            {
                await this.feedbackService.DeleteDriverRatingAsync(userId);
            }

            var oldValue = string.Format((OLD + PROPERTIES_FORMAT), ride.FreeSpots, ride.DepartureTime, ride.Comment, ride.Pets, ride.Smoking, ride.Price, ride.Currency);

            if (newRide.FreeSpots != null && newRide.FreeSpots != ride.FreeSpots)
            {
                var freePlaces = await this.bookService.GetFreePlacesAsync(rideId);

                if (!freePlaces.IsSuccess)
                {
                    return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
                }

                if ((ride.FreeSpots - freePlaces.Value) > newRide.FreeSpots)
                {
                    return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
                }
            }

            ride.FreeSpots = newRide.FreeSpots ?? ride.FreeSpots;
            ride.DepartureTime = newRide.DepartureTime ?? ride.DepartureTime;
            ride.Comment = newRide.Comment ?? ride.Comment;
            ride.Price = newRide.Price ?? ride.Price;
            ride.Pets = newRide.Pets ?? ride.Pets;
            ride.Smoking = newRide.Smoking ?? ride.Smoking;

            var rideAfterUpdate = await this.rideRepository.UpdateAsync(ride);

            if (rideAfterUpdate == null)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }            

            var upDatedRide = this.mapper.Map<GetRideDto>(ride);

            var result = await this.geoMapService.GetDistanceAndDurationAsync(ride.StartAddress, ride.EndAddress);

            if (!result.IsSuccess)
            {
                return Result<GetRideDto>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            upDatedRide.Distance = result.Value.Distance;
            upDatedRide.Duration = result.Value.Duration;

            var rideBooks = await this.bookService.GetBookingsAsync(FindRideBookParam(ride.Id));

            if (!rideBooks.IsSuccess) 
            {                
                return Result<GetRideDto>.Success(upDatedRide);
            }

            var newValue = string.Format((OLD + PROPERTIES_FORMAT), ride.FreeSpots, ride.DepartureTime, ride.Comment, ride.Pets, ride.Smoking, ride.Price, ride.Currency);

            var affectedPeople = rideBooks.Value.Select(x => x.Passenger.Email).ToList();

            await this.NotifyAffectedPeople(CHANGE_VEHICLE, affectedPeople, oldValue, newValue, rideId, userId);

            return Result<GetRideDto>.Success(upDatedRide);
        }

        private async Task<Ride> RideRolesValidation(Guid userId, Guid rideId)
        {
            var ride = await this.rideRepository.GetByIdAsync(rideId, r => r, Include());

            if (ride == null)
            {
                return null;
            }

            if (ride.DepartureTime < DateTime.UtcNow)
            {
                return null;
            }

            var roles = await this.userService.GetUserRolesAsync(userId);

            if (!roles.IsSuccess)
            {
                return null;
            }

            if (userId != ride.DriverId && !roles.Value.Contains(UserRoles.Administrator))
            {
                return null;
            }

            return ride;
        }

        private async Task NotifyAffectedPeople(string operation, List<string> affectedPeople, string oldValue, string newValue, Guid rideId, Guid driverId)
        {
            var user = await this.userService.GetUserDetailsAsync(driverId);
            var userDto = this.mapper.Map<GetUserDto>(user.Value);

            MimeMessage message = this.emailService.CreateRideMessage(operation, affectedPeople, oldValue, newValue, rideId, userDto);
            await this.emailService.SendEmailAsync(message);
        }

        private FindBookingQuery FindRideBookParam(Guid rideId)
        {
            FindBookingQuery findBooking = new FindBookingQuery()
            {
                RideId = rideId,
                StartIndex = 0,
                Take = int.MaxValue
            };
            return findBooking;
        }

        private Func<IQueryable<Ride>, IIncludableQueryable<Ride, object>> Include()
        {
            return tr => tr.Include(tr => tr.StartAddress)
                .Include(tr => tr.EndAddress)
                .Include(tr => tr.Driver)
                .Include(tr => tr.Driver)
                .ThenInclude(or => or.ReceivedFeedbacks.Where(f => !f.IsDeleted && f.Type == FeedbackType.Driver))
                .Include(tr => tr.RideBookings.Where(b => !b.IsDeleted))
                .Include(tr => tr.Vehicle);
        }

        private Func<IQueryable<Ride>, IQueryable<Ride>> Filter(FindRideQuery query)
        {
            return tr => tr
            .Where(tr => !tr.IsDeleted)

            .Where(tr => (query.StartCountry != null) ? tr.StartAddress.Country.ToLower().Contains(query.StartCountry.ToLower()) : true)
            .Where(tr => (query.StartCity != null) ? tr.StartAddress.City.ToLower().Contains(query.StartCity.ToLower()) : true)
            .Where(tr => (query.StartRegion != null) ? tr.StartAddress.Region.ToLower().Contains(query.StartRegion.ToLower()) : true)
            .Where(tr => (query.StartAddressLine != null) ? tr.StartAddress.AddressLine.ToLower().Contains(query.StartAddressLine.ToLower()) : true)

            .Where(tr => (query.EndCountry != null) ? tr.EndAddress.Country.ToLower().Contains(query.EndCountry.ToLower()) : true)
            .Where(tr => (query.EndCity != null) ? tr.EndAddress.City.ToLower().Contains(query.EndCity.ToLower()) : true)
            .Where(tr => (query.EndRegion != null) ? tr.EndAddress.Region.ToLower().Contains(query.EndRegion.ToLower()) : true)
            .Where(tr => (query.EndAddressLine != null) ? tr.EndAddress.AddressLine.ToLower().Contains(query.EndAddressLine.ToLower()) : true)

            .Where(tr => (query.DriverId != null) ? tr.DriverId == query.DriverId : true)
            .Where(tr => (query.Price != null) ? tr.Price == query.Price : true)
            .Where(tr => (query.FreeSpots != null) ?(tr.FreeSpots - tr.RideBookings.Where(b => b.Status == BookingStatus.Аpproved).Sum(b => b.BookedPlaces)) >= query.FreeSpots : true)


            .Where(tr => (query.RideStatus != null) ? tr.Status == query.RideStatus : true)

            .Where(tr => (query.Date != null) ? tr.DepartureTime.Date >= query.Date.Value.Date : true);
        }

        private Func<IQueryable<Ride>, IOrderedQueryable<Ride>> Order(FindRideQuery query)
        {
            if (query.OrderBy == RidesOrderBy.None)
            {
                return null;
            }

            switch (query.OrderBy)
            {
                case RidesOrderBy.StartLocation:
                    if (query.descOrder)
                    {
                        return o => o.OrderByDescending(o => (o.StartAddress.Country + " " + o.StartAddress.City + " " + o.StartAddress.Region + " " + o.StartAddress.AddressLine));
                    }

                    return o => o.OrderBy(o => (o.StartAddress.Country + " " + o.StartAddress.City + " " + o.StartAddress.Region + " " + o.StartAddress.AddressLine));

                case RidesOrderBy.EndLocation:
                    if (query.descOrder)
                    {
                        return o => o.OrderByDescending(o => (o.EndAddress.Country + " " + o.EndAddress.City + " " + o.EndAddress.Region + " " + o.EndAddress.AddressLine));
                    }

                    return o => o.OrderBy(o => (o.EndAddress.Country + " " + o.EndAddress.City + " " + o.EndAddress.Region + " " + o.EndAddress.AddressLine));

                case RidesOrderBy.DriverRating:
                    if (query.descOrder)
                    {
                        return o => o.OrderByDescending(o => o.Driver.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Driver).Average(f => f.Rating));
                    }

                    return o => o.OrderBy(o => o.Driver.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Driver).Average(f => f.Rating));

                case RidesOrderBy.Date:
                    if (query.descOrder)
                    {
                        return o => o.OrderByDescending(o => o.DepartureTime);
                    }

                    return o => o.OrderBy(o => o.DepartureTime);

                default: return null;
            }
        }
    }
}
