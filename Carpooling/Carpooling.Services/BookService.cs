﻿namespace Carpooling.Services
{
    using Carpooling.Models.Entities;

    using MimeKit;

    public class BookService : IBookService
    {
        private const string BOOKING_OPERATION = "Ride Booking";
        private const string BOOKING_CANCELATION = "Cancelling the ride booking";
        private const string BOOKING_ACCEPTED = "Booking accepted";
        private const string REJECT_BOOKING = "Reject your ride booking";
        private const string ALREADY_CANCELED = "Tne book already is canceled";
        private const string BOOK_CHANGED = "Book changed";

        private readonly IRepository<RideBooking, RideBooking> rideBookingRepository;
        private readonly IRepository<RideBooking, GetRideBookingDto> getRideBookingDtoRepository;

        private readonly IRepository<Ride, Ride> rideRepository;

        private readonly IEmailService emailService;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IFeedbackService feedbackService;

        public BookService(IRepository<RideBooking, RideBooking> rideBookingRepository,
                           IRepository<RideBooking, GetRideBookingDto> getRideBookingDtoRepository,
                           IRepository<Ride, Ride> rideRepository,
                           IEmailService emailService,
                           IUserService userService,
                           IFeedbackService feedbackService,
                           IMapper mapper)
        {
            this.rideBookingRepository = rideBookingRepository;
            this.getRideBookingDtoRepository = getRideBookingDtoRepository;
            this.rideRepository = rideRepository;
            this.emailService = emailService;
            this.userService = userService;
            this.feedbackService = feedbackService;
            this.mapper = mapper;
        }

        public async Task<Result<bool>> CreateBookingAsync(Guid userId, CreateBookingDto newBook)
        {
            var rideResult = await this.rideRepository.GetByIdAsync(newBook.RideId, r => r, r => r.Include(r => r.Driver).Include(r => r.StartAddress).Include(r => r.EndAddress));

            if (rideResult == null)
            {
                return Result<bool>.Failure(BOOKING_NOT_FOUND, HttpStatusCode.NotFound);
            }

            var freePlaces = await this.GetFreePlacesAsync(newBook.RideId);

            if (!freePlaces.IsSuccess)
            {
                return Result<bool>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            if (freePlaces.Value < newBook.BookPlaces)
            {
                return Result<bool>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            RideBooking rideBooking = new RideBooking()
            {
                RideId = rideResult.Id,
                UserId = userId,
                BookedPlaces = newBook.BookPlaces,
                Status = BookingStatus.Pending,
                CreatedOn = DateTime.UtcNow
            };

            var book = await this.rideBookingRepository.CreateAsync(rideBooking);
            book.Ride = rideResult;      

            await NotifyAsync(BOOKING_OPERATION, rideResult.Driver.Email, userId, book);

            return Result<bool>.Success(true);
        }

        public async Task<Result<int>> GetFreePlacesAsync(Guid rideId)
        {
            var ride = await this.rideRepository.GetByIdAsync(rideId, r => r);

            if (ride == null)
            {
                return Result<int>.Failure(RIDE_NOT_FOUND, HttpStatusCode.NotFound);
            }

            var declaredPlaces = ride.FreeSpots;

            Func<IQueryable<RideBooking>, IQueryable<RideBooking>> ApprovedFilter = b => b.Where(b => b.Status == BookingStatus.Аpproved);

            var approvedBooks = await this.rideBookingRepository.GetAsync(b => b, ApprovedFilter);

            if (approvedBooks == null || !approvedBooks.Any())
            {
                return Result<int>.Success(declaredPlaces);
            }

            var bookedPlaces = approvedBooks.Sum(b => b.BookedPlaces);

            var freePlaces = declaredPlaces - bookedPlaces;

            if (freePlaces < 0)
            {
                return Result<int>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            return Result<int>.Success(bookedPlaces);
        }

        public async Task<Result<bool>> DeleteBookingAsync(Guid userId, Guid bookId)
        {
            var booking = await rideBookingRepository.GetByIdAsync(bookId, b => b);

            if (booking == null)
            {
                return Result<bool>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            var userRoles = await this.userService.GetUserRolesAsync(userId);

            if (!userRoles.IsSuccess)
            {
                return Result<bool>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }

            if (!userRoles.Value.Contains(UserRoles.Administrator) && booking.UserId != userId)
            {
                return Result<bool>.Failure(UNATHORIZED_OPERATION, HttpStatusCode.Unauthorized);
            }

            return Result<bool>.Success(await this.rideBookingRepository.SoftDeleteAsync(booking));
        }

        public async Task<Result<GetRideBookingDto>> GetBookingDetailsAsync(Guid bookId)
        {
            var booking = await this.getRideBookingDtoRepository.GetByIdAsync(bookId, r => this.mapper.Map<GetRideBookingDto>(r));
            if (booking == null)
            {
                return Result<GetRideBookingDto>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            return Result<GetRideBookingDto>.Success(booking);
        }

        public async Task<Result<IEnumerable<GetRideBookingDto>>> GetBookingsAsync(FindBookingQuery query)
        {
            var result = await this.getRideBookingDtoRepository.GetAsync(b => this.mapper.Map<GetRideBookingDto>(b),
                                                            Filter(query), this.Order(), this.Include(),
                                                            new PageParameters() { StartIndex = query.StartIndex, Take = query.Take });
            if (result == null)
            {
                return Result<IEnumerable<GetRideBookingDto>>.Failure(BOOKING_NOT_FOUND, HttpStatusCode.NotFound);
            }

            return Result<IEnumerable<GetRideBookingDto>>.Success(result);
        }

        public async Task<Result<int>> GetItemsCountAfterFiltesAsync(FindBookingQuery queryParams)
        {
            var count = await this.rideBookingRepository.GetItemsCountAfterFiltersAsync(Filter(queryParams));
            return Result<int>.Success(count);
        }

        public async Task<Result<bool>> BookingCancelationAsync(Guid userId, Guid bookId)
        {
            var book = await this.rideBookingRepository.GetByIdAsync(bookId, b => b);

            if (book == null)
            {
                return Result<bool>.Failure(ResultMessages.INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            bool isCurrect = book.UserId != userId || book.Status == BookingStatus.Canceled || book.Status == BookingStatus.Rejected || book.Ride.DepartureTime < DateTime.UtcNow;

            if (isCurrect)
            {
                return Result<bool>.Failure(ALREADY_CANCELED, HttpStatusCode.BadRequest);
            }

            if (Validations.IsDepartureTimeApproaching(book.Ride.DepartureTime))
            {
                await this.feedbackService.DeletePassengerRatingAsync(userId);
            }

            bool isBeenApproved = book.Status == BookingStatus.Аpproved;

            book.Status = BookingStatus.Canceled;

            if (isBeenApproved)
            {
                await NotifyAsync(BOOKING_CANCELATION, book.Ride.Driver.Email, userId, book);
            }

            var updatedBook = await rideBookingRepository.UpdateAsync(book);
            return Result<bool>.Success(updatedBook != null);
        }

        public async Task<Result<bool>> RejectBookingAsync(Guid userId, Guid bookId)
        {
            var book = await this.rideBookingRepository.GetByIdAsync(bookId, b => b);

            if (book == null)
            {
                return Result<bool>.Failure(ResultMessages.INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            bool isNotCurrect = book.Ride.DriverId != userId || book.Status == BookingStatus.Canceled || book.Status == BookingStatus.Rejected || book.Ride.DepartureTime < DateTime.UtcNow;

            if (isNotCurrect)
            {
                return Result<bool>.Failure(ALREADY_CANCELED, HttpStatusCode.BadRequest);
            }

            if (Validations.IsDepartureTimeApproaching(book.Ride.DepartureTime))
            {
                await this.feedbackService.DeleteDriverRatingAsync(userId);
            }

            bool isBeenApproved = book.Status == BookingStatus.Аpproved;

            book.Status = BookingStatus.Rejected;

            if (isBeenApproved)
            {
                await NotifyAsync(REJECT_BOOKING, book.User.Email, userId, book);
            }

            var updatedBook = await rideBookingRepository.UpdateAsync(book);
            return Result<bool>.Success(updatedBook != null);
        }

        public async Task<Result<bool>> ChangeBookedPassengersAsync(Guid userId, UpdateBookingDto bookingUpdateDto)
        {
            var book = await this.rideBookingRepository.GetByIdAsync(bookingUpdateDto.BookingId, b => b);

            if (book == null)
            {
                return Result<bool>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            bool isInvalid = IsBookingInvalid(book, userId, bookingUpdateDto);

            if (isInvalid)
            {
                return Result<bool>.Failure(INVALID_PARAMS, HttpStatusCode.BadRequest);
            }

            if (Validations.IsDepartureTimeApproaching(book.Ride.DepartureTime))
            {
                await this.feedbackService.DeletePassengerRatingAsync(userId);
            }

            var freePlaces = (await this.GetFreePlacesAsync(book.RideId)).Value;

            RideBooking updatedBooking = UpdateBookingDetails(book, bookingUpdateDto, freePlaces);

            if(updatedBooking == null)
            {
                return Result <bool>.Failure(NOT_ENOUGH_PLACES, HttpStatusCode.BadRequest);
            }

            if (book.Status == BookingStatus.Аpproved)
            {
                await NotifyAsync(BOOK_CHANGED, book.Ride.Driver.Email, userId, book);
            }

            return Result<bool>.Success(updatedBooking != null);
        }

        private RideBooking UpdateBookingDetails(RideBooking booking, UpdateBookingDto bookingUpdateDto, int freePlaces)
        {
            if (booking.Status != BookingStatus.Аpproved && bookingUpdateDto.BookedPlaces <= freePlaces)
            {
                booking.BookedPlaces = bookingUpdateDto.BookedPlaces;
                return rideBookingRepository.UpdateAsync(booking).Result;
            }
            else if (booking.Status == BookingStatus.Аpproved && bookingUpdateDto.BookedPlaces <= (freePlaces + booking.BookedPlaces))
            {
                booking.BookedPlaces = bookingUpdateDto.BookedPlaces;
                return rideBookingRepository.UpdateAsync(booking).Result;
            }
            return null;
        }

        private bool IsBookingInvalid(RideBooking booking, Guid userId, UpdateBookingDto bookingUpdateDto)
        {
            return booking.Ride.DepartureTime < DateTime.UtcNow ||
                   userId != booking.UserId ||
                   bookingUpdateDto.BookedPlaces < 0 ||
                   booking.Status == BookingStatus.Canceled ||
                   booking.Status == BookingStatus.Rejected;
        }

        private async Task NotifyAsync(string title, string recipientEmail, Guid userId, RideBooking booking)
        {
            var userDetailsResult = await userService.GetUserDetailsAsync(userId);
            if (userDetailsResult.IsSuccess)
            {
                GetUserDto userDto = mapper.Map<GetUserDto>(userDetailsResult.Value);
                MimeMessage message = emailService.CreateBookingMessage(title, recipientEmail, userDto, booking);
                await emailService.SendEmailAsync(message);
            }
        }

        private Func<IQueryable<RideBooking>, IQueryable<RideBooking>> Filter(FindBookingQuery query)
        {
            return b => b.Where(b => !b.IsDeleted)
                         .Where(b => (query.UserId != null) ? b.UserId == query.UserId : true)
                         .Where(b => (query.RideId != null) ? b.RideId == query.RideId : true)
                         .Where(b => (query.Status != null) ? b.Status == query.Status : true);
        }

        private Func<IQueryable<RideBooking>, IIncludableQueryable<RideBooking, object>> Include()
        {
            return b => b.Include(b => b.Ride)
                            .Include(b => b.Ride)
                                .ThenInclude(r => r.Driver)
                            .Include(b => b.Ride)
                                .ThenInclude(r => r.StartAddress)
                            .Include(b => b.Ride)
                                .ThenInclude(r => r.EndAddress)
                         .Include(b => b.User)
                            .Include(b => b.User)
                                .ThenInclude(u => u.ReceivedFeedbacks);
        }

        private Func<IQueryable<RideBooking>, IOrderedQueryable<RideBooking>> Order()
        {
            return b => b.OrderByDescending(b => b.CreatedOn);
        }
    }
}
