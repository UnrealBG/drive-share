﻿
namespace Carpooling.Services
{
    using System;
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.Entities;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Contracts;
    using Carpooling.Services.Helpers;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Carpooling.Models.DtoModels.UpdateDtos;
    using Carpooling.DataAccess.Repository.Contracts;
    using Microsoft.EntityFrameworkCore.Query;
    using Microsoft.EntityFrameworkCore;
    using System.Net;
    using AutoMapper;
    using Carpooling.Models.GlobalConstants;
    using Carpooling.Models.Enums;
    using Microsoft.IdentityModel.Tokens;
    using Carpooling.Models.DtoModels.Contracts;
    using static Carpooling.Models.Enums.CrudAction;

    public class UserService : IUserService
    {
        private readonly IRepository<User, User> userRepository;
        private readonly IMapper mapper;

        public UserService(IRepository<User, User> repository, IMapper mapper)
        {
            this.userRepository = repository;
            this.mapper = mapper;
        }

        public async Task<Result<GetUserDto>> CreateUserAsync(IUserInboundDto createUserDto, ClaimsIdentity claimsIdentity)
        {
            if (!Guid.TryParse(claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value, out Guid azureUserId))
            {
                return Result<GetUserDto>.Failure(USER_WRONG_PARAMETERS, HttpStatusCode.BadRequest);
            }

            var userIsUnique = await this.IsUserUniqueAsync(azureUserId, createUserDto, crudAction: Create);
            if (userIsUnique.Value)
            {
                // Create a new user object
                var userToCreate = this.mapper.Map<User>(createUserDto);
                userToCreate.Roles = UserRoles.User;
                userToCreate.CreatedOn = DateTime.UtcNow;
                userToCreate.Id = Guid.NewGuid();
                userToCreate.AzureUserId = azureUserId;

                // Add user to database
                var createdUser = await this.userRepository.CreateAsync(userToCreate);

                if (createdUser is null)
                {
                    return Result<GetUserDto>.Failure(USER_CREATE_ERROR, HttpStatusCode.InternalServerError);
                }
                return Result<GetUserDto>.Success(this.mapper.Map<GetUserDto>(createdUser));
            }

            return Result<GetUserDto>.Failure(userIsUnique.Error, userIsUnique.StatusCode);
        }

        public async Task<Result<ICollection<GetUserDto>>> GetUserAsync(FindUserQuery query)
        {
            var users = await this.QueryUsersAsync(query);

            if (users.Count == 0)
            {
                return Result<ICollection<GetUserDto>>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }
            var userDtoCollection = users.Select(u => mapper.Map<GetUserDto>(u)).ToList();
            return Result<ICollection<GetUserDto>>.Success(userDtoCollection);
        }

        public async Task<Result<GetUserDto>> GetUserByIdAsync(Guid userId)
        {
            var user = await this.userRepository.GetByIdAsync(userId, u => u, Include());
            if (user is null)
            {
                return Result<GetUserDto>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }
            var userDto = mapper.Map<GetUserDto>(user);
            return Result<GetUserDto>.Success(userDto);
        }

        public async Task<Result<int>> GetItemsCountAfterFiltesAsync(FindUserQuery query)
        {
            var count = await this.userRepository.GetItemsCountAfterFiltersAsync(Filter(query));
            return Result<int>.Success(count);
        }

        public async Task<Result<User>> GetUserByAzureUserIdAsync(Guid azureUserId)
        {
            var query = new FindUserQuery()
            {
                AzureUserId = azureUserId
            };
            var user = await this.QueryUsersAsync(query);

            if (user.Count == 0)
            {
                return Result<User>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }
            var userDto = mapper.Map<User>(user.First());
            return Result<User>.Success(userDto);

        }

        public async Task<Result<User>> GetUserDetailsAsync(Guid userId)
        {
            var user = await this.userRepository.GetByIdAsync(userId, u => u);
            if (user is null)
            {
                return Result<User>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }
            return Result<User>.Success(user);
        }

        public async Task<Result<User>> GetUserDetailsAsync(ClaimsIdentity claimsIdentity)
        {
            var azureUserId = await this.GetAzureUserIdAsync(claimsIdentity);

            if (!azureUserId.IsSuccess)
            {
                return Result<User>.Failure(USER_WRONG_PARAMETERS, HttpStatusCode.BadRequest);
            }

            var query = new FindUserQuery()
            {
                AzureUserId = azureUserId.Value
            };
            var user = await this.QueryUsersAsync(query);

            if (user.Count == 0)
            {
                return Result<User>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }

            return Result<User>.Success(user.First());
        }

        public async Task<Result<User>> GetUserDetailsAsync(ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal is null)
            {
                return Result<User>.Failure(USER_WRONG_PARAMETERS, HttpStatusCode.BadRequest);
            }

            return await this.GetUserDetailsAsync(claimsPrincipal.Identity as ClaimsIdentity);
        }

        public async Task<Result<bool>> DeleteUserAsync(Guid userId)
        {
            var user = await this.GetUserDetailsAsync(userId);
            if (user is null)
            {
                return Result<bool>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            var deleteUserResult = await this.userRepository.DeleteAsync(userId);

            return deleteUserResult
                ? Result<bool>.Success(deleteUserResult)
                : Result<bool>.Failure(USER_DELETE_ERROR, HttpStatusCode.BadRequest);
        }

        public async Task<Result<List<UserRoles>>> GetUserRolesAsync(Guid userId)
        {
            var user = await this.userRepository.GetByIdAsync(userId, u => u);
            if (user is null)
            {
                return Result<List<UserRoles>>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
            }

            var roles = new List<UserRoles>
            {
                user.Roles
            };

            return Result<List<UserRoles>>.Success(roles);
        }

        public async Task<Result<GetUserDto>> UpdateUserAsync(IUserInboundDto updateUserDto, ClaimsIdentity claimsIdentity)
        {
            if (!Guid.TryParse(claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value, out Guid azureUserId))
            {
                return Result<GetUserDto>.Failure(USER_WRONG_PARAMETERS, HttpStatusCode.BadRequest);
            }

            var userIsUnique = await this.IsUserUniqueAsync(azureUserId, updateUserDto, crudAction: Update);
            if (userIsUnique.Value)
            {
                var user = await this.GetUserByAzureUserIdAsync(azureUserId);
                if (user is null)
                {
                    return Result<GetUserDto>.Failure(INVALID_PARAMS, HttpStatusCode.Conflict);
                }

                var userToUpdate = mapper.Map(updateUserDto, user.Value);

                var updateResult = await this.userRepository.UpdateAsync(userToUpdate);

                return (updateResult is not null)
                    ? Result<GetUserDto>.Success(mapper.Map<GetUserDto>(userToUpdate))
                    : Result<GetUserDto>.Failure(USER_UPDATE_ERROR, HttpStatusCode.BadRequest);
            }

            return Result<GetUserDto>.Failure(userIsUnique.Error, userIsUnique.StatusCode);
        }

        public async Task<Result<bool>> UserExists(FindUserQuery query)
        {
            var result = await QueryUsersAsync(query);

            if (result.Count > 0)
            {
                return Result<bool>.Success(true);
            }
            return Result<bool>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
        }

        public async Task<Result<bool>> UserExists(ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal is null)
            {
                return Result<bool>.Failure(UNATHORIZED_OPERATION, HttpStatusCode.Unauthorized);
            }

            var azureUserId = await GetAzureUserIdAsync(claimsPrincipal);

            if (azureUserId.IsSuccess)
            {
                var query = new FindUserQuery()
                {
                    AzureUserId = azureUserId.Value
                };

                var result = await QueryUsersAsync(query);

                if (result.Count > 0)
                {
                    return Result<bool>.Success(true);
                }
            }
            else
            {
                return Result<bool>.Failure(azureUserId.Error, azureUserId.StatusCode);
            }

            return Result<bool>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
        }

        public async Task<Result<bool>> IsUserUniqueAsync(Guid azureUserId, IUserInboundDto userDto, CrudAction crudAction)
        {
            var queryUsername = new FindUserQuery() { Username = userDto.Username };
            var queryEmail = new FindUserQuery() { Email = userDto.Email };
            var queryPhoneNumber = new FindUserQuery() { PhoneNumber = userDto.PhoneNumber };

            var userNameQueryResult = await this.QueryUsersAsync(queryUsername);
            if (userNameQueryResult.Count > 0)
            {
                bool isDifferentUser = userNameQueryResult.First().AzureUserId != azureUserId;

                if (crudAction == Create || crudAction == Update && isDifferentUser)
                {
                    return Result<bool>.Failure(USERNAME_ALREADY_EXIST, HttpStatusCode.BadRequest);
                }
            }

            var emailQueryResult = await this.QueryUsersAsync(queryEmail);
            if (emailQueryResult.Count > 0)
            {
                bool isDifferentUser = emailQueryResult.First().AzureUserId != azureUserId;

                if (crudAction == Create || crudAction == Update && isDifferentUser)
                {
                    return Result<bool>.Failure(EMAIL_ALREADY_EXIST, HttpStatusCode.BadRequest);
                }
            }

            var phoneQueryResult = await this.QueryUsersAsync(queryPhoneNumber);
            if (phoneQueryResult.Count > 0)
            {
                bool isDifferentUser = phoneQueryResult.First().AzureUserId != azureUserId;

                if (crudAction == Create || crudAction == Update && isDifferentUser)
                {
                    return Result<bool>.Failure(USERPHONE_ALREADY_EXIST, HttpStatusCode.BadRequest);
                }
            }

            return Result<bool>.Success(true);
        }

        public async Task<Result<Guid>> GetAzureUserIdAsync(ClaimsIdentity identity)
        {
            if (identity is null)
            {
                return Result<Guid>.Failure(UNATHORIZED_OPERATION, HttpStatusCode.Unauthorized);
            }

            if (Guid.TryParse(identity.FindFirst(ClaimTypes.NameIdentifier).Value, out Guid azureUserId))
            {
                return Result<Guid>.Success(azureUserId);
            }

            return Result<Guid>.Failure(USER_WRONG_PARAMETERS, HttpStatusCode.BadRequest);
        }

        public async Task<Result<Guid>> GetAzureUserIdAsync(ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal is null)
            {
                return Result<Guid>.Failure(UNATHORIZED_OPERATION, HttpStatusCode.Unauthorized);
            }

            var claimsIdentity = claimsPrincipal.Identity as ClaimsIdentity;

            return await this.GetAzureUserIdAsync(claimsIdentity);
        }

        public async Task<Result<bool>> BlockUserAsync(string username, ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal.IsInRole("Administrator"))
            {
                var user = await this.QueryUsersAsync(new FindUserQuery() { Username = username });
                if (user.Count > 0)
                {
                    var userToBlock = user.First();
                    userToBlock.IsBlocked = true;
                    var updateResult = await this.userRepository.UpdateAsync(userToBlock);
                    if (updateResult is not null)
                    {
                        return Result<bool>.Success(true);
                    }
                    return Result<bool>.Failure(USER_UPDATE_ERROR, HttpStatusCode.BadRequest);
                }
                return Result<bool>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }
            return Result<bool>.Failure(UNATHORIZED_OPERATION, HttpStatusCode.Unauthorized);
        }

        public async Task<Result<bool>> UnblockUser(string username, ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal.IsInRole("Administrator"))
            {
                var user = await this.QueryUsersAsync(new FindUserQuery() { Username = username });
                if (user.Count > 0)
                {
                    var userToBlock = user.First();
                    userToBlock.IsBlocked = false;
                    var updateResult = await this.userRepository.UpdateAsync(userToBlock);
                    if (updateResult is not null)
                    {
                        return Result<bool>.Success(true);
                    }
                    return Result<bool>.Failure(USER_UPDATE_ERROR, HttpStatusCode.BadRequest);
                }
                return Result<bool>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }
            return Result<bool>.Failure(UNATHORIZED_OPERATION, HttpStatusCode.Unauthorized);
        }

        public async Task<Result<User>> GetUserByUserNameAsync(string username, ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal.IsInRole("Administrator"))
            {
                var user = await this.QueryUsersAsync(new FindUserQuery() { Username = username });
                if (user.Count > 0)
                {
                    return Result<User>.Success(user.First());
                }
                return Result<User>.Failure(USER_NOT_FOUND, HttpStatusCode.NotFound);
            }
            return Result<User>.Failure(UNATHORIZED_OPERATION, HttpStatusCode.Unauthorized);
        }

        private async Task<ICollection<User>> QueryUsersAsync(FindUserQuery query)
        {
            return await this.userRepository.GetAsync(
                u => u,
                this.Filter(query),
                this.Order(query),
                this.Include(),
                new PageParameters()
                {
                    StartIndex = query.StartIndex,
                    Take = query.Take
                });
        }

        private Func<IQueryable<User>, IQueryable<User>> Filter(FindUserQuery query)
        {
            return u => u.Where(u => (query.Id != null) ? u.Id == query.Id : true)
                         .Where(u => (query.AzureUserId != null) ? u.AzureUserId == query.AzureUserId : true)
                         .Where(u => (query.Username != null) ? u.Username == query.Username : true)
                         .Where(u => (query.FirstName != null) ? u.FirstName == query.FirstName : true)
                         .Where(u => (query.LastName != null) ? u.LastName == query.LastName : true)
                         .Where(u => (query.Email != null) ? u.Email == query.Email : true)
                         .Where(u => (query.PhoneNumber != null) ? u.PhoneNumber == query.PhoneNumber : true)
                         .Where(u => (query.Roles != null) ? u.Roles == query.Roles : true)
                         .Where(u => (query.IsBlocked != null) ? u.IsBlocked == query.IsBlocked : true);
        }

        private Func<IQueryable<User>, IIncludableQueryable<User, object>> Include()
        {
            return u => u.Include(r => r.OrganizedRides)
                            .Include(r => r.RideBookings)
                            .Include(f => f.GivenFeedbacks)
                            .Include(f => f.ReceivedFeedbacks);
        }

        private Func<IQueryable<User>, IOrderedQueryable<User>> Order(FindUserQuery query)
        {
            if (query.UsersOrderBy != null)
            {
                switch (query.UsersOrderBy)
                {
                    case UsersOrderBy.FirstName:
                        if (query.Descending)
                        {
                            return u => u.OrderByDescending(r => r.FirstName);
                        }
                        else
                        {
                            return u => u.OrderBy(r => r.FirstName);
                        }

                    case UsersOrderBy.LastName:
                        if (query.Descending)
                        {
                            return u => u.OrderByDescending(u => u.LastName);
                        }
                        else
                        {
                            return u => u.OrderBy(u => u.LastName);
                        }

                    case UsersOrderBy.DriverRating:
                        if (query.Descending)
                        {
                            return u => u.OrderByDescending(u => (u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Driver).Sum(f => f.Rating)));
                        }
                        else
                        {
                            return u => u.OrderBy(u => (u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Driver).Sum(f => f.Rating)));
                        }

                    case UsersOrderBy.PassengerRating:
                        if (query.Descending)
                        {
                            return u => u.OrderByDescending(u => (u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Passenger).Sum(f => f.Rating)));
                        }
                        else
                        {
                            return u => u.OrderBy(u => (u.ReceivedFeedbacks.Where(f => f.Type == FeedbackType.Driver).Sum(f => f.Rating)));
                        }
                }
            }
            return null;
        }
    }
}
