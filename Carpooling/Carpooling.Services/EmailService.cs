﻿namespace Carpooling.Services
{
    using MimeKit;
    using MimeKit.Utils;

    using static Carpooling.Services.Helpers.RideBookingEmailPropertiesKey;

    public class EmailService : IEmailService
    {
        private const string COMPANY_NAME = "JourneyJoiner";
        private const string TEMPLATE_DIRECTORY = "EmailResponseHtml";
        private const string IMAGE_DIRECTORY = "Images";
        private const string RATING_FORMAT = "0.#";
        private const string BOOKINGTEMPLATE_HTML = "BookingEmailTemplate.html";
        private const string BOOK_RIDE_IMAGE = "journeytogether.jpg";
        private const string RIDE_EMAIL_TEMPLATE_HTML = "RideEmailTemplate.html";
        private const string PASSENGER_RATING_ANNOUNCEMENT = "Passenger raiting : ";
        private const string DRIVER_RATING_ANNOUNCEMENT = "Driver raiting : ";
        private const string BOOKING_SUBJECT = "Booking from ";
        private const string RIDE_SUBJECT = "Modifies of ride";

        private readonly IEmailClient client;

        public EmailService(IEmailClient client)
        {
            this.client = client;
        }

        public MimeMessage CreateBookingMessage(string operationName, string emailAddressTo, GetUserDto fromUser, RideBooking book)
        {
            var email = new MimeMessage();
            email.From.Add(new MailboxAddress(COMPANY_NAME, this.client.EmailFrom));
            var emailMessage = new MimeMessage();
            string[] to = { emailAddressTo };
            email.To.AddRange(to.Select(x => new MailboxAddress(x, x)));
            email.Subject = $"{BOOKING_SUBJECT}{fromUser.Name}";
            var bodyBuilder = new BodyBuilder();
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = Path.Combine(baseDir, TEMPLATE_DIRECTORY, IMAGE_DIRECTORY, BOOK_RIDE_IMAGE);
            var image = bodyBuilder.LinkedResources.Add(filePath);
            image.ContentId = MimeUtils.GenerateMessageId();
            MimeEntity avatar = null;
            if (fromUser.ImgUrl != null)
            {
                avatar = bodyBuilder.LinkedResources.Add(fromUser.ImgUrl);
                avatar.ContentId = MimeUtils.GenerateMessageId();
            }

            baseDir = AppDomain.CurrentDomain.BaseDirectory;
            filePath = Path.Combine(baseDir, TEMPLATE_DIRECTORY, BOOKINGTEMPLATE_HTML);

            using (StreamReader SourceReader = System.IO.File.OpenText(filePath))
            {
                bodyBuilder.HtmlBody = SourceReader.ReadToEnd();
            }

            string rating = (book.Status == BookingStatus.Аpproved || book.Status == BookingStatus.Canceled) ? $"{PASSENGER_RATING_ANNOUNCEMENT}{fromUser.PassengerRating.ToString(RATING_FORMAT)}" : $"{DRIVER_RATING_ANNOUNCEMENT}{fromUser.DriverRating.ToString(RATING_FORMAT)}";
            string startAddress = book.Ride.StartAddress.City + ", " + book.Ride.StartAddress.Region + ", " + book.Ride.StartAddress.Country;
            string endAddress = book.Ride.EndAddress.City + ", " + book.Ride.EndAddress.Region + ", " + book.Ride.EndAddress.Country;

            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(TEMPLATE_IMG, image.ContentId);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(AVATAR, (avatar != null) ? avatar.ContentId : null);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(BOOKING_OPERATION, operationName);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(BOOKING_ID, book.Id.ToString());
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(NAME, fromUser.Name);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(USERNAME, fromUser.UserName);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(EMAIL, fromUser.Email);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(PHONE_NUMBER, fromUser.PhoneNumber);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RATING, rating);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(STATUS, book.Status.ToString());
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RIDE_ID, book.RideId.ToString());
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(START_ADDRESS, startAddress);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(END_ADDRESS, endAddress);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(INFO_LINK, this.client.AppLink);

            email.Body = bodyBuilder.ToMessageBody();

            return email;
        }

        public MimeMessage CreateRideMessage(string operation, List<string> affectedPeople, string oldValue, string newValue, Guid rideId, GetUserDto driver)
        {
            var email = new MimeMessage();
            email.From.Add(new MailboxAddress(COMPANY_NAME, this.client.EmailFrom));
            var emailMessage = new MimeMessage();
            List<string> to = affectedPeople;
            email.To.AddRange(to.Select(x => new MailboxAddress(x, x)));
            email.Subject = $"{RIDE_SUBJECT} from {driver.Name}";
            var bodyBuilder = new BodyBuilder();
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = Path.Combine(baseDir, TEMPLATE_DIRECTORY, IMAGE_DIRECTORY, BOOK_RIDE_IMAGE);
            var image = bodyBuilder.LinkedResources.Add(filePath);
            image.ContentId = MimeUtils.GenerateMessageId();
            MimeEntity avatar = null;
            if (driver.ImgUrl != null)
            {
                avatar = bodyBuilder.LinkedResources.Add(driver.ImgUrl);
                avatar.ContentId = MimeUtils.GenerateMessageId();
            }

            baseDir = AppDomain.CurrentDomain.BaseDirectory;
            filePath = Path.Combine(baseDir, TEMPLATE_DIRECTORY, RIDE_EMAIL_TEMPLATE_HTML);

            using (StreamReader SourceReader = System.IO.File.OpenText(filePath))
            {
                bodyBuilder.HtmlBody = SourceReader.ReadToEnd();
            }

            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.TEMPLATE_IMG, image.ContentId);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.AVATAR, (avatar != null) ? avatar.ContentId : null);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.OPERATION, operation);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.RIDE_ID, rideId.ToString());
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.OLD_VALUE, oldValue);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.NEW_VALUE, newValue);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.DRIVER_NAME, driver.Name);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.USER_NAME, driver.UserName);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.EMAIL, driver.Email);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.PHONENUMBER, driver.PhoneNumber);
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(RideEmailPropertiesKey.DRIVER_RATING, driver.DriverRating.ToString(RATING_FORMAT));
            bodyBuilder.HtmlBody = bodyBuilder.HtmlBody.Replace(INFO_LINK, this.client.AppLink);

            email.Body = bodyBuilder.ToMessageBody();

            return email;
        }

        public async Task SendEmailAsync(MimeMessage message)
        {
            await this.client.SendAsync(message);
        }
    }
}
