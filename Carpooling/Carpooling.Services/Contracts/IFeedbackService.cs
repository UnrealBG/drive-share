﻿namespace Carpooling.Services.Contracts
{
    public interface IFeedbackService
    {
        Task<Result<bool>> CreateFeedbackAsync(Guid userId, CreatFeedbackDto newFeedback);

        Task<Result<IEnumerable<GetRecivedFeedbackDto>>> GetRecivedFeedbacksAsync(FindFeedbackQuery query);

        Task<Result<IEnumerable<GetGivenFeedbackDto>>> GetGivenFeedbacksAsync(FindFeedbackQuery query);

        Task<Result<double>> GetRatingAsync(Guid userId, FeedbackType rideRole);

        Task<Result<bool>> DeleteFeedbackAsync(Guid userId, Guid feedbackId);

        Task<Result<bool>> DeleteDriverRatingAsync(Guid driverId);

        Task<Result<bool>> DeletePassengerRatingAsync(Guid passengerId);

        Task<Result<int>> GetItemsCountAfterFiltesAsync(FindFeedbackQuery queryParams);
    }
}

