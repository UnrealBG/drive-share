﻿namespace Carpooling.Services.Contracts
{
    public interface IAddressService
    {
        Task<Result<Address>> CreateAddressAsync(CreateAddressDto newAddress);

        Task<Result<Address>> UpdateAddressAsync(Address address, CreateAddressDto newAddress);

        Task<Result<bool>> DeleteAddressAsync(Guid id);

        Task<Result<Address>> GetAddressByIdAsync(Guid addressId);
    }
}
