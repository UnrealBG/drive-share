﻿namespace Carpooling.Services.Contracts
{
    using BingMapsRESTToolkit;

    using Address = Models.Entities.Address;

    public interface IGeoLocationService
    {
        Task<Result<Location>> GeocodeAsync(CreateAddressDto addressDto);

        Task<Result<string>> ReverseGeocodeAsync(Location location);

        Task<Result<List<string>>> GetNearbyAddressAsync(Location location);

        Task<Result<(double Distance, double Duration)>> GetDistanceAndDurationAsync(Address startAddress, Address endAddress);
    }
}
