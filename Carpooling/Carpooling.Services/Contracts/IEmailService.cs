﻿namespace Carpooling.Services.Contracts
{
    using MimeKit;

    public interface IEmailService
    {
        Task SendEmailAsync(MimeMessage message);

        MimeMessage CreateBookingMessage(string operationName, string emailAddressTo, GetUserDto fromUser, RideBooking book);

        MimeMessage CreateRideMessage(string operation, List<string> affectedPeople, string oldValue, string newValue, Guid rideId, GetUserDto driver);
    }
}
