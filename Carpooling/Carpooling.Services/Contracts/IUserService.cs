﻿namespace Carpooling.Services.Contracts
{
    using Carpooling.Models.DtoModels.Contracts;
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.DtoModels.UpdateDtos;
    using Carpooling.Models.Entities;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Helpers;
    using System.Security.Claims;

    public interface IUserService
    {
        Task<Result<bool>> BlockUserAsync(string username, ClaimsPrincipal claimsPrincipal);
        Task<Result<GetUserDto>> CreateUserAsync(IUserInboundDto createUserDto, ClaimsIdentity claimsIdentity);
        Task<Result<bool>> DeleteUserAsync(Guid userId);
        Task<Result<int>> GetItemsCountAfterFiltesAsync(FindUserQuery query);
        Task<Result<Guid>> GetAzureUserIdAsync(ClaimsIdentity identity);
        Task<Result<Guid>> GetAzureUserIdAsync(ClaimsPrincipal claimsPrincipal);
        Task<Result<ICollection<GetUserDto>>> GetUserAsync(FindUserQuery query);
        Task<Result<User>> GetUserByAzureUserIdAsync(Guid azureUserId);
        Task<Result<GetUserDto>> GetUserByIdAsync(Guid userId);
        Task<Result<User>> GetUserByUserNameAsync(string username, ClaimsPrincipal claimsPrincipal);
        Task<Result<User>> GetUserDetailsAsync(ClaimsIdentity claimsIdentity);
        Task<Result<User>> GetUserDetailsAsync(ClaimsPrincipal claimsPrincipal);
        Task<Result<User>> GetUserDetailsAsync(Guid userId);
        Task<Result<List<UserRoles>>> GetUserRolesAsync(Guid userId);
        Task<Result<bool>> IsUserUniqueAsync(Guid azureUserId, IUserInboundDto userDto, CrudAction crudAction);
        Task<Result<bool>> UnblockUser(string username, ClaimsPrincipal claimsPrincipal);
        Task<Result<GetUserDto>> UpdateUserAsync(IUserInboundDto updateUserDto, ClaimsIdentity claimsIdentity);
        Task<Result<bool>> UserExists(ClaimsPrincipal claimsPrincipal);
        Task<Result<bool>> UserExists(FindUserQuery query);
    }
}
