﻿namespace Carpooling.Services.Contracts
{
    public interface IBookService
    {
        Task<Result<bool>> CreateBookingAsync(Guid userId, CreateBookingDto newBook);

        Task<Result<bool>> BookingCancelationAsync(Guid userId, Guid bookId);

        Task<Result<bool>> RejectBookingAsync(Guid userId, Guid bookId);

        Task<Result<bool>> ChangeBookedPassengersAsync(Guid userId, UpdateBookingDto bookinUpdateDto);

        Task<Result<GetRideBookingDto>> GetBookingDetailsAsync(Guid bookId);

        Task<Result<bool>> DeleteBookingAsync(Guid userId, Guid bookId);

        Task<Result<IEnumerable<GetRideBookingDto>>> GetBookingsAsync(FindBookingQuery query);

        Task<Result<int>> GetItemsCountAfterFiltesAsync(FindBookingQuery queryParams);

        Task<Result<int>> GetFreePlacesAsync(Guid rideId);
    }
}

