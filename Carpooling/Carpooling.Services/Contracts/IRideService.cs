﻿namespace Carpooling.Services.Contracts
{
    public interface IRideService
    {
        Task<Result<IEnumerable<GetRideDto>>> GetRidesAsync(FindRideQuery queryParams);

        Task<Result<int>> GetItemsCountAfterFiltesAsync(FindRideQuery queryParams);

        Task<Result<bool>> UpdateRideStatusAsync(Guid userId, UpdateRideStatus newRideStatus);

        Task<Result<GetRideDto>> CreateRideAsync(Guid userId, CreateRideDto newRide);

        Task<Result<bool>> DeleteRideAsync(Guid userId, Guid rideId);

        Task<Result<GetRideDto>> GetRideDetailsAsync(Guid rideId);

        Task<Result<GetRideDto>> UpdateRideStartLocationAsync(Guid userId, Guid rideId, CreateAddressDto newLocation);

        Task<Result<GetRideDto>> UpdateRideEndLocationAsync(Guid userId, Guid rideId, CreateAddressDto newLocation);

        Task<Result<GetRideDto>> UpdateRideVehicleAsync(Guid userId, Guid rideId, Guid VehicleId);

        Task<Result<GetRideDto>> UpdateRideAsync(Guid userId, Guid rideId, UpdateRideDto newRide);
    }
}

