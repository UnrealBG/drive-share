﻿namespace Carpooling.Services.Contracts
{
    public interface IVehicleService
    {
        Task<Result<GetVehicleDto>> CreateVehicleAsync(Guid userId, CreateVehicleDto newVehicle);

        Task<Result<GetVehicleDto>> UpdateVehicleAsync(Guid vehicleId, CreateVehicleDto newVehicle);

        Task<Result<bool>> DeleteVehicleAsync(Guid id);

        Task<Result<GetVehicleDto>> GetVehicleAsync(Guid id);

        Task<Result<List<GetVehicleDto>>> GetVehiclesAsync(Guid userId);

        Task<bool> IsPlateNumberExistAsync(string plateNumber);
    }
}
