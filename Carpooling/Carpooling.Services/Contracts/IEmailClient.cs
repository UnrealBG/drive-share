﻿namespace Carpooling.Services.Contracts
{
    using MimeKit;

    public interface IEmailClient
    {
        string EmailFrom { get; }

        string AppLink { get; }

        Task SendAsync(MimeMessage message);
    }
}
