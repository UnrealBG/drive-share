﻿namespace Carpooling.Services.ExternalServices
{
    using Carpooling.Services.Contracts;
    using MailKit.Net.Smtp;
    using MimeKit;

    public class EmailClient : IEmailClient 
    {
        private readonly EmailConfiguration emailConfig;
        public EmailClient(EmailConfiguration emailConfig)
        {
            this.emailConfig = emailConfig;
        }

        public string EmailFrom
        {
            get { return this.emailConfig.UserName; }
        }

        public string AppLink
        {
            get
            {
                return this.emailConfig.AppLink;
            }
        }

        public async Task SendAsync(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {                    
                    await client.ConnectAsync(this.emailConfig.SmtpServer, this.emailConfig.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync(this.emailConfig.UserName, this.emailConfig.Password);
                    await client.SendAsync(mailMessage);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    await client.DisconnectAsync(true);
                    client.Dispose();
                }
            }
        }        
    }
}
