﻿namespace Carpooling.Services.ExternalServices
{
    using BingMapsRESTToolkit;

    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json.Linq;

    using Address = Models.Entities.Address;

    public class GeoLocationService : IGeoLocationService
    {
        private readonly string bingMapsKey;

        public GeoLocationService(IConfiguration config)
        {
            this.bingMapsKey = config["BingMapsKey"];
        }

        public async Task<Result<Location>> GeocodeAsync(CreateAddressDto addressDto)
        {
            var geocodeRequest = new GeocodeRequest()
            {
                Query = $"{addressDto.Country}, {addressDto.City}, {addressDto.Region}",
                IncludeIso2 = true,
                MaxResults = 5,
                BingMapsKey = this.bingMapsKey
            };

            var response = await ServiceManager.GetResponseAsync(geocodeRequest);

            if (response == null || response.ResourceSets == null || response.ResourceSets.Length == 0 ||
                response.ResourceSets[0].Resources == null || response.ResourceSets[0].Resources.Length == 0)
            {
                return Result<Location>.Failure("Invalid address according to Bing Maps API", HttpStatusCode.BadRequest);
            }

            var location = response.ResourceSets[0].Resources[0] as Location;
            return Result<Location>.Success(location);
        }

        public async Task<Result<string>> ReverseGeocodeAsync(Location location)
        {
            var reverseGeocodeRequest = new ReverseGeocodeRequest
            {
                Point = new Coordinate(location.Point.Coordinates[0], location.Point.Coordinates[1]),
                IncludeEntityTypes = new List<EntityType> { EntityType.Address },
                BingMapsKey = this.bingMapsKey
            };

            var response = await ServiceManager.GetResponseAsync(reverseGeocodeRequest);

            if (response == null || response.ResourceSets == null || response.ResourceSets.Length == 0 ||
                response.ResourceSets[0].Resources == null || response.ResourceSets[0].Resources.Length == 0)
            {
                return Result<string>.Failure("Invalid address according to Bing Maps API", HttpStatusCode.BadRequest);
            }

            var results = response.ResourceSets[0].Resources.OfType<Location>().ToList();
            var addresses = results.Select(x => x.Address.FormattedAddress).FirstOrDefault();

            return Result<string>.Success(addresses);
        }

        public async Task<Result<List<string>>> GetNearbyAddressAsync(Location location)
        {
            var reverseGeocodeResult = await this.ReverseGeocodeAsync(location);

            if (!reverseGeocodeResult.IsSuccess)
            {
                return Result<List<string>>.Failure(reverseGeocodeResult.Error, reverseGeocodeResult.StatusCode);
            }

            return Result<List<string>>.Success(new List<string> { reverseGeocodeResult.Value });
        }

        public async Task<Result<(double Distance, double Duration)>> GetDistanceAndDurationAsync(Address startAddress, Address endAddress)
        {
            var request = new DistanceMatrixRequest()
            {
                BingMapsKey = this.bingMapsKey,
                Origins = new List<SimpleWaypoint> { new SimpleWaypoint(startAddress.Latitude, startAddress.Longitude) },
                Destinations = new List<SimpleWaypoint> { new SimpleWaypoint(endAddress.Latitude, endAddress.Longitude) },
                TravelMode = TravelModeType.Driving
            };

            var response = await ServiceManager.GetResponseAsync(request);

            if (response != null &&
                response.ResourceSets != null &&
                response.ResourceSets.Length > 0 &&
                response.ResourceSets[0].Resources != null &&
                response.ResourceSets[0].Resources.Length > 0)
            {
                var result = (DistanceMatrix)response.ResourceSets[0].Resources[0];
                return Result<(double Distance, double Duration)>.Success(
                    (result.Results[0].TravelDistance, result.Results[0].TravelDuration));
            }

            return Result<(double Distance, double Duration)>.Failure("Distance and duration calculation failed", HttpStatusCode.NotFound);
        }
    }
}
