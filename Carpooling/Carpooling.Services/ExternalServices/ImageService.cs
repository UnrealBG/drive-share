﻿namespace Carpooling.Services.ExternalServices
{
    using CloudinaryDotNet;

    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;

    public class ImageService : IImageService
    {
        private readonly Account account;

        public ImageService(IConfiguration configuration)
        {
            this.account = new Account(
                configuration["Cloudinary:CloudName"],
                configuration["Cloudinary:ApiKey"],
                configuration["Cloudinary:ApiSecret"]);
        }

        public async Task<string> UploadAsync(IFormFile file)
        {
            var clint = new Cloudinary(this.account);
            var uploadFileResult = await clint.UploadAsync(
                                       new CloudinaryDotNet.Actions.ImageUploadParams()
                                           {
                                               File = new FileDescription(file.FileName, file.OpenReadStream()),
                                               DisplayName = file.FileName
                                           });

            if (uploadFileResult.StatusCode != null && uploadFileResult.StatusCode == HttpStatusCode.OK)
            {
                return uploadFileResult.SecureUrl.ToString();
            }

            return null;
        }
    }
}
