﻿namespace Carpooling.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IRepository<Vehicle, Vehicle> repository;

        private readonly IRepository<Vehicle, GetVehicleDto> getVehicleRepo;

        private readonly IMapper mapper;

        private readonly IUserService userService;

        public VehicleService(IRepository<Vehicle, Vehicle> repository, IRepository<Vehicle, GetVehicleDto> getVehicleRepo, IMapper mapper, IUserService userService)
        {
            this.repository = repository;
            this.getVehicleRepo = getVehicleRepo;
            this.mapper = mapper;
            this.userService = userService;
        }

        public async Task<Result<GetVehicleDto>> CreateVehicleAsync(Guid userId, CreateVehicleDto newVehicle)
        {
            if (newVehicle == null)
            {
                return Result<GetVehicleDto>.Failure(VEHICLE_NOT_FOUND, HttpStatusCode.BadRequest);
            }
            
            var mappedVehicle = this.mapper.Map<Vehicle>(newVehicle);
            mappedVehicle.UserId = userId;

            var vehicle = await this.repository.CreateAsync(mappedVehicle);

            var vehicleDto = this.mapper.Map<GetVehicleDto>(vehicle);

            return Result<GetVehicleDto>.Success(vehicleDto);
        }

        public async Task<Result<GetVehicleDto>> UpdateVehicleAsync(Guid vehicleId, CreateVehicleDto newVehicle)
        {
            var oldVehicle = await this.repository.GetByIdAsync(vehicleId, v => v);

            if (oldVehicle == null)
            {
                return Result<GetVehicleDto>.Failure(string.Format(VEHICLE_UPDATE_ERROR, vehicleId), HttpStatusCode.BadRequest);
            }

            var mappedVehicle = this.mapper.Map(newVehicle, oldVehicle);

            var vehicle = await this.repository.UpdateAsync(mappedVehicle);

            var vehicleDto = this.mapper.Map<GetVehicleDto>(vehicle);

            return Result<GetVehicleDto>.Success(vehicleDto);
        }

        public async Task<Result<bool>> DeleteVehicleAsync(Guid id)
        {
            var vehicle = await this.repository.GetByIdAsync(id, v => v);

            if (vehicle == null)
            {
                  return Result<bool>.Failure(VEHICLE_NOT_FOUND, HttpStatusCode.BadRequest);
            }

            await this.repository.SoftDeleteAsync(vehicle);

            return Result<bool>.Success(true);
        }

        public async Task<Result<GetVehicleDto>> GetVehicleAsync(Guid id)
        {
            var vehicle = await this.repository.GetByIdAsync(id, v => v);

            if (vehicle == null)
            {
                return Result<GetVehicleDto>.Failure(VEHICLE_NOT_FOUND, HttpStatusCode.BadRequest);
            }

            var vehicleDto = this.mapper.Map<GetVehicleDto>(vehicle);

            return Result<GetVehicleDto>.Success(vehicleDto);
        }

        public async Task<Result<List<GetVehicleDto>>> GetVehiclesAsync(Guid userId)
        {
            var vehicles = await this.getVehicleRepo.GetAsync(v => this.mapper.Map<GetVehicleDto>(v),Filter(userId));

            if (vehicles == null)
            {
                return Result<List<GetVehicleDto>>.Failure(VEHICLE_NOT_FOUND, HttpStatusCode.BadRequest);
            }

            return Result<List<GetVehicleDto>>.Success(vehicles.ToList());
        }

        public async Task<bool> IsPlateNumberExistAsync(string plateNumber)
        {
            var vehicles = await this.repository.GetAsync(v => v, v => v.Where(x => x.PlateNumber == plateNumber));

            if (vehicles != null && vehicles.Any())
            {
                return true;
            }

            return false;
        }

        private Func<IQueryable<Vehicle>, IQueryable<Vehicle>> Filter(Guid userId)
        {
            return v => v.Where(x => x.UserId == userId && !x.IsDeleted);
        }
    }
}