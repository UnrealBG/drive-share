﻿namespace Carpooling.Models.GlobalConstants
{
    public static class ResultMessages
    {
        public const string SUCCESS_BOOKED = "You have successfully booked your ride!";
        public const string RIDE_CREATE_SUCCESS = "Ride created successfully.";
        public const string RIDE_DELETE_SUCCESS = "Ride deleted successfully.";
        public const string RIDE_NOT_FOUND = "Ride was not found.";
        public const string INVALID_PARAMS = "Invalid input.";
        public const string INVALID_LOCATION = "Invalid address.";
        public const string UNSUPPORTED_OPERATION = "Unallowed operation.";
        public const string UNATHORIZED_OPERATION = "Unauthorized operation.";
        public const string RIDE_UPDATED_SUCCESS = "Updated successfully RIDE: .";
        public const string RIDE_UPDATE_ERROR = "Couldn't find RIDE with .";
        public const string COUNTRY_NULL_ERROR = "Country cannot be null.";
        public const string COUNTRY_DELETE_SUCCESSFULL = "Country Successfully deleted.";
        public const string ADDRESS_CREATE_SUCCESS = "Address created succesfully.";
        public const string ADDRESS_DELETED_SUCCESS = "Address was successfully deleted.";
        public const string ADDRESS_NOT_FOUND = "Address could not be found.";
        public const string CITY_CREATE_SUCCESS = "City Successfully created.";
        public const string CITY_DELETE_SUCCESSFULL = "City Successfully deleted.";
        public const string CITY_NULL_ERROR = "City cannot be null.";
        public const string CITY_ADDED = "City added to Country.";
        public const string CITY_EXISTS = "City already exists.";
        public const string USER_WRONG_PARAMETERS = "Wrong Parameters.";
        public const string USER_CREATE_SUCCESS = "User created successfully.";
        public const string USER_CREATE_ERROR = "User could not be created.";
        public const string USER_DELETE_ERROR = "User could not be deleted.";
        public const string USER_UPDATE_ERROR = "Couldn't find user with .";
        public const string USER_NOT_FOUND = "User not found.";
        public const string USER_DELETED = "User deleted successfully.";
        public const string USER_ALREADY_EXIST = "User already exist.";
        public const string USERNAME_ALREADY_EXIST = "Username already exist.";
        public const string USEREMAIL_ALREADY_EXIST = "User with that email already exist.";
        public const string USERPHONE_ALREADY_EXIST = "User with that phone number already exist.";
        public const string EMAIL_ALREADY_EXIST = "Email already exist.";
        public const string EMAIL_SENDING_FAILED = "Email sending failed.";
        public const string SUCCESS_BLOCKED = "Successfully blocked.";
        public const string SUCCESS_UNBLOCKED = "Successfully unblocked.";
        public const string VEHICLE_CREATE_SUCCESS = "Vehicle created successfully.";
        public const string VEHICLE_DELETE_SUCCESS = "Vehicle deleted successfully.";
        public const string VEHICLE_NOT_FOUND = "Vehicle was not found.";
        public const string VEHICLE_UPDATE_ERROR = "Couldn't find vehicle with {0}.";
        public const string VEHICLE_UPDATE_SUCCESS = "Updated successfully vehicle: .";
        public const string VEHICLE_ALREADY_EXIST = "Vehicle already exists.";
        public const string BOOKING_NOT_FOUND = "Ride booking not found.";
        public const string FEEDBACK_NOT_FOUND = "Feedback not found.";
        public const string NOT_ENOUGH_PLACES = "Not enough places.";

        public const int TIME_APPROACHING_IN_HOURS = 1;        
    }
}