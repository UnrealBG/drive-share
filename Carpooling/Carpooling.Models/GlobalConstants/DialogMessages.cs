﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carpooling.Models.GlobalConstants
{
    public static class DialogMessages
    {
        public const string USER_BLOCK_CONFIRMATION = "Are you sure you want to block user {0}?";
        public const string USER_UNBLOCK_CONFIRMATION = "Are you sure you want to unblock user {0}?";
        public const string USER_DELETE_CONFIRMATION = "Are you sure you want to delete user {0}?";
    }
}
