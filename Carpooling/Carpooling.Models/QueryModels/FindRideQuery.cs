﻿namespace Carpooling.Models.QueryModels
{
    using Carpooling.Models.Enums;

    public class FindRideQuery : PageParameters
    {
        public string StartCountry { get; set; }

        public string StartCity { get; set; }

        public string StartRegion { get; set; }

        public string StartAddressLine { get; set; }

        public string EndCountry { get; set; }

        public string EndCity { get; set; }

        public string EndRegion { get; set; }

        public string EndAddressLine { get; set; }

        public RideStatus? RideStatus { get; set; } = Enums.RideStatus.Active;

        public Guid? DriverId { get; set; } = null;

        public decimal? Price { get; set; } = null;

        public int? FreeSpots { get; set; } = null;

        public DateTime? Date { get; set; } = null;

        public RidesOrderBy OrderBy { get; set; } = RidesOrderBy.DriverRating;

        public bool descOrder = true;
    }
}
