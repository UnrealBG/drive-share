﻿namespace Carpooling.Models.QueryModels
{
    using Carpooling.Models.Enums;

    public class FindBookingQuery : PageParameters
    {
        public Guid? RideId { get; set; }

        public Guid? UserId { get; set; }

        public BookingStatus? Status { get; set; }
    }
}
