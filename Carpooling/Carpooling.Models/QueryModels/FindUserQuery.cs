﻿using Carpooling.Models.Entities;
using Carpooling.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carpooling.Models.QueryModels
{
    public class FindUserQuery : PageParameters
    {
        public Guid? Id { get; set; }
        public Guid? AzureUserId { get; set; }

        public string? Username { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public string? Email { get; set; }

        public string? PhoneNumber { get; set; }

        public UserRoles? Roles { get; set; }

        public bool? IsBlocked { get; set; }

        public UsersOrderBy? UsersOrderBy { get; set; }

        public bool Descending { get; set; } = true;
    }
}
