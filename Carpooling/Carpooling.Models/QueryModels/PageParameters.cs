﻿namespace Carpooling.Models.QueryModels
{
    public class PageParameters
    {
        public int StartIndex { get; set; } = 0;

        public int Take { get; set; } = 10;
    }
}
