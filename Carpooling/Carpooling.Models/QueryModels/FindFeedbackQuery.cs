﻿namespace Carpooling.Models.QueryModels
{
    using Carpooling.Models.Enums;

    public class FindFeedbackQuery : PageParameters
    {
        public FeedbackType? Type { get; set; }

        public Guid? RecipientId { get; set; }

        public Guid? SenderId { get; set; }
    }
}
