﻿namespace Carpooling.Models.Entities
{
    using System.ComponentModel.DataAnnotations;

    using Carpooling.Models.Enums;

    public class Feedback : BaseEntity
    {
        [Range(0, 5)]
        public int Rating { get; set; }

        public DateTime CreatedDate { get; set; }

        public string? Comment { get; set; }

        public Guid SenderId { get; set; }

        public User Sender { get; set; }

        public Guid ReceiverId { get; set; }

        public User Receiver { get; set; }

        public FeedbackType Type { get; set; }
    }
}
