﻿namespace Carpooling.Models.Entities.Contracts
{
    public interface IBaseEntity
    {
        Guid Id { get; set; }

        bool IsDeleted { get; set; }
    }
}
