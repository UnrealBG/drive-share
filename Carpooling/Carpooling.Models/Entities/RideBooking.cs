﻿namespace Carpooling.Models.Entities
{
    using Carpooling.Models.Enums;

    public class RideBooking : BaseEntity
    {
        public Guid UserId { get; set; }
        
        public DateTime CreatedOn { get; set; }

        public int BookedPlaces { get; set; }

        public User? User { get; set; }

        public Guid RideId { get; set; }

        public Ride? Ride { get; set; }

        public BookingStatus Status { get; set; }
    }
}
