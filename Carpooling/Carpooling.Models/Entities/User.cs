﻿namespace Carpooling.Models.Entities
{
    using Carpooling.Models.Enums;
    using System.ComponentModel.DataAnnotations;
    using System.Text.Json.Serialization;

    public class User : BaseEntity
    {
        public Guid AzureUserId { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string Username { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string FirstName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string LastName { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [MinLength(13, ErrorMessage = "Phone number have to be in format +359887766554")]
        public string PhoneNumber { get; set; }

        public string? ProfileImageUrl { get; set; }

        public DateTime CreatedOn { get; set; }

        public UserRoles Roles { get; set; }

        public bool IsBlocked { get; set; }

        public ICollection<Ride> OrganizedRides { get; set; }

        public ICollection<RideBooking> RideBookings { get; set; }

        public ICollection<Feedback> GivenFeedbacks { get; set; }

        public ICollection<Feedback> ReceivedFeedbacks { get; set; }

        public ICollection<Vehicle> Vehicles { get; set; }
    }
}
