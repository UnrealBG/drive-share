﻿namespace Carpooling.Models.Entities
{
    public class Vehicle : BaseEntity
    {
        public string Make { get; set; }

        public string Model { get; set; }

        public string PlateNumber { get; set; }

        public string? Color { get; set; }

        public string? Description { get; set; }

        public string? ImageUrl { get; set; }

        public Guid UserId { get; set; }

        public virtual User? User { get; set; }
    }
}
