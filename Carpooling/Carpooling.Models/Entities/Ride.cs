﻿namespace Carpooling.Models.Entities
{
    using Carpooling.Models.Enums;

    public class Ride : BaseEntity
    {
        public Guid StartAddressId { get; set; }

        public Address StartAddress { get; set; }

        public Guid EndAddressId { get; set; }

        public Address EndAddress { get; set; }

        public DateTime DepartureTime { get; set; }

        public int FreeSpots { get; set; }

        public string? Comment { get; set; }

        public Guid DriverId { get; set; }

        public User? Driver { get; set; }

        public decimal Price { get; set; }

        public string Currency { get; set; }

        public Guid VehicleId { get; set; }

        public Vehicle? Vehicle { get; set; }

        public bool? Smoking { get; set; }

        public bool? Pets { get; set; }

        public ICollection<RideBooking> RideBookings { get; set; }

        public RideStatus Status { get; set; }
    }
}
