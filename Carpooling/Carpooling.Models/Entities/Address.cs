﻿using System.Net;

namespace Carpooling.Models.Entities
{
    public class Address : BaseEntity
    {
        public string City { get; set; }

        public string Country { get; set; }

        public string Region { get; set; }

        public string AddressLine { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public override string ToString()
        {
            return Country + ", " + Region + ", " + City + ", " + AddressLine;
        }
    }
}
