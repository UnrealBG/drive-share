﻿namespace Carpooling.Models.Enums
{
    public enum RideStatus
    {
        Active,
        Completed,
        Cancelled
    }
}
