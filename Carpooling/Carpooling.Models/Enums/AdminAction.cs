﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carpooling.Models.Enums
{
    public enum AdminAction
    {
        Block,
        Unblock,
        Delete,
    }
}
