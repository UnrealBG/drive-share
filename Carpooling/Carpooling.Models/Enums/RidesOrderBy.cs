﻿namespace Carpooling.Models.Enums
{
    public enum RidesOrderBy
    {
        None,
        DriverRating,
        Date,
        StartLocation,
        EndLocation
    }
}
