﻿namespace Carpooling.Models.Enums
{
    public enum BookingStatus
    {
        Pending,
        Аpproved,
        Rejected,
        Canceled
    }
}
