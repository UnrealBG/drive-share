﻿namespace Carpooling.Models.Enums
{
    public enum UsersOrderBy
    {
        FirstName,
        LastName,
        DriverRating,
        PassengerRating
    }
}
