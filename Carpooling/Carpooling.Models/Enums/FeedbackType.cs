﻿namespace Carpooling.Models.Enums
{
    public enum FeedbackType
    {
        Passenger,
        Driver
    }
}
