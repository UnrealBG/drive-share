﻿namespace Carpooling.Models.Enums
{
    public enum UserRoles
    {
        Administrator,
        User
    }
}
