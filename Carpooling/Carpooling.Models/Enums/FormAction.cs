﻿namespace Carpooling.Models.Enums
{
    public enum FormAction
    {
        Create,
        Update,
    }
}
