﻿namespace Carpooling.Models.Enums
{
    public enum CrudAction
    {
        Create,
        Read,
        Update,
        Delete,
    }
}
