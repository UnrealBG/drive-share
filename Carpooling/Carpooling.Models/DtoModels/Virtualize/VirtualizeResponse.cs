﻿namespace Carpooling.Models.DtoModels.Virtualize
{
    public class VirtualizeResponse<TResult> where TResult : class
    {
        public List<TResult> Items { get; set; }
        public int ItemsCount { get; set; }
    }
}
