﻿namespace Carpooling.Models.DtoModels.CreateDtos
{
    public class GetAddressDto
    {
        public string Country { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string AddressLine { get; set; }
    }
}
