﻿namespace Carpooling.Models.DtoModels.CreateDtos
{
    public class CreateBookingDto
    {
        public Guid RideId { get; set; }

        public int BookPlaces { get; set; }
    }
}
