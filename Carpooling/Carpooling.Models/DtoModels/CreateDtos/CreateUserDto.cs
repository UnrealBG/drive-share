﻿
namespace Carpooling.Models.DtoModels.CreateDtos
{
    using System.ComponentModel.DataAnnotations;
    using Carpooling.Models.DtoModels.Contracts;

    public class CreateUserDto : IUserInboundDto
    {
        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string Username { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string FirstName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string LastName { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [Required, MinLength(13, ErrorMessage = "Phone number have to be in format +359887766554")]
        public string PhoneNumber { get; set; }

        [Url]
        public string? ProfileImageUrl { get; set; }
    }
}
