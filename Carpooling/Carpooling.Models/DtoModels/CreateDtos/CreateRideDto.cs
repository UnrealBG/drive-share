﻿namespace Carpooling.Models.DtoModels.CreateDtos
{
    using Carpooling.Models.Entities;

    public class CreateRideDto
    {
        public Guid StartAddressId { get; set; }

        public Guid EndAddressId { get; set; }

        public int FreeSpots { get; set; }

        public DateTime DepartureTime { get; set; }

        public bool? Pets { get; set; }

        public bool? Smoking { get; set; }

        public string Comment { get; set; }

        public decimal Price { get; set; }

        public string Currency { get; set; }

        public Guid VehicleId { get; set; }

        public Guid DriverId { get; set; }
    }
}
