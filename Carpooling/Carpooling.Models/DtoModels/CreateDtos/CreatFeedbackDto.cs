﻿namespace Carpooling.Models.DtoModels.CreateDtos
{
    using Carpooling.Models.Enums;
    using System.ComponentModel.DataAnnotations;

    public class CreatFeedbackDto
    {
        [Required]
        public Guid ReciverId { get; set; }

        [Required]
        public string Comment { get; set; }

        [Required]
        public int Rating { get; set; }

        [Required]
        public FeedbackType Type { get; set; }
    }
}
