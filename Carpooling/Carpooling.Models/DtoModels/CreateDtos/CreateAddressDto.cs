﻿namespace Carpooling.Models.DtoModels.CreateDtos
{
    public class CreateAddressDto
    {
        public string Country { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string? AddressLine { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }
    }
}
