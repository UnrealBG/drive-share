﻿namespace Carpooling.Models.DtoModels.Contracts
{
    public interface IUserOutBoundDto
    {
        DateTime CreatedOn { get; set; }
        int DriverCommentCount { get; set; }
        double DriverRating { get; set; }
        string Email { get; set; }
        string ImgUrl { get; set; }
        bool IsBlocked { get; set; }
        string Name { get; set; }
        int PassengerCommentsCount { get; set; }
        double PassengerRating { get; set; }
        string PhoneNumber { get; set; }
        string UserName { get; set; }
    }
}