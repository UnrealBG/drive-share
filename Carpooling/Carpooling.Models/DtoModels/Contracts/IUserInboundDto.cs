﻿namespace Carpooling.Models.DtoModels.Contracts
{
    public interface IUserInboundDto
    {
        string Username { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }
        string PhoneNumber { get; set; }
        string? ProfileImageUrl { get; set; }
    }
}