﻿namespace Carpooling.Models.DtoModels.UpdateDtos
{
    public class UpdateVehicleDto
    {
        public string? Make { get; set; }

        public string? Model { get; set; }

        public string? PlateNumber { get; set; }

        public string? Color { get; set; }

        public string? ImageUrl { get; set; }

        public string? Description { get; set; }
    }
}
