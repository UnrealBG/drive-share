﻿namespace Carpooling.Models.DtoModels.UpdateDtos
{
    using Carpooling.Models.Enums;
    using System.ComponentModel.DataAnnotations;

    public class UpdateRideStatus
    {
        [Required]
        public Guid RideId { get; set; }
        [Required]
        public RideStatus Status { get; set; }
    }
}
