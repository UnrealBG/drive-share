﻿using Carpooling.Models.DtoModels.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carpooling.Models.DtoModels.UpdateDtos
{
    public class UpdateUserDto : IUserInboundDto
    {
        [StringLength(20, MinimumLength = 2, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string Username { get; set; }

        [StringLength(20, MinimumLength = 2, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string FirstName { get; set; }

        [StringLength(20, MinimumLength = 2, ErrorMessage = "The {0} must be between {2} and {1} characters long.")]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [MinLength(13, ErrorMessage = "Phone number have to be in format +359887766554")]
        public string PhoneNumber { get; set; }

        [Url]
        public string ProfileImageUrl { get; set; }
    }
}
