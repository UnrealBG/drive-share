﻿namespace Carpooling.Models.DtoModels.UpdateDtos
{
    public class UpdateRideDto
    {
        public int? FreeSpots { get; set; }

        public DateTime? DepartureTime { get; set; }

        public string? Comment { get; set; }

        public decimal? Price { get; set; }

        public bool? Pets { get; set; }

        public bool? Smoking { get; set; }
    }
}
