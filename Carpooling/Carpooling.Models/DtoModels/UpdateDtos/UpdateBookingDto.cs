﻿namespace Carpooling.Models.DtoModels.UpdateDtos
{
    using Carpooling.Models.Enums;
    using System.ComponentModel.DataAnnotations;

    public class UpdateBookingDto
    {
        [Required]
        public Guid BookingId { get; set; }

        [Required]
        public int BookedPlaces { get; set; }
    }
}
