﻿using Carpooling.Models.DtoModels.Contracts;

namespace Carpooling.Models.DtoModels.GetDtos
{

    public class GetUserDto : IUserOutBoundDto
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string UserName { get; set; }

        public double DriverRating { get; set; }

        public double PassengerRating { get; set; }

        public int PassengerCommentsCount { get; set; }

        public int DriverCommentCount { get; set; }

        public string ImgUrl { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsBlocked { get; set; }
    }
}
