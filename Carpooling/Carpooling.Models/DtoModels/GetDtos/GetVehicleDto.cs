﻿namespace Carpooling.Models.DtoModels.GetDtos
{
    public class GetVehicleDto
    {
        public Guid Id { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string ImgUrl { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }

        public string LicensePlate { get; set; }
    }
}
