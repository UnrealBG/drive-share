﻿namespace Carpooling.Models.DtoModels.GetDtos.AbstractClasses
{
    using Carpooling.Models.Enums;

    public abstract class GetFeedbackDto
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public int Rating { get; set; }
        public FeedbackType Type { get; set; }
    }
}
