﻿namespace Carpooling.Models.DtoModels.GetDtos
{
    using Carpooling.Models.DtoModels.GetDtos.AbstractClasses;

    public class GetGivenFeedbackDto : GetFeedbackDto
    {
        public string RecipientName { get; set; }
    }
}
