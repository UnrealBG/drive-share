﻿namespace Carpooling.Models.DtoModels.GetDtos
{
    using Carpooling.Models.Entities;

    public class GetRideDto
    {
        public Guid Id { get; set; }

        public GetUserDto Driver { get; set; }

        public GetVehicleDto Vehicle { get; set; }

        public Address StartAddress { get; set; }

        public Address EndAddress { get; set; }

        public double Price { get; set; }

        public string Currency { get; set; }

        public int FreeSpots { get; set; }

        public DateTime DepartureTime { get; set; }

        public double Distance { get; set; }

        public double Duration { get; set; }

        public int ApprovedBooks { get; set; }

        public int PendingBooks { get; set; }

        public string Status { get; set; }

        public bool? Smoking { get; set; }

        public bool? Pets { get; set; }

        public string Comment { get; set; }
    }
}
