﻿namespace Carpooling.Models.DtoModels.GetDtos
{
    using Carpooling.Models.DtoModels.GetDtos.AbstractClasses;

    public class GetRecivedFeedbackDto : GetFeedbackDto
    {
        public string AuthorName { get; set; }
    }
}
