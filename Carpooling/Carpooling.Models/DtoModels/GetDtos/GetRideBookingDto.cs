﻿namespace Carpooling.Models.DtoModels.GetDtos
{
    using Carpooling.Models.Entities;
    using Carpooling.Models.Enums;

    public class GetRideBookingDto
    {
        public Guid Id { get; set; }

        public GetUserDto Passenger { get; set; }

        public string RideId { get; set; }

        public BookingStatus Status { get; set; }

        public Ride? Ride { get; set; }
    }
}
