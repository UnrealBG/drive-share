﻿namespace Carpooling.DataAccess.Data
{
    using System.IO;

    using Carpooling.Models.Entities;
    using Carpooling.Models.Enums;

    using Microsoft.EntityFrameworkCore;

    public class DbInitializer : IDbInitializer
    {
        private readonly CarpoolingDbContext ctx;

        private Random rand;

        public DbInitializer(CarpoolingDbContext ctx)
        {
            this.ctx = ctx;
            var seed = DateTime.UtcNow.Day+DateTime.UtcNow.Hour+DateTime.UtcNow.Minute;
            this.rand = new Random(seed);
        }

        public void Initialize()
        {
            try
            {
                if (this.ctx.Database.GetPendingMigrations().Any())
                {
                    this.ctx.Database.Migrate();
                }

                if (!this.ctx.Users.Any())
                {
                    this.GenerateUsers(60);
                    this.CreatAddresses();
                    this.CreateVehicle(50);
                    this.CreateRides();
                    this.CreateDriverFeedback();
                    this.CreatePassengersFeedback();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Create users
        /// </summary>
        /// <param name="count">Up to 60</param>
        private void GenerateUsers(int count)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "BgNames.txt");
            if (File.Exists(path) && count <= 60)
            {
                string text = File.ReadAllText(path);
                List<User> users = new List<User>();
                var addDeysMax = 91;
                var addHoursMax = 12;

                using (StringReader sr = new StringReader(text))
                {
                    string line = sr.ReadLine();
                    int index = 1;
                    int phonNumEnd = 1000;
                    while (line != null)
                    {
                        string[] names = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (names.Length == 2)
                        {
                            var addDays = rand.Next(addDeysMax);
                            var addHours = rand.Next(addHoursMax);
                            var date = DateTime.UtcNow.AddDays(-addDays).AddHours(addHours);
                            User newUser = new User()
                            {
                                AzureUserId = Guid.NewGuid(),
                                Username = $"{names[0].ToLower()}_{index}",
                                Email = $"{names[1]}@gmail.com",
                                FirstName = names[0],
                                LastName = names[1],
                                Roles = UserRoles.User,
                                CreatedOn = date,

                                PhoneNumber = $"088{rand.Next(10)}88{index % 10}{phonNumEnd++}",
                                IsBlocked = false
                            };

                            users.Add(newUser);

                            index++;
                        }
                        line = sr.ReadLine();
                    }
                }

                int usersCount = users.Count;
                HashSet<int> conectedIndex = new HashSet<int>();

                for (int iteration = 0; iteration < count; iteration++)
                {
                    int index = 0;
                    do
                    {
                        index = rand.Next(usersCount);
                    } while (conectedIndex.Contains(index));
                    conectedIndex.Add(index);
                    this.ctx.Users.Add(users[index]);
                }

                ctx.SaveChanges();
            }
        }

        private void CreatAddresses()
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "Adresses.txt");
            if (File.Exists(path))
            {
                string text = File.ReadAllText(path);

                using (StringReader sr = new StringReader(text))
                {
                    var line = sr.ReadLine();
                    while (line != null)
                    {
                        var addressItems = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        Address address = new Address()
                        {
                            Country = addressItems[0].Trim(),
                            City = addressItems[2].Trim(),
                            Region = addressItems[1].Trim(),
                            AddressLine = addressItems[3].Trim(),
                            Latitude = double.Parse(addressItems[4].Trim()),
                            Longitude = double.Parse(addressItems[5].Trim())
                        };
                        this.ctx.Addresses.Add(address);
                        line = sr.ReadLine();
                    }
                    ctx.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Create Vehicles
        /// </summary>
        /// <param name="carCount"> Up to 100 </param>
        private void CreateVehicle(int carCount)
        {
            if (carCount <= 100)
            {
                string[] colors = { "Green", "Yellow", "Brown", "Ping", "Magenta", "Blue", "Sky blue", "Lime", "Red" };
                string[] licentNumber = { "BP {0} BG", "BH {0} BG", "Y {0} BG", "C {0} BG", "A {0} BG", "E {0} BG", "CH {0} BG", "H {0} BG", "M {0} BG", "B {0} BG", "PB {0} BG", "X {0} BG" };
                string[] description = { "Comfort", "Standard" };
                string[] makes = { "BMW", "Renault", "Peugeot", "Seat", "WV", "Nissan", "Suzuki" };
                string[][] models =
                    {
                        new string[] { "X5", "X3", "i3", "i4"},
                        new string[] { "Megan", "Clio", "Capture", "Arkana"},
                        new string[] { "508", "2008", "308", "5008"},
                        new string[] {"Ibiza", "Leon", "Ateca", "CUPRA Born"},
                        new string[] {"Golf", "Polo", "Tiguan", "Arteon"},
                        new string[] {"X-TAIL", "QASHQAI", "JUKE", "LEAF"},
                        new string[] {"VITARA", "S-CROSS", "IGNIS", "SWACE"}
                    };

                User[] users = ctx.Users.ToArray();
                var startNumber = 1001;
                var step = 10;

                for (int i = 0; i < carCount; i++)
                {
                    var car = new Vehicle();

                    var index = rand.Next(colors.Length);
                    car.Color = colors[index];

                    index = rand.Next(licentNumber.Length);
                    car.PlateNumber = string.Format(licentNumber[index], startNumber);

                    var makeIndex = rand.Next(models.Length);
                    var modelIndex = rand.Next(models[makeIndex].Length);
                    car.Make = makes[makeIndex];
                    car.Model = models[makeIndex][modelIndex];

                    index = rand.Next(description.Length);
                    car.Description = description[index];

                    index = rand.Next(users.Length);
                    car.UserId = users[index].Id;

                    startNumber = startNumber + step;

                    this.ctx.Vehicles.Add(car);
                }

                this.ctx.SaveChanges();
            }
        }

        private void CreateRides()
        {
            var freeSpotsMax = 4;
            var addDeysMax = 10;
            var addHoursMax = 12;

            var priceMax = 100;
            string[] currencies = { "Lv", "€" };

            var usersIds = this.ctx.Vehicles.Select(v => v.UserId).Distinct().ToArray();
            var addresses = this.ctx.Addresses.ToArray();
            var vehicles = this.ctx.Vehicles.ToArray();

            HashSet<int> ids = new HashSet<int>();
            
            for(int i=0; i<usersIds.Length; i++)
            {
                var ride = new Ride();

                var index = rand.Next(addresses.Length);
                ride.StartAddressId = addresses[index].Id;

                var old = index;
                index = rand.Next(addresses.Length);
                while(old == index)
                {
                    index = rand.Next(addresses.Length);
                }
                ride.EndAddressId = addresses[index].Id;

                var addDays = rand.Next(addDeysMax);
                var addHours = rand.Next(addHoursMax);
                var date = DateTime.UtcNow.AddDays(addDays).AddHours(addHours);
                ride.DepartureTime = date;

                var freeSpots = rand.Next(freeSpotsMax);
                ride.FreeSpots = freeSpots+1;
              
                ride.DriverId = usersIds[i];

                var usersVehicles = vehicles.Where(v => v.UserId == ride.DriverId).ToArray();
                index = rand.Next(usersVehicles.Length);
                ride.VehicleId = vehicles[index].Id;

                var price = rand.Next(priceMax);
                ride.Price = price;

                var currencyIndex = rand.Next(currencies.Length);
                ride.Currency = currencies[currencyIndex];

                ride.Smoking = (rand.Next(2) == 1)? true : false;

                ride.Pets = (rand.Next(2) == 1) ? true : false;

                this.ctx.Rides.Add(ride);
            }

            this.ctx.SaveChanges();
        }

        private void CreateDriverFeedback()
        {
            string[] comments = {"Bad driver", "Nothing special driver", "Just right driver", "Good driver", "Very good driver" };
            var rides = ctx.Rides.ToList();
            var users = ctx.Users.ToList();

            foreach(var ride in rides)
            {
                for (int i = 0; i < 10; i++)
                {
                    var rating = rand.Next(5);
                    var senderIndex = rand.Next(users.Count);                    
                    
                    Feedback feedback = new Feedback()
                    {
                        Rating = rating,
                        Comment = comments[rating],
                        SenderId = users[senderIndex].Id,
                        ReceiverId = ride.DriverId,
                        Type = FeedbackType.Driver
                    };

                    ctx.Feedbacks.Add(feedback);
                }
            }

            ctx.SaveChanges();
        }

        private void CreatePassengersFeedback()
        {
            string[] comments = { "Bad passenger", "Nothing special passenger", "Just right passenger", "Good passenger", "Very good passenger" };
            var users = ctx.Users.ToList();
            int conunt = (int)(users.Count * 0.3);
            for(int j = 0; j < conunt; j++)
            {
                var passengerIndex = rand.Next(users.Count);

                for (int i = 0; i < 10; i++)
                {                    
                    var userIndex = rand.Next(users.Count);
                    while (userIndex == passengerIndex)
                    {
                        userIndex = rand.Next(users.Count);
                    }

                    var rating = rand.Next(5);

                    Feedback feedback = new Feedback()
                    {
                        Rating = rating,
                        Comment = comments[rating],
                        SenderId = users[userIndex].Id,
                        ReceiverId = users[passengerIndex].Id,
                        Type = FeedbackType.Passenger
                    };

                    ctx.Feedbacks.Add(feedback);
                }
            }

            ctx.SaveChanges();
        }
    }
}
