﻿namespace Carpooling.DataAccess.Data
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
