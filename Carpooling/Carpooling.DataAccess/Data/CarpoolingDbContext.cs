﻿namespace Carpooling.DataAccess.Data
{
    using Carpooling.Models.Entities;

    using Microsoft.EntityFrameworkCore;

    public class CarpoolingDbContext : DbContext
    {
        public CarpoolingDbContext(DbContextOptions<CarpoolingDbContext> options)
            : base(options)
        {
        }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Ride> Rides { get; set; }

        public DbSet<RideBooking> RideBookings { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<Feedback> Feedbacks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CarpoolingDbContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}
