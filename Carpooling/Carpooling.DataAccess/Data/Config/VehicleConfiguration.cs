﻿namespace Carpooling.DataAccess.Data.Config
{
    using Carpooling.Models.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class VehicleConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.HasOne(x => x.User)
                .WithMany(x => x.Vehicles)
                .HasForeignKey(x => x.UserId);

            builder.HasIndex(x => x.PlateNumber)
                .IsUnique();

            builder.Property(x => x.Make)
                .IsRequired();

            builder.Property(x => x.Model)
                .IsRequired();

            builder.Property(x => x.PlateNumber)               
                .IsRequired();

            builder.Property(x => x.UserId)
                .IsRequired();
        }
    }
}
