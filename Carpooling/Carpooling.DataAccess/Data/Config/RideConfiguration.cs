﻿namespace Carpooling.DataAccess.Data.Config
{
    using Carpooling.Models.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class RideConfiguration : IEntityTypeConfiguration<Ride>
    {
        public void Configure(EntityTypeBuilder<Ride> builder)
        {
            builder.HasOne(x => x.StartAddress)
                .WithMany()
                .HasForeignKey(x => x.StartAddressId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.EndAddress)
                .WithMany()
                .HasForeignKey(x => x.EndAddressId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Driver)
                .WithMany(x => x.OrganizedRides)
                .HasForeignKey(x => x.DriverId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Vehicle)
                .WithMany()
                .HasForeignKey(x => x.VehicleId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(x => x.RideBookings)
                .WithOne(x => x.Ride)
                .HasForeignKey(x => x.RideId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(x => x.StartAddressId)
                .IsRequired();

            builder.Property(x => x.EndAddressId)
                .IsRequired();

            builder.Property(x => x.DepartureTime)
                .IsRequired();

            builder.Property(x => x.FreeSpots)
                .IsRequired();

            builder.Property(x => x.Price)
                .HasPrecision(18, 2)
                .IsRequired();

            builder.Property(x => x.Status)
                .IsRequired();
        }
    }
}
