﻿namespace Carpooling.DataAccess.Data.Config
{
    using Carpooling.Models.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class AddressConfiguration : IEntityTypeConfiguration<Address>
    {

        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.Property(x => x.City)
                .IsRequired();

            builder.Property(x => x.Country)
                .IsRequired();

            builder.Property(x => x.Region)
                .IsRequired();

            builder.Property(x => x.AddressLine)
                .IsRequired();
        }
    }
}
