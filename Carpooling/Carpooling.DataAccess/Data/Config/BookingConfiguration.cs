﻿namespace Carpooling.DataAccess.Data.Config
{
    using Carpooling.Models.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class RideBookingConfiguration : IEntityTypeConfiguration<RideBooking>
    {
        public void Configure(EntityTypeBuilder<RideBooking> builder)
        {
            builder.Property(x => x.Id).HasDefaultValueSql("NEWID()");

            builder.HasKey(tr => new { tr.UserId, tr.RideId });

            builder.HasOne(x => x.User)
                .WithMany(x => x.RideBookings)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Ride)
                .WithMany(x => x.RideBookings)
                .HasForeignKey(x => x.RideId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(x => x.UserId)
                .IsRequired();

            builder.Property(x => x.RideId)
                .IsRequired();

            builder.Property(x => x.Status)
                .IsRequired();
        }
    }
}
