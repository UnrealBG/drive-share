﻿namespace Carpooling.DataAccess.Data.Config
{
    using Carpooling.Models.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasIndex(x => x.Username)
                .IsUnique();

            builder.HasIndex(x => x.Email)
                .IsUnique();

            builder.HasIndex(x => x.PhoneNumber)
                .IsUnique();

            builder.HasMany(x => x.OrganizedRides)
                .WithOne(x => x.Driver)
                .HasForeignKey(x => x.DriverId);

            builder.HasMany(x => x.RideBookings)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId);

            builder.HasMany(x => x.GivenFeedbacks)
                .WithOne(x => x.Sender)
                .HasForeignKey(x => x.SenderId);

            builder.HasMany(x => x.ReceivedFeedbacks)
                .WithOne(x => x.Receiver)
                .HasForeignKey(x => x.ReceiverId);

            builder.HasMany(x => x.Vehicles)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId);

            builder.Property(x => x.Username)
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(x => x.Email)
                .HasMaxLength(256)
                .IsRequired()
                .HasAnnotation("Regex", "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");

            builder.Property(x => x.PhoneNumber)
                .HasMaxLength(13)
                .IsRequired();
        }
    }
}
