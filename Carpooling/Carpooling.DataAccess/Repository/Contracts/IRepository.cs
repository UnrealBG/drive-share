﻿namespace Carpooling.DataAccess.Repository.Contracts
{
    using System.Collections.Generic;
    using System.Linq.Expressions;

    using Carpooling.Models.QueryModels;

    using Microsoft.EntityFrameworkCore.Query;

    public interface IRepository<TEntity, TResult> where TEntity : class where TResult : class
    {
        public Task<TResult> GetByIdAsync(
            Guid id,
            Expression<Func<TEntity, TResult>> selector,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null);

        public Task<ICollection<TResult>> GetAsync(
            Expression<Func<TEntity, TResult>> selector,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> filters = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
            PageParameters paging = null);

        public Task<TEntity> CreateAsync(TEntity entity);

        public Task<TEntity> UpdateAsync(TEntity entityToUpdate);

        public Task<bool> DeleteAsync(Guid id);

        public Task<bool> SoftDeleteAsync(TEntity item);

        public Task<int> GetCountAsync();

        Task<int> GetItemsCountAfterFiltersAsync(Func<IQueryable<TEntity>, IQueryable<TEntity>> filters = null);
    }
}
