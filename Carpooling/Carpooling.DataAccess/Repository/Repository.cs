﻿namespace Carpooling.DataAccess.Repository
{
    using System.Collections.Generic;
    using System.Linq.Expressions;

    using Carpooling.DataAccess.Data;
    using Carpooling.DataAccess.Repository.Contracts;
    using Carpooling.Models.Entities.Contracts;
    using Carpooling.Models.QueryModels;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Query;

    public class Repository<TEntity, TResult> : IRepository<TEntity, TResult> where TEntity : class, IBaseEntity where TResult : class
    {
        private readonly IDbContextFactory<CarpoolingDbContext> dbContextFactory;

        public Repository(IDbContextFactory<CarpoolingDbContext> dbContextFactory)
        {
            this.dbContextFactory = dbContextFactory;
        }

        public async Task<int> GetItemsCountAfterFiltersAsync(Func<IQueryable<TEntity>, IQueryable<TEntity>> filters = null)
        {
            return await ExecuteWithDbContextAsync(async ctx =>
            {
                var dbSet = ctx.Set<TEntity>();
                IQueryable<TEntity> query = dbSet;

                if (filters != null)
                {
                    query = filters(query).Where(t => !t.IsDeleted).AsSplitQuery();
                }

                return await query.CountAsync();
            });
        }



        public async Task<TResult> GetByIdAsync(Guid id, Expression<Func<TEntity, TResult>> selector, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            return await ExecuteWithDbContextAsync(async ctx =>
                {
                    var dbSet = ctx.Set<TEntity>();
                    IQueryable<TEntity> query = dbSet;

                    if (include != null)
                    {
                        query = include(query);
                    }

                    return await query.Where(t => t.Id == id).Where(t => !t.IsDeleted).Select(selector).FirstOrDefaultAsync();
                });
        }

        public virtual async Task<ICollection<TResult>> GetAsync(
            Expression<Func<TEntity, TResult>> selector,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> filters = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
            PageParameters paging = null)
        {
            return await ExecuteWithDbContextAsync(async ctx =>
                {
                    var dbSet = ctx.Set<TEntity>();
                    IQueryable<TEntity> query = dbSet;

                    if (include != null)
                    {
                        query = include(query).AsSplitQuery();
                    }

                    if (orderBy != null)
                    {
                        query = orderBy(query).AsSplitQuery();
                    }
                    else
                    {
                        query = query.OrderBy(t => t.Id).AsSingleQuery();
                    }

                    if (filters != null)
                    {
                        query = filters(query).Where(t => !t.IsDeleted).AsSplitQuery();
                    }

                    if (paging != null)
                    {
                        query = query.Skip(paging.StartIndex).Take(paging.Take);
                    }

                    return await query.Select(selector).ToListAsync();
                });
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity)
        {
            return await ExecuteWithDbContextAsync(async ctx =>
                {
                    var dbSet = ctx.Set<TEntity>();

                    await dbSet.AddAsync(entity);
                    await ctx.SaveChangesAsync();

                    return entity;
                });
        }


        public virtual async Task<TEntity> UpdateAsync(TEntity entityToUpdate)
        {
            return await ExecuteWithDbContextAsync(async ctx =>
                {
                    var dbSet = ctx.Set<TEntity>();

                    await dbSet.AddAsync(entityToUpdate);
                    ctx.Entry(entityToUpdate).State = EntityState.Modified;
                    await ctx.SaveChangesAsync();

                    return entityToUpdate;
                });
        }

        public virtual async Task<bool> DeleteAsync(Guid id)
        {
            return await ExecuteWithDbContextAsync(async ctx =>
                {
                    var dbSet = ctx.Set<TEntity>();
                    TEntity entityToDelete = await dbSet.FindAsync(id);

                    if (entityToDelete == null)
                    {
                        return false;
                    }

                    if (ctx.Entry(entityToDelete).State == EntityState.Detached)
                    {
                        dbSet.Attach(entityToDelete);
                    }

                    dbSet.Remove(entityToDelete);
                    await ctx.SaveChangesAsync();

                    return true;
                });
        }

        private async Task DeleteAsync(TEntity entityToDelete)
        {
            using var ctx = this.dbContextFactory.CreateDbContext();
            var dbSet = ctx.Set<TEntity>();

            if (ctx.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }

            dbSet.Remove(entityToDelete);
            await ctx.SaveChangesAsync();
        }

        public async Task<bool> SoftDeleteAsync(TEntity item)
        {
            item.IsDeleted = true;

            var result = await UpdateAsync(item);
            if (result == null) return false;
            return true;
        }

        public async Task<int> GetCountAsync()
        {
            return await ExecuteWithDbContextAsync(async ctx =>
                {
                    var dbSet = ctx.Set<TEntity>();
                    return await dbSet.Where(x => !x.IsDeleted).CountAsync();
                });
        }

        private async Task<TResult> ExecuteWithDbContextAsync<TResult>(Func<CarpoolingDbContext, Task<TResult>> action)
        {
            using var ctx = this.dbContextFactory.CreateDbContext();
            return await action(ctx);
        }
    }
}
