﻿namespace Carpooling.Web.ExceptionHandling
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Components;
    using Microsoft.AspNetCore.Components.Web;

    public class CustomErrorBoundary : ErrorBoundary
    {
        [Inject]
        private IHostEnvironment Env { get; set; }

        protected override Task OnErrorAsync(Exception exception)
        {
            if (this.Env.IsDevelopment())
            {
                return base.OnErrorAsync(exception);
            }

            return Task.CompletedTask;
        }
    }
}
