﻿namespace Carpooling.Web.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    
    [Route("api/[controller]")]
    [ApiController]
    public class ProxyController : ControllerBase
    {
        private readonly string apiKey;

        public ProxyController(IConfiguration config)
        {
            this.apiKey = config["BingMapsKey"];
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var url = $"https://www.bing.com/api/maps/mapcontrol?callback=GetMap&key={this.apiKey}";

            using var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            return this.Content(content, "text/javascript");
        }
    }
}
