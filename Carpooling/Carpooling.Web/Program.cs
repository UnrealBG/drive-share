namespace Carpooling.Web
{
    using Blazored.Toast;

    using Carpooling.DataAccess.Data;
    using Carpooling.DataAccess.Repository.Contracts;
    using Carpooling.DataAccess.Repository;
    using Carpooling.Services.Contracts;
    using Carpooling.Services;
    using Carpooling.Services.ExternalServices;

    using Microsoft.AspNetCore.Authentication.OpenIdConnect;
    using Microsoft.Identity.Web;
    using Microsoft.Identity.Web.UI;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using System.Security.Claims;

    using Carpooling.Web.Helpers;

    using Blazored.Modal;

    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
            })
                .AddMicrosoftIdentityWebApp(
                msIdOptions =>
                {
                    builder.Configuration.Bind("AzureADB2C", msIdOptions);

                    msIdOptions.Events.OnSignedOutCallbackRedirect = async context =>
                    {
                        context.Response.Redirect("/");
                        context.HandleResponse();
                    };

                    msIdOptions.Events.OnTicketReceived = async context =>
                    {
                        var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();

                        var user = await userService.GetUserByAzureUserIdAsync(Guid.Parse(context.Principal.GetNameIdentifierId()));

                        if (user.Value is null)
                        {
                            return;
                        }
                        else if (user.Value.IsBlocked)
                        {
                            context.Response.Redirect("/blockeduser");
                            context.HandleResponse();
                        }
                        else
                        {
                            var userRoles = await userService.GetUserRolesAsync(user.Value.Id);

                            var claimsIdentity = context.Principal.Identity as ClaimsIdentity;
                            foreach (var role in userRoles.Value)
                            {
                                claimsIdentity.AddClaim(new Claim(ClaimTypes.Role, role.ToString()));
                            }
                        }
                    };
                },
                cookieOptions =>
                {
                    cookieOptions.Cookie.Name = "CarpoolingCookie";
                    cookieOptions.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                    cookieOptions.SlidingExpiration = true;
                });

            builder.Services.AddControllersWithViews()
                .AddMicrosoftIdentityUI();

            var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
            builder.Services.AddDbContextFactory<CarpoolingDbContext>(options => options.UseSqlServer(connectionString));
            builder.Services.AddDbContextPool<CarpoolingDbContext>(
                options =>
                    options.UseSqlServer(connectionString));

            builder.Services.AddRazorPages();
            builder.Services.AddServerSideBlazor()
                .AddMicrosoftIdentityConsentHandler();

            builder.Services.AddHttpContextAccessor();

            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            builder.Services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            var emailConfig = builder.Configuration
                .GetSection("EmailConfiguration")
                .Get<EmailConfiguration>();
            builder.Services.AddSingleton(emailConfig);

            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            builder.Services.AddScoped<IDbInitializer, DbInitializer>();
            builder.Services.AddScoped<IGeoLocationService, GeoLocationService>();
            builder.Services.AddScoped<IAddressService, AddressService>();
            builder.Services.AddScoped<IUserService, UserService>();
            builder.Services.AddScoped<IImageService, ImageService>();
            builder.Services.AddScoped<IEmailClient, EmailClient>();
            builder.Services.AddScoped<IEmailService, EmailService>();
            builder.Services.AddScoped<IVehicleService, VehicleService>();
            builder.Services.AddScoped<IRideService, RideService>();
            builder.Services.AddScoped<IFeedbackService, FeedbackService>();
            builder.Services.AddScoped<IBookService, BookService>();
            builder.Services.AddTransient<AddressFromMap>();
            builder.Services.AddBlazoredToast();
            builder.Services.AddBlazoredModal();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            SeedDataBase();

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapRazorPages();
            app.MapControllers();
            app.MapBlazorHub();
            app.MapFallbackToPage("/_Host");

            app.Run();

            void SeedDataBase()
            {
                using var scope = app.Services.CreateScope();
                var dbInitializer = scope.ServiceProvider.GetRequiredService<IDbInitializer>();
                dbInitializer.Initialize();
            }
        }
    }
}