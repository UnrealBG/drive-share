﻿namespace Carpooling.Web.Validations.Account
{
    using System.Net.Http;

    using Carpooling.Models.DtoModels.CreateDtos;

    using FluentValidation;

    public class RegisterUserValidation : AbstractValidator<CreateUserDto>
    {
        private readonly HttpClient httpClient;

        public RegisterUserValidation(HttpClient httpClient)
        {
            this.httpClient = httpClient;

            this.RuleFor(_ => _.FirstName).NotEmpty()
                .WithMessage("Your first name cannot be empty")
                .MinimumLength(2)
                .WithMessage("Your first name length must be at least 2.")
                .MaximumLength(16)
                .WithMessage("Your first name length must not exceed 16.");
            this.RuleFor(_ => _.LastName).NotEmpty()
                .WithMessage("Your last name cannot be empty")
                .MinimumLength(2)
                .WithMessage("Your last name length must be at least 2.")
                .MaximumLength(16)
                .WithMessage("Your last name length must not exceed 16.");
            //this.RuleFor(_ => _.Email).NotEmpty().WithMessage("Your email cannot be empty")
            //    .EmailAddress()
            //    .MustAsync(async (value, cancellationToken) => await this.UniqueEmail(value))
            //    .When(_ => !string.IsNullOrEmpty(_.Email) && Regex.IsMatch(_.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase), ApplyConditionTo.CurrentValidator)
            //    .WithMessage("email should be unique");

            //this.RuleFor(_ => _.Password).NotEmpty().WithMessage("Your password cannot be empty").MinimumLength(6).WithMessage("Your password length must be at least 6.").WithMessage("email should be unique")
            //    .MaximumLength(16).WithMessage("Your password length must not exceed 16.")
            //    .Matches(@"[A-Z]+").WithMessage("Your password must contain at least one uppercase letter.")
            //    .Matches(@"[a-z]+").WithMessage("Your password must contain at least one lowercase letter.")
            //    .Matches(@"[0-9]+").WithMessage("Your password must contain at least one number.")
            //    .Matches(@"[\@\!\?\*\.]+").WithMessage("Your password must contain at least one (@!? *.).");

            //this.RuleFor(_ => _.ConfirmPassword).Equal(_ => _.Password).WithMessage("ConfirmPassword must equal Password");
            this.RuleFor(_ => _.Username).NotEmpty()
                .WithMessage("Your username cannot be empty")
                .MinimumLength(2)
                .WithMessage("Your username length must be at least 2.")
                .MaximumLength(16)
                .WithMessage("Your username length must not exceed 16.");
            this.RuleFor (_ => _.ProfileImageUrl).Must(_ => Uri.IsWellFormedUriString(_, UriKind.Absolute)).WithMessage("Your avatar url must be valid");
            this.RuleFor(_ => _.PhoneNumber).ChildRules(
                _ =>
                    {
                _.RuleFor(x => x).Must(x => x.StartsWith("+359")).WithMessage("Phone number must start with +359");
                _.RuleFor(x => x).Must(x => x.Length == 13).WithMessage("Phone number must be 13 characters long");
            });
        }

        public Func<object, string, Task<IEnumerable<string>>> ValidateValue =>
            async (model, propertyName) =>
            {
                var result = await this.ValidateAsync(
                                 ValidationContext<CreateUserDto>.CreateWithOptions(
                                     (CreateUserDto)model,
                                     x => x.IncludeProperties(propertyName)));
                if (result.IsValid)
                {
                    return Array.Empty<string>();
                }

                return result.Errors.Select(e => e.ErrorMessage);
            };

        //private async Task<bool> UniqueEmail(string email)
        //{
        //    // TODO : Check if email is unique
        //    return true;
        //}
    }
}
