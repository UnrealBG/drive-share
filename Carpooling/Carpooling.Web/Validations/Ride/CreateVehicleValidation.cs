﻿namespace Carpooling.Web.Validations.Ride
{
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Services.Contracts;

    using FluentValidation;

    public class CreateVehicleValidation : AbstractValidator<CreateVehicleDto>
    {
        private readonly IVehicleService vehicleService;

        public CreateVehicleValidation(IVehicleService vehicleService)
        {
            this.vehicleService = vehicleService;
            this.RuleFor(x => x.Make)
                .NotEmpty()
                .WithMessage("Make is required.")
                .MaximumLength(51)
                .WithMessage("Make must be less than 51 characters.");

            this.RuleFor(x => x.Model)
                .NotEmpty()
                .WithMessage("Model is required.")
                .MaximumLength(51)
                .WithMessage("Model must be less than 51 characters.");

            this.RuleFor(x => x.Color)
                .NotEmpty()
                .WithMessage("Color is required.")
                .MaximumLength(30)
                .WithMessage("Color must be less than 30 characters.");

            this.RuleFor(x => x.PlateNumber)
                .NotEmpty()
                .WithMessage("Plate number is empty or already exists.")
                .MaximumLength(10)
                .WithMessage("Plate number must be less than 10 characters.")
                .Matches(@"^[A-Z0-9]+$")
                .WithMessage("Plate number can only contain uppercase letters and numbers.");
        }
    }
}
