﻿namespace Carpooling.Web.Validations.Ride
{
    using Carpooling.Models.DtoModels.CreateDtos;

    using FluentValidation;

    public class CreateRideValidation : AbstractValidator<CreateRideDto>
    {
        public CreateRideValidation()
        {
            this.RuleFor(_ => _.DepartureTime)
                .GreaterThan(DateTime.UtcNow.AddHours(1).ToLocalTime())
                .WithMessage("Departure time must be in the future.");

            this.RuleFor(_ => _.FreeSpots)
                .GreaterThan(0)
                .WithMessage("Free spots must be greater than 0.");

            this.RuleFor(_ => _.Pets)
                .NotNull()
                .WithMessage("Please specify if pets are allowed or not.");

            this.RuleFor(_ => _.Smoking)
                .NotNull()
                .WithMessage("Please specify if smoking is allowed or not.");

            this.RuleFor(_ => _.VehicleId).Must(this.BeAValidGuid).WithMessage("Please select a vehicle.");

        }

        private bool BeAValidGuid(Guid id)
        {
            return id != Guid.Empty;
        }
    }
}
