﻿namespace Carpooling.Web.Validations.Address
{
    using Carpooling.Models.DtoModels.CreateDtos;

    using FluentValidation;

    public class CreateAddressValidation : AbstractValidator<CreateAddressDto>
    {
        public CreateAddressValidation()
        {
            RuleFor(_ => _.City)
                .NotEmpty()
                .WithMessage("City cannot be empty")
                .MinimumLength(1)
                .WithMessage("City length must be at least 2.")
                .MaximumLength(85).WithMessage("City length must not exceed 85.");
            RuleFor(_ => _.Country)
                .NotEmpty()
                .WithMessage("Country cannot be empty")
                .MinimumLength(4)
                .WithMessage("Country length must be at least 4.")
                .MaximumLength(56)
                .WithMessage("Country length must not exceed 56.");
            RuleFor(_ => _.AddressLine)
                .NotEmpty()
                .WithMessage("Address line cannot be empty")
                .MinimumLength(4)
                .WithMessage("Address line length must be at least 4.")
                .MaximumLength(56)
                .WithMessage("Address line length must not exceed 256.");
        }
    }
}
