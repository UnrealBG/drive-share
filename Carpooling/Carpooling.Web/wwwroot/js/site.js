function closeOffcanvasOnLinkClick() {
  var closeLinks = document.querySelectorAll('.close-offcanvas');
  closeLinks.forEach(function (link) {
    link.addEventListener('click', function () {
      UIkit.offcanvas('#offcanvas').hide();
    });
  });
}