﻿var directionsManager;


var map;
var startLocation = null;
var endLocation = null;
var startLatitude = null;
var startLongitude = null;
var endLatitude = null;
var endLongitude = null;

function loadMap() {
    map = new Microsoft.Maps.Map(document.getElementById('map'), {});

    Microsoft.Maps.loadModule('Microsoft.Maps.Directions', function () {
        directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map);
    });

    Microsoft.Maps.Events.addHandler(map, 'rightclick', function (e) {
        var point = new Microsoft.Maps.Point(e.getX(), e.getY());
        var location = map.tryPixelToLocation(point);
        if (!startLocation) {
            startLocation = location;
            addPinAtLocation(startLocation, "Start");
        } else if (!endLocation) {
            endLocation = location;
            addPinAtLocation(endLocation, "End");
            createRoute();
        } else {   
            startLocation = null;
            endLocation = null;
            directionsManager.clearAll();
            map.entities.clear();
            showContextMenu(e);
        }
    });
    return "";
}

function showContextMenu(e) {
    var menu = document.getElementById("contextMenu");
    menu.style.left = e.clientX + "px";
    menu.style.top = e.clientY + "px";
    menu.style.display = "block";

    var location = e.location;
    menu.setAttribute("data-lat", location.latitude);
    menu.setAttribute("data-lon", location.longitude);
}

function setAsStartLocation() {
    var menu = document.getElementById("contextMenu");
    var lat = parseFloat(menu.getAttribute("data-lat"));
    var lon = parseFloat(menu.getAttribute("data-lon"));
    startLocation = new Microsoft.Maps.Location(lat, lon);
    addPinAtLocation(startLocation, "Start");
    menu.style.display = "none";
}

function setAsEndLocation() {
    var menu = document.getElementById("contextMenu");
    var lat = parseFloat(menu.getAttribute("data-lat"));
    var lon = parseFloat(menu.getAttribute("data-lon"));
    endLocation = new Microsoft.Maps.Location(lat, lon);
    addPinAtLocation(endLocation, "End");
    menu.style.display = "none";
}

function addPinAtLocation(location, label) {
    var pushpin = new Microsoft.Maps.Pushpin(location, { text: label });
    map.entities.push(pushpin);
}

function clearAllPinsAndLocations() {
    map.entities.clear();
    startLocation = null;
    endLocation = null;
    var menu = document.getElementById("contextMenu");
    menu.style.display = "none";
}

function createRoute() {
    if (!startLocation || !endLocation) {
        alert("Please set both the start and end locations.");
        return;
    }

    directionsManager.clearAll();

    var startWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: startLocation });
    var endWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: endLocation });

    directionsManager.addWaypoint(startWaypoint);
    directionsManager.addWaypoint(endWaypoint);

    directionsManager.calculateDirections();
}

function setAsEndLocation() {
    var menu = document.getElementById("contextMenu");
    var lat = parseFloat(menu.getAttribute("data-lat"));
    var lon = parseFloat(menu.getAttribute("data-lon"));
    endLocation = new Microsoft.Maps.Location(lat, lon);
    addPinAtLocation(endLocation, "End");
    menu.style.display = "none";

    createRoute();
}

function addPinAtLocation(location, label) {
    var pushpin = new Microsoft.Maps.Pushpin(location, { text: label });
    map.entities.push(pushpin);

    if (label === "Start") {
        startLatitude = location.latitude;
        startLongitude = location.longitude;
    } else if (label === "End") {
        endLatitude = location.latitude;
        endLongitude = location.longitude;
    }

    if (startLatitude !== null && startLongitude !== null && endLatitude !== null && endLongitude !== null) {
        DotNet.invokeMethodAsync('Carpooling.Web', 'UpdatePinCoordinates', startLatitude, startLongitude, endLatitude, endLongitude);
    }
}

function createFinalRoute(startLat, startLon, endLat, endLon) {
    if (directionsManager) {
        var startWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(startLat, startLon) });
        var endWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(endLat, endLon) });

        directionsManager.addWaypoint(startWaypoint);
        directionsManager.addWaypoint(endWaypoint);

        directionsManager.calculateDirections();
    } else {
        alert("Directions Manager not initialized.");
    }
}

function clearDirections() {
    if (directionsManager) {
        directionsManager.clearAll();
    }

    map.entities.clear();
    startLocation = null;
    endLocation = null;
}