﻿var map;
var dirMan;

function loadBingMap() {
    map = new Microsoft.Maps.Map(document.getElementById('map'), {});
    Microsoft.Maps.loadModule('Microsoft.Maps.Directions', function () {
        dirMan = new Microsoft.Maps.Directions.DirectionsManager(map);
    });

    return "";
}

function createDetailsRoute(startLat, startLon, endLat, endLon) {
    if (dirMan) {
        var startWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(startLat, startLon) });
        var endWaypoint = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(endLat, endLon) });

        dirMan.addWaypoint(startWaypoint);
        dirMan.addWaypoint(endWaypoint);

        dirMan.calculateDirections();
    } else {
        alert("Directions Manager not initialized.");
    }
}

function addPin() {
    var menu = document.getElementById("contextMenu");
    var lat = parseFloat(menu.getAttribute("data-lat"));
    var lon = parseFloat(menu.getAttribute("data-lon"));
    var location = new Microsoft.Maps.Location(lat, lon);
    var pushpin = new Microsoft.Maps.Pushpin(location);
    map.entities.push(pushpin);
    menu.style.display = "none";
    console.log("Adding pin at:", lat, lon);
}

function clearAllDirections() {
    if (dirMan) {
        dirMan.clearAll();
    }
}



