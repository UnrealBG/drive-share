﻿namespace Carpooling.Web.Helpers
{
    public static class ViewStringConstants
    {
        public const string ALLOWED_PETS = "Pets are allowed in the Car";
        public const string NOT_ALLOWED_PETS = "Pets are not allowed in the Car";
        public const string ALLOWED_SMOKING = "Smoking is allowed";
        public const string NOT_ALLOWED_SMOKING = "Smoking is not allowed";
        public const string AVATAR = "img/driver-1.png";
        public static readonly string[] CAR_AVATARS = { "img/search-car-1.png", "img/search-car-2.png", "img/search-car-3.png" };
        public const string DATA_FORMAT = "dd/MM/yy hh:mm tt";
        public const string DESCENDING = "Descending";
        public const string ASCENDING = "Ascending";
    }
}
