﻿namespace Carpooling.Web.Helpers
{
    using BingMapsRESTToolkit;

    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Services.Contracts;

    using Microsoft.JSInterop;

    public class AddressFromMap
    {
        private readonly IGeoLocationService geoLocationService;

        public AddressFromMap(IGeoLocationService geoLocationService)
        {
            this.geoLocationService = geoLocationService;
        }

        private static double StartLatitude { get; set; }

        private static double StartLongitude { get; set; }

        private static double EndLatitude { get; set; }

        private static double EndLongitude { get; set; }


        [JSInvokable]
        public static void UpdatePinCoordinates(double startLat, double startLon, double endLat, double endLon)
        {
            StartLatitude = startLat;
            StartLongitude = startLon;
            EndLatitude = endLat;
            EndLongitude = endLon;
        }

        public async Task<(CreateAddressDto from, CreateAddressDto to)> 
            GetAddressFromMap(CreateAddressDto fromAddressDto, CreateAddressDto toAddressDto)
        {

            Location startLocation =
                new Location { Point = new Point { Coordinates = new double[] { StartLatitude, StartLongitude } } };

            Location endLocation =
                new Location { Point = new Point { Coordinates = new double[] { EndLatitude, EndLongitude } } };

            var startAddress = await this.geoLocationService.ReverseGeocodeAsync(startLocation);
            var endAddress = await this.geoLocationService.ReverseGeocodeAsync(endLocation);

            if (!startAddress.IsSuccess || !endAddress.IsSuccess)
            {
                return (null, null);
            }

            var startAddressResult = startAddress.Value.Split(',');
            fromAddressDto.AddressLine = startAddressResult[0];
            fromAddressDto.City = startAddressResult[1];
            fromAddressDto.Region = startAddressResult[1];
            fromAddressDto.Country = startAddressResult[2];
            fromAddressDto.Latitude = StartLatitude;
            fromAddressDto.Longitude = StartLongitude;

            var endAddressResult = endAddress.Value.Split(',');
            toAddressDto.AddressLine = endAddressResult[0];
            toAddressDto.City = endAddressResult[1];
            toAddressDto.Region = endAddressResult[1];
            toAddressDto.Country = endAddressResult[2];
            toAddressDto.Latitude = EndLatitude;
            toAddressDto.Longitude = EndLongitude;

            return (fromAddressDto, toAddressDto);
        }
    }
}
