﻿namespace Carpooling.Web.Pages
{
    using System.Globalization;

    using Blazored.Toast.Services;

    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Services.Contracts;
    using Carpooling.Web.Helpers;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Components;
    using Microsoft.AspNetCore.Components.Authorization;
    using Microsoft.JSInterop;

    [Authorize(Roles = "User,Administrator")]
    public partial class OfferRide
    {
        private CreateRideDto ride = new ();
        private CreateAddressDto fromAddressDto = new ();
        private CreateAddressDto toAddressDto = new ();
        private Models.Entities.Address fromAddress = new ();
        private Models.Entities.Address toAddress = new ();
        private List<GetVehicleDto> cars = new ();

        // TODO: Add currency selection
        private string currency = "EUR";

        private AuthenticationState authState;
        private Guid userId;

        [Inject]
        protected IVehicleService VehicleService { get; set; }

        [Inject]
        protected IUserService UserService { get; set; }

        [Inject]
        protected IRideService RideService { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected IToastService ToastService { get; set; }

        [Inject]
        protected IAddressService AddressService { get; set; }

        [Inject]
        protected AuthenticationStateProvider AuthenticationStateProvider { get; set; }

        [Inject]
        protected AddressFromMap AddressFromMap { get; set; }

        private Guid SelectedCarId { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.ride.DepartureTime = DateTime.UtcNow.ToLocalTime();

            this.authState = await this.AuthenticationStateProvider.GetAuthenticationStateAsync();

            var userIdentity = this.authState.User.Identity;
            if (userIdentity != null && userIdentity.IsAuthenticated)
            {
                this.userId = await UserIdentityHelper.GetUserId(this.authState.User, this.UserService);
            }

            this.cars = await this.LoadCarsFromDatabase();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await this.JSRuntime.InvokeVoidAsync("loadMap", null);
            }
        }

        private async Task<List<GetVehicleDto>> LoadCarsFromDatabase()
        {
            var vehicles = await this.VehicleService.GetVehiclesAsync(this.userId);

            return vehicles.Value.ToList();
        }

        private async Task SelectCar(ChangeEventArgs args)
        {
            var selectedCar = args.Value.ToString();

            if (selectedCar == "new")
            {
                await this.JSRuntime.InvokeVoidAsync("showModal", "#vehicle-modal");
            }
            else if (Guid.TryParse(args.Value?.ToString(), out Guid selectedGuid))
            {
                this.SelectedCarId = selectedGuid;
                this.ride.VehicleId = this.SelectedCarId;
            }
        }

        private async Task CreateAddress(string destination)
        {
            if (destination == "pickup")
            {
                await this.JSRuntime.InvokeVoidAsync("showModal", "#fromAddress-modal");
            }
            else
            {
                await this.JSRuntime.InvokeVoidAsync("showModal", "#toAddress-modal");
            }
        }

        private void UpdateBooleanValue(ChangeEventArgs e, string property, bool value)
        {
            if ((string)e.Value == "yes")
            {
                if (property == "Smoking")
                {
                    this.ride.Smoking = value;
                }
                else if (property == "Pets")
                {
                    this.ride.Pets = value;
                }
            }
            else
            {
                if (property == "Smoking")
                {
                    this.ride.Smoking = !value;
                }
                else if (property == "Pets")
                {
                    this.ride.Pets = !value;
                }
            }
        }

        private async Task CreateRide()
        {
            if (this.fromAddressDto.Country != null && this.toAddressDto.Country != null)
            {
                await this.CreateAddressFromDto(this.fromAddressDto, this.toAddressDto);
            }
            else
            {
                var addressParams = await this.AddressFromMap.GetAddressFromMap(this.fromAddressDto, this.toAddressDto);

                if (addressParams.from == null || addressParams.to == null)
                {
                    this.ToastService.ShowError("Please select valid addresses from the map or enter them manually.");
                    return;
                }

                await this.CreateAddressFromDto(this.fromAddressDto, this.toAddressDto);
            }

            this.ride.StartAddressId = this.fromAddress.Id;
            this.ride.EndAddressId = this.toAddress.Id;
            this.ride.Currency = this.currency;
            this.ride.DriverId = this.userId;

            var result = await this.RideService.CreateRideAsync(this.userId, this.ride);

            if (result.IsSuccess)
            {
                this.ToastService.ShowSuccess("Ride created successfully");
                this.NavigationManager.NavigateTo($"/ride-details/{result.Value.Id}", forceLoad: true);
            }
            else
            {
                this.ToastService.ShowError(result.Error);
            }
        }

        private async Task CreateAddressFromDto(CreateAddressDto addrA, CreateAddressDto addrB)
        {
            this.fromAddress = (await this.AddressService.CreateAddressAsync(addrA)).Value;
            this.toAddress = (await this.AddressService.CreateAddressAsync(addrB)).Value;
        }

        private void HandleFromAddressCreated(CreateAddressDto updatedAddress)
        {
            this.fromAddressDto = updatedAddress;
        }

        private void HandleToAddressCreated(CreateAddressDto updatedAddress)
        {
            this.toAddressDto = updatedAddress;
        }

        private async Task CreateRoute()
        {
            await this.CreateAddressFromDto(this.fromAddressDto, this.toAddressDto);

            var startLat = double.Parse(this.fromAddress.Latitude.ToString(), CultureInfo.InvariantCulture);
            var startLon = double.Parse(this.fromAddress.Longitude.ToString(), CultureInfo.InvariantCulture);
            var endLat = double.Parse(this.toAddress.Latitude.ToString(), CultureInfo.InvariantCulture);
            var endLon = double.Parse(this.toAddress.Longitude.ToString(), CultureInfo.InvariantCulture);

            await this.JSRuntime.InvokeVoidAsync("clearDirections");
            await this.JSRuntime.InvokeVoidAsync("createFinalRoute", startLat, startLon, endLat, endLon);
        }

        private async Task HandleFromVehicleCreate()
        {
            this.cars = await this.LoadCarsFromDatabase();
        }
    }
}
