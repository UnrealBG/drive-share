﻿namespace Carpooling.Web.Pages
{
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.Enums;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Contracts;
    using Carpooling.Web.Helpers;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Components;
    using Microsoft.AspNetCore.Components.Web.Virtualization;
    using Microsoft.JSInterop;

    using static Carpooling.Web.Helpers.ViewStringConstants;

    [Authorize(Roles = "User,Administrator")]
    public partial class FindRide
    {
        private readonly string[] serviceImages = new[] { "img/driver-service-1.png", "img/driver-service-2.png", "img/driver-service-3.png" };

        private Virtualize<GetRideDto>? virtualizeRide;
        private List<GetRideDto> rides;
        private Random random;
        private int totalCount;
        private FindRideQuery rideQuery;
        private bool isSearchResult;
        private string orderType = DESCENDING;
        private string orderBy = RidesOrderBy.DriverRating.ToString();
        private FindRideQuery searchQuery;
        private string selectedRideId = null;

        public bool bookingConfirmation { get; set; } = false;

        [Inject]
        protected IRideService rideService { get; set; }

        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected AddressFromMap AddressFromMap { get; set; }

        private string PetsIsAllowed(bool? isAwllowed)
        {
            return (isAwllowed == true) ? ALLOWED_PETS : NOT_ALLOWED_PETS;
        }

        private string SmokongIsAwolled(bool? isAwllowed)
        {
            return (isAwllowed == true) ? ALLOWED_SMOKING : NOT_ALLOWED_SMOKING;
        }

        protected override async Task OnInitializedAsync()
        {
            this.isSearchResult = false;
            var seed = DateTime.Now.Microsecond;
            this.random = new Random(seed);
            this.rideQuery = new FindRideQuery() { RideStatus = RideStatus.Active };
            this.searchQuery = new FindRideQuery() { RideStatus = RideStatus.Active, Date = DateTime.Now };

            var result = await this.rideService.GetRidesAsync(this.rideQuery);
            List<GetRideDto> resultList = new List<GetRideDto>();

            var countResult = await this.rideService.GetItemsCountAfterFiltesAsync(this.rideQuery);

            if (result.IsSuccess)
            {
                resultList = result.Value.ToList();
                this.totalCount = countResult.Value;
            }

            this.rides = resultList;
        }

        private async ValueTask<ItemsProviderResult<GetRideDto>> LoadRides(ItemsProviderRequest request)
        {
            this.rideQuery.StartIndex = request.StartIndex;
            this.rideQuery.Take = request.Count;

            List<GetRideDto> resultList = new List<GetRideDto>();

            this.totalCount = 0;

            var result = await this.rideService.GetRidesAsync(this.rideQuery);

            var countResult = await this.rideService.GetItemsCountAfterFiltesAsync(rideQuery);

            if (result.IsSuccess)
            {
                resultList = result.Value.ToList();
                this.totalCount = countResult.Value;
            }


            return new ItemsProviderResult<GetRideDto>(resultList, this.totalCount);
        }

        private async void GetSearchResult()
        {
            this.rideQuery = this.searchQuery;

            this.rideQuery.Date = this.searchQuery.Date.Value.ToUniversalTime();

            this.searchQuery = new FindRideQuery() { RideStatus = RideStatus.Active, Date = DateTime.Now };

            this.isSearchResult = true;

            await this.virtualizeRide?.RefreshDataAsync();
            this.StateHasChanged();
        }

        private async void SortRides()
        {
            this.rideQuery.OrderBy = Enum.Parse<RidesOrderBy>(orderBy);
            this.rideQuery.descOrder = (orderType == DESCENDING) ? true : false;

            await this.virtualizeRide?.RefreshDataAsync();
            this.StateHasChanged();
        }

        private async Task ShowMap()
        {
            await this.JSRuntime.InvokeVoidAsync("showModal", "#map-modal");
        }

        private async Task ModalClosed()
        {
            var result = await AddressFromMap.GetAddressFromMap(new CreateAddressDto(), new CreateAddressDto());

            this.searchQuery.StartCountry = result.from.Country ?? this.searchQuery.StartCountry;
            this.searchQuery.StartRegion = result.from.Region ?? this.searchQuery.StartRegion;
            this.searchQuery.StartCity = result.from.City ?? this.searchQuery.StartCity;

            this.searchQuery.EndCountry = result.to.Country ?? this.searchQuery.EndCountry;
            this.searchQuery.EndRegion = result.to.Region ?? this.searchQuery.EndRegion;
            this.searchQuery.EndCity = result.to.City ?? this.searchQuery.EndCity;
        }

        public void BookConfirmation(GetRideDto ride)
        {
            this.bookingConfirmation = true;
            this.selectedRideId = ride.Id.ToString();
        }

        public async Task BackToFindRide()
        {
            this.bookingConfirmation = false;
        }
    }
}
