﻿namespace Carpooling.Web.Pages
{
    using System.Text;

    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.Enums;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Contracts;

    using Microsoft.AspNetCore.Components;

    public partial class Index
    {
        private List<GetUserDto> topDrivers = new();
        private List<GetUserDto> topPassengers = new();
        private Driver demoDriver = new Driver { Rating = 3.5 };

        [Inject]
        protected IUserService UserService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            FindUserQuery query = new FindUserQuery()
            {
                UsersOrderBy = UsersOrderBy.DriverRating,
                Descending = true
            };

            var users = await this.UserService.GetUserAsync(query);

            if (users.IsSuccess)
            {
                this.topDrivers = users.Value.ToList();
            }

            query = new FindUserQuery()
            {
                UsersOrderBy = UsersOrderBy.PassengerRating,
                Descending = true
            };

            users = await this.UserService.GetUserAsync(query);

            if (users.IsSuccess)
            {
                this.topPassengers = users.Value.ToList();
            }
        }

        private string DisplayStars(double rating)
        {
            int fullStars = (int)rating;
            bool hasHalfStar = rating % 1 >= 0.5;
            int maxStars = 5;
            StringBuilder starsRepresentation = new StringBuilder();

            for (int i = 0; i < fullStars; i++)
            {
                starsRepresentation.Append("<i class='fas fa-star'></i>");
            }

            if (hasHalfStar)
            {
                starsRepresentation.Append("<i class='fas fa-star-half-alt'></i>");
            }

            for (int i = 0; i < maxStars - fullStars - (hasHalfStar ? 1 : 0); i++)
            {
                starsRepresentation.Append("<i class='far fa-star'></i>");
            }

            return starsRepresentation.ToString();
        }

        // TODO: Да изтрием този метод, когато имаме реални данни!!!
        private double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        // TODO: Да изтрием този клас, когато имаме реални данни!!!
        public class Driver
        {
            public double Rating { get; set; }
        }
    }
}
