﻿namespace Carpooling.Web.Pages
{
    using System.Text.Json;

    using Blazored.Toast.Services;

    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Services;
    using Carpooling.Services.Contracts;
    using Carpooling.Web.Helpers;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Components;
    using Microsoft.AspNetCore.Components.Authorization;
    using Microsoft.AspNetCore.Components.Forms;
    using Microsoft.JSInterop;

    [Authorize(Roles = "User,Administrator")]
    public partial class VehicleCreationModal
    {
        private CreateVehicleDto vehicle = new ();
        private EditContext EditContext;
        private ValidationMessageStore messageStore;
        private List<string> models = new ();
        private string carImage;

        [Parameter]
        public EventCallback OnClose { get; set; }

        [Parameter]
        public EventCallback OnVehicleCreated { get; set; }

        [Inject]
        protected IVehicleService VehicleService { get; set; }

        [Inject]
        protected IUserService UserService { get; set; }

        [Inject]
        protected AuthenticationStateProvider AuthenticationStateProvider { get; set; }

        [Inject]
        protected IHostEnvironment HostEnvironment { get; set; }

        [Inject]
        protected IImageService ImageService { get; set; }

        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected IToastService ToastService { get; set; }

        protected Dictionary<string, List<string>> Brands { get; set; } = new ();

        protected List<string> Colors { get; set; } = new ();

        protected override async Task OnInitializedAsync()
        {
            this.EditContext = new EditContext(this.vehicle);
            this.messageStore = new ValidationMessageStore(this.EditContext);

            var carsFilePath = Path.Combine(this.HostEnvironment.ContentRootPath, "Shared", "cars.json");
            var colorsFilePath = Path.Combine(this.HostEnvironment.ContentRootPath, "Shared", "colors.json");

            var cars = await File.ReadAllTextAsync(carsFilePath);
            var colors = await File.ReadAllTextAsync(colorsFilePath);

            this.Brands = JsonSerializer.Deserialize<Dictionary<string, List<string>>>(cars);
            this.Colors = JsonSerializer.Deserialize<List<string>>(colors);
        }

        protected async Task CreateVehicle()
        {
            if (this.EditContext.Validate())
            {
                var user = (await this.AuthenticationStateProvider.GetAuthenticationStateAsync()).User;
                var userId = await UserIdentityHelper.GetUserId(user, this.UserService);

                var result = await this.VehicleService.CreateVehicleAsync(userId, this.vehicle);

                if (result.IsSuccess)
                {
                    this.ToastService.ShowSuccess("Vehicle added successfully");
                    await this.JSRuntime.InvokeVoidAsync("closeModal", "#vehicle-modal");
                    this.StateHasChanged();
                }
                else
                {
                    this.ToastService.ShowError("Something went wrong");
                }

                await this.OnVehicleCreated.InvokeAsync();

                this.ResetModal();
            }
        }

        private void LoadModels(ChangeEventArgs args)
        {
            this.vehicle.Make = args.Value.ToString();
            this.models = this.Brands.TryGetValue(this.vehicle.Make, out var brand) ? brand : new List<string>();
        }

        private async Task LoadImage(InputFileChangeEventArgs e)
        {
            var imageFile = e.File;
            if (imageFile != null)
            {
                var format = "image/png";
                var resizedImage = await imageFile.RequestImageFileAsync(format, 400, 400);
                var buffer = new byte[resizedImage.Size];

                using var stream = resizedImage.OpenReadStream();
                int bytesRead;
                int totalBytesRead = 0;
                while ((bytesRead = await stream.ReadAsync(buffer, totalBytesRead, buffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;
                }

                var imageBase64 = Convert.ToBase64String(buffer);
                this.carImage = $"data:{format};base64,{imageBase64}";

                var file = new FormFile(new MemoryStream(buffer), 0, buffer.Length, "name", imageFile.Name)
                               {
                                   Headers = new HeaderDictionary(),
                                   ContentType = "image/png"
                               };

                var url = await this.ImageService.UploadAsync(file);

                this.vehicle.ImageUrl = url;

                this.StateHasChanged();
            }

        }

        private async Task CheckPlateNumber(ChangeEventArgs e)
        {
            string plateNumber = e.Value.ToString();

            var fieldIdentifier = new FieldIdentifier(this.vehicle, "PlateNumber");

            bool exists = await this.VehicleService.IsPlateNumberExistAsync(plateNumber);

            if (exists)
            {
                this.messageStore.Add(this.EditContext.Field("PlateNumber"), "Plate number already exists.");
            }
            else
            {
                this.vehicle.PlateNumber = plateNumber;
                this.messageStore.Clear(this.EditContext.Field("PlateNumber"));
            }

            this.EditContext.NotifyValidationStateChanged();
        }

        private void ResetModal()
        {
            this.vehicle = new ();
            this.carImage = null;
            this.models = new ();
            this.EditContext = new EditContext(this.vehicle);
        }
    }
}
