﻿namespace Carpooling.Web.Pages
{
    using Blazored.Modal;
    using Blazored.Modal.Services;
    using Blazored.Toast.Services;

    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Contracts;
    using Carpooling.Web.Helpers;
    using Carpooling.Web.Shared;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Components;
    using Microsoft.AspNetCore.Components.Authorization;

    [Authorize(Roles = "User,Administrator")]
    public partial class MyRides
    {
        private IEnumerable<GetRideDto>? rides = new List<GetRideDto>();

        private IEnumerable<GetRideBookingDto>? bookings = new List<GetRideBookingDto>();

        [Parameter]
        public string Message { get; set; }

        [Parameter]
        public string ConfirmLabel { get; set; } = "Yes";

        [Parameter]
        public string CancelLabel { get; set; } = "No";

        [Parameter]
        public EventCallback<bool> OnConfirm { get; set; }

        [Parameter]
        public EventCallback OnCancel { get; set; }

        [CascadingParameter]
        public IModalService ModalService { get; set; }

        [Inject]
        protected IToastService ToastService { get; set; }

        [Inject]
        protected IRideService RideService { get; set; }

        [Inject]
        protected IBookService BookingService { get; set; }

        [Inject]
        protected IUserService UserService { get; set; }

        [Inject]
        protected AuthenticationStateProvider AuthenticationStateProvider { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var userClaim = (await AuthenticationStateProvider.GetAuthenticationStateAsync()).User;
            var userId = await UserIdentityHelper.GetUserId(userClaim, this.UserService);

            var user = await this.UserService.GetUserByIdAsync(userId);

            if (user.IsSuccess)
            {
                FindRideQuery query = new FindRideQuery()
                {
                    DriverId = userId
                };

                var result = await this.RideService.GetRidesAsync(query);

                if (result.IsSuccess)
                {
                    this.rides = result.Value.ToList();
                }
            }

            var bookingQuery = new FindBookingQuery()
            {
                UserId = userId
            };

            await this.GetBookings(bookingQuery);
        }

        private async Task CancelBooking(Guid userId, Guid bookingId)
        {
            var isTrue = await this.ShowConfirmationModal();

            if (isTrue)
            {
                var result = await this.BookingService.DeleteBookingAsync(userId, bookingId);

                if (result.IsSuccess)
                {
                    this.ToastService.ShowSuccess("Booking cancelled successfully!");

                    var bookingQuery = new FindBookingQuery()
                    {
                        UserId = userId
                    };

                    await this.GetBookings(bookingQuery);

                    this.StateHasChanged();
                }
                else
                {
                    this.ToastService.ShowError("Something went wrong!");
                }
            }
        }

        private async Task GetBookings(FindBookingQuery bookingQuery)
        {
            var resultBookings = await this.BookingService.GetBookingsAsync(bookingQuery);

            if (resultBookings.IsSuccess)
            {
                this.bookings = resultBookings.Value;
            }
        }

        private async Task<bool> ShowConfirmationModal()
        {
            var title = $"Cancel the booking?";

            var parameters = new ModalParameters
                             {
                                 { "Message", string.Format("Are you sure you want to cancel the booking?") }
                             };

            var confirmationModal = this.ModalService.Show<ConfirmationModalDialog>(title, parameters);
            var result = await confirmationModal.Result;

            if (!result.Cancelled)
            {
                return true;
            }

            return false;
        }
    }
}
