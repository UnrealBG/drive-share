﻿namespace Carpooling.Web.Pages
{
    using Blazored.Modal;
    using Blazored.Modal.Services;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.Entities;
    using Carpooling.Models.Enums;
    using Carpooling.Models.QueryModels;
    using Carpooling.Services.Contracts;
    using Carpooling.Web.Shared;
    using Microsoft.AspNetCore.Components;
    using Microsoft.AspNetCore.Components.Authorization;
    using Microsoft.AspNetCore.Components.Web;
    using Microsoft.AspNetCore.Components.Web.Virtualization;
    using Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure;
    using Microsoft.JSInterop;
    using System.Text;
    using System.Threading.Tasks.Dataflow;

    public partial class AdminPanel
    {
        private List<GetUserDto> userProfileCollection = new List<GetUserDto>();

        private Virtualize<GetUserDto> virtualizeUserProfiles;

        private int totalCount;

        private bool isSearchResult;

        private FindUserQuery searchQuery = new FindUserQuery();

        private FindUserQuery userQuery = new FindUserQuery();

        private bool searchMenuExpanded = false;

        private string searchString = "Search";

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected AuthenticationStateProvider AuthStateProvider { get; set; }

        [Inject]
        protected IUserService UserService { get; set; }

        [CascadingParameter]
        protected IModalService Modal { get; set; }

        protected override async Task OnInitializedAsync()
        {
            totalCount = (await UserService.GetItemsCountAfterFiltesAsync(userQuery)).Value;

            await this.LoadModel(new ItemsProviderRequest());
        }

        private async ValueTask<ItemsProviderResult<GetUserDto>> LoadModel(ItemsProviderRequest request)
        {
            this.userQuery.StartIndex = request.StartIndex;
            this.userQuery.Take = request.Count;

            List<GetUserDto> resultList = new List<GetUserDto>();

            this.totalCount = 0;

            var result = await this.UserService.GetUserAsync(this.userQuery);

            var countResult = await this.UserService.GetItemsCountAfterFiltesAsync(userQuery);

            if (result.IsSuccess)
            {
                resultList = result.Value.ToList();
                this.totalCount = countResult.Value;
            }

            return new ItemsProviderResult<GetUserDto>(resultList, this.totalCount);
        }

        private async Task BlockUserAsync(string username)
        {
            var authState = await this.AuthStateProvider.GetAuthenticationStateAsync();

            await this.UserService.BlockUserAsync(username, authState.User);

            await virtualizeUserProfiles.RefreshDataAsync();

            StateHasChanged();
        }

        private async Task UnblockUserAsync(string username)
        {

            var authState = await this.AuthStateProvider.GetAuthenticationStateAsync();

            await this.UserService.UnblockUser(username, authState.User);

            await virtualizeUserProfiles.RefreshDataAsync();

            StateHasChanged();
        }

        private async Task DeleteUserAsync(string username)
        {
            var authState = await this.AuthStateProvider.GetAuthenticationStateAsync();

            var userQueryResult = await this.UserService.GetUserByUserNameAsync(username, authState.User);
            if (userQueryResult.IsSuccess)
            {
                await this.UserService.DeleteUserAsync(userQueryResult.Value.Id);
            }

            await virtualizeUserProfiles.RefreshDataAsync();

            StateHasChanged();
        }

        private async Task ShowConfirmationModal(string username, AdminAction adminAction)
        {
            var title = $"{adminAction.ToString()} user {username}";

            var parameters = new ModalParameters
            {
                { "UserName", username },
                { "AdminAction", adminAction },
                { "Message", string.Format("Are you sure you want to {0} user {1}?", adminAction.ToString().ToLower(), username) }
            };

            var confirmationModal = Modal.Show<ConfirmationModalDialog>(title, parameters);
            var result = await confirmationModal.Result;

            if (!result.Cancelled)
            {
                Console.WriteLine("The user confirmed the action.");

                switch (adminAction)
                {
                    case AdminAction.Block:
                        await this.BlockUserAsync(username);
                        break;
                    case AdminAction.Unblock:
                        await this.UnblockUserAsync(username);
                        break;
                    case AdminAction.Delete:
                        await this.DeleteUserAsync(username);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Console.WriteLine("The user cancelled the action.");
            }
        }

        private async Task GetSearchResult()
        {
            this.userQuery = this.searchQuery;

            //this.searchQuery = new FindUserQuery();

            this.isSearchResult = true;

            await this.virtualizeUserProfiles?.RefreshDataAsync();

            this.searchMenuExpanded = false;

            this.StateHasChanged();
        }

        private async Task OnKeyUpHandler(KeyboardEventArgs args)
        {
            if (args.Code == "Enter")
            {
                var query= this.searchQuery;
                await RefreshSearchString();
                await GetSearchResult();
            }
            else
            {
                var query = this.searchQuery;
            }
        }

        private async Task ExpandSearchMenu()
        {
            await this.ResetSearch();
            this.searchMenuExpanded = true;
        }

        private async Task RefreshSearchString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var property in typeof(FindUserQuery).GetProperties())
            {
                var value = property.GetValue(this.searchQuery);

                if (value != null &&
                    property.Name != "Descending" &&
                    property.Name != "StartIndex" &&
                    property.Name != "Take"
                    )
                {
                    sb.Append($"{property.Name}: {value} ");
                }
            }
            this.searchString = sb.ToString();
        }

        private async Task ResetSearch()
        {
            this.searchQuery = new FindUserQuery();
            this.searchString = "Search";
        }
    }
}
