﻿namespace Carpooling.Web.Pages
{
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;

    using Blazored.Toast.Services;
    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.GlobalConstants;
    using Carpooling.Services;
    using Carpooling.Services.Contracts;
    using Carpooling.Web.Helpers;
    using Microsoft.AspNetCore.Components;
    using Microsoft.AspNetCore.Components.Authorization;
    using Microsoft.JSInterop;

    public partial class RideDetails
    {
        private string fromAddress;
        private string toAddress;
        private string vehicleData;
        private string rideDuration;

        private double distance;
        private int bookPlaces = 1;
        
        private bool invalidRideId;
        private bool rideDoesNotExist;

        private GetRideDto ride = new GetRideDto();

        private AuthenticationState authState;

        [Parameter]
        public string RideId { get; set; }

        [Parameter]
        public bool BookingConfirmation { get; set; } = false;

        [Parameter]
        public EventCallback OnBookConfirmedBack { get; set; }

        [Inject]
        protected IRideService RideService { get; set; }

        [Inject]
        protected IGeoLocationService GeoLocationService { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected IToastService ToastService { get; set; }

        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected IUserService UserService { get; set; }

        [Inject]
        protected AuthenticationStateProvider AuthenticationStateProvider { get; set; }

        [Inject]
        protected IBookService BookService { get; set; }
 
        protected override async Task OnInitializedAsync()
        {
            if (!Guid.TryParse(this.RideId, out Guid rideId))
            {
                this.invalidRideId = true;
                return;
            }

            var currentRide = (await this.RideService.GetRideDetailsAsync(rideId)).Value;

            if (currentRide == null)
            {
                this.rideDoesNotExist = true;
                return;
            }

            this.authState = await this.AuthenticationStateProvider.GetAuthenticationStateAsync();

            this.ride = currentRide;

            this.fromAddress = $"{this.ride.StartAddress.Country}, {this.ride.StartAddress.Region}, {this.ride.StartAddress.Country}, {this.ride.StartAddress.AddressLine}";
            this.toAddress = $"{this.ride.EndAddress.Country}, {this.ride.EndAddress.Region}, {this.ride.EndAddress.Country}, {this.ride.EndAddress.AddressLine}";

            this.vehicleData = $"{this.ride.Vehicle.Make} {this.ride.Vehicle.Model}";

            var result = await this.GeoLocationService.GetDistanceAndDurationAsync(this.ride.StartAddress, this.ride.EndAddress);

            this.distance = Math.Round(result.Value.Distance, 2);
            var duration = result.Value.Duration;
            var durationTime = TimeSpan.FromMinutes(duration);

            this.rideDuration = durationTime.Hours == 0 ? $"{durationTime.Minutes} min" : $"{durationTime.Hours} h {durationTime.Minutes} min";
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                if (this.invalidRideId || this.rideDoesNotExist)
                {
                    this.NavigationManager.NavigateTo("/error");
                }
                else
                {
                    await this.JSRuntime.InvokeVoidAsync("loadBingMap", null);
                    Thread.Sleep(500);
                    await this.CreateRoute(this.ride.StartAddress.Latitude, this.ride.StartAddress.Longitude, this.ride.EndAddress.Latitude, this.ride.EndAddress.Longitude);
                }
            }
        }

        private async Task CreateRoute(double startLatitude, double startLongitude, double endLatitude, double endLongitude)
        {
            double startLat = double.Parse(startLatitude.ToString(), CultureInfo.InvariantCulture);
            double startLon = double.Parse(startLongitude.ToString(), CultureInfo.InvariantCulture);
            double endLat = double.Parse(endLatitude.ToString(), CultureInfo.InvariantCulture);
            double endLon = double.Parse(endLongitude.ToString(), CultureInfo.InvariantCulture);

            await this.JSRuntime.InvokeVoidAsync("clearAllDirections");
            await this.JSRuntime.InvokeVoidAsync("createDetailsRoute", startLat, startLon, endLat, endLon);
        }

        private async Task BackToFindRide()
        {
            await this.OnBookConfirmedBack.InvokeAsync();
        }

        private async Task CreateBook()
        {
            if(SetBookPlacesInRanege())
            {
                this.ToastService.ShowError("Booked places have to be greater than one, up to free places.");
                return;
            }

            Guid userId = Guid.Empty;
            var userIdentity = this.authState.User.Identity;

            if (userIdentity != null && userIdentity.IsAuthenticated)
            {
                userId = await UserIdentityHelper.GetUserId(this.authState.User, this.UserService);
            }

            CreateBookingDto booking = new CreateBookingDto()
            {
                RideId = ride.Id,
                BookPlaces = this.bookPlaces
            };

            var result = this.BookService.CreateBookingAsync(userId, booking);

            this.ToastService.ShowSuccess(ResultMessages.SUCCESS_BOOKED);

            this.NavigationManager.NavigateTo("/");
        }

        private bool SetBookPlacesInRanege()
        {
            bool result = false;
            if(this.bookPlaces<1)
            {
                bookPlaces = 1;
                result = true;
            }
            else if(this.bookPlaces>ride.FreeSpots)
            {
                this.bookPlaces = ride.FreeSpots;
                result = true;
            }           
            
            return result;
        }
    }
}
