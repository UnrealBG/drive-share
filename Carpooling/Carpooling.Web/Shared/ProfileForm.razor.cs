﻿namespace Carpooling.Web.Shared
{
    using System.Security.Claims;
    using Microsoft.Identity.Web;

    using Carpooling.Models.DtoModels.CreateDtos;
    using Carpooling.Models.DtoModels.UpdateDtos;
    using Carpooling.Models.DtoModels.GetDtos;
    using Carpooling.Models.DtoModels.Contracts;
    using Carpooling.Services.Contracts;
    using Carpooling.Models.Entities;
    using Carpooling.Models.Enums;
    using static Carpooling.Models.Enums.FormAction;
    using AutoMapper;

    using Microsoft.AspNetCore.Components;
    using Microsoft.AspNetCore.Components.Authorization;
    using Microsoft.AspNetCore.Components.Forms;
    using Carpooling.Services.Helpers;
    using Carpooling.Services;
    using Microsoft.AspNetCore.Mvc.Infrastructure;

    public partial class ProfileForm
    {
        private IUserInboundDto profileModel;
        private string imageSrc;
        private ClaimsPrincipal claimsPrincipal;
        private Guid azureObjectId;
        private User userProfile;
        private string errorMessage;

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected AuthenticationStateProvider AuthStateProvider { get; set; }

        [Inject]
        protected IUserService UserService { get; set; }

        [Inject]
        protected IImageService ImageService { get; set; }

        [Inject]
        protected IMapper Mapper { get; set; }

        [Parameter]
        public FormAction FormAction { get; set; }

        [Parameter]
        public string ButtonText { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await this.InitializeModel();

            var authState = await this.AuthStateProvider.GetAuthenticationStateAsync();
            if (authState.User.Identity.IsAuthenticated)
            {
                this.claimsPrincipal = authState.User;
                if (claimsPrincipal is not null)
                {
                    if (Guid.TryParse(claimsPrincipal.FindFirstValue(ClaimTypes.NameIdentifier), out Guid azureUserId))
                    {
                        this.azureObjectId = azureUserId;

                        var userQueryResult = await this.UserService.GetUserByAzureUserIdAsync(azureUserId);
                        this.userProfile = userQueryResult.Value;
                    }

                    await this.FillInKnownValues();

                    this.ButtonText ??= "Submit";
                }
            }
        }

        private async Task FillInKnownValues()
        {
            if (this.FormAction == Update)
            {
                await this.FillInFromUserProfile();
            }
            await this.FillInFromClaims();
        }

        private async Task FillInFromUserProfile()
        {
            if (this.userProfile is not null)
            {
                this.profileModel = this.Mapper.Map<UpdateUserDto>(this.userProfile);
            }
        }

        private async Task FillInFromClaims()
        {
            if (this.profileModel.Username is null)
            {
                this.profileModel.Username = this.claimsPrincipal.FindFirstValue("name") ?? string.Empty;
            }
            this.profileModel.Email = this.claimsPrincipal.FindFirstValue("emails") ?? string.Empty;
        }

        private async Task InitializeModel()
        {
            if (this.FormAction == Create)
            {
                this.profileModel = new CreateUserDto();
            }
            else
            {
                this.profileModel = new UpdateUserDto();
            }
        }

        private async Task SubmitAsync()
        {
            var claimsIdentity = this.claimsPrincipal.Identity as ClaimsIdentity;

            if (this.FormAction == Create)
            {
                var result = await this.UserService.CreateUserAsync(this.profileModel, claimsIdentity);
                await this.HandleValidSubmit(result, "/MicrosoftIdentity/Account/SignIn?");
            }
            else
            {
                var result = await this.UserService.UpdateUserAsync(this.profileModel, claimsIdentity);
                await this.HandleValidSubmit(result, "/");
            }

            StateHasChanged();
        }

        private async Task HandleValidSubmit(Result<GetUserDto> result, string redirectUrl)
        {
            if (result.IsSuccess)
            {
                this.NavigationManager.NavigateTo(redirectUrl, forceLoad: true);
            }
            else
            {
                this.errorMessage = result.Error;
            }
        }

        private async Task LoadImage(InputFileChangeEventArgs e)
        {
            var imageFile = e.File;
            if (imageFile != null)
            {
                var format = "image/png";
                var resizedImage = await imageFile.RequestImageFileAsync(format, 100, 100);
                var buffer = new byte[resizedImage.Size];
                await resizedImage.OpenReadStream().ReadAsync(buffer);
                var imageBase64 = Convert.ToBase64String(buffer);
                this.imageSrc = $"data:{format};base64,{imageBase64}";

                var file = new FormFile(new MemoryStream(buffer), 0, buffer.Length, "name", imageFile.Name)
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "image/png"
                };

                var url = await this.ImageService.UploadAsync(file);

                this.profileModel.ProfileImageUrl = url;

                StateHasChanged();
            }
        }
    }
}
